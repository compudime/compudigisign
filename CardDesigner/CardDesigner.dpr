program CardDesigner;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.StartUpCopy,
  FMX.Forms,
  uMainIDECards in 'uMainIDECards.pas' {FMainDesigner},
  uFrmPreview in 'uFrmPreview.pas' {FrmPreview},
  uCards in '..\Package\uCards.pas',
  uFrmEventSelector in 'uFrmEventSelector.pas' {FrmEventSelector},
  uFrmRTML in 'uFrmRTML.pas' {FrmRTMLPreview};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFMainDesigner, FMainDesigner);
  Application.CreateForm(TFrmPreview, FrmPreview);
  Application.CreateForm(TFrmEventSelector, FrmEventSelector);
  Application.CreateForm(TFrmRTMLPreview, FrmRTMLPreview);
  Application.Run;
end.
