procedure DisplayJSONLabel (Sender: TJSONLabel; JSONLabelName, JSONField: string; var Text: string);
begin
  if      (JSONField = 'Price') and (JSONLabelName = 'TJSONLabel6') then
    Text := '$' + Text
  else if JSONField = 'Desc'  then
    Text := Lowercase (Text)
  else if JSONField = 'Image' then
  begin
    if Text = '' then
      Text := '(Empty)'
    else if Pos ('[', Text) <> 0 then
      Text := 'JSON Binary Image'
    else 
      Text := 'JSON File Image';
  end;
end;

procedure BeforeDisplayCard;
var
  s: string;
  i: integer;
begin
  RotateLabel.RotationAngle := 45;

  s := '';
  for i := 0 to Card.GetJSONFieldCount - 1 do
    s := s + Card.GetJSONFieldName (i) + ', ';

  if Length (s) > 0 then
    Delete (s, Length (s) - 1, 2);
  TJSONLabel9.Text := s;
end;

procedure BeforeObjectDisplay (Sender: TControl);
begin
  if Sender = TJSONLabel7 then
    TJSONLabel7.Text := TJSONLabel7.Text + ' - TJSONLabel7';
end;