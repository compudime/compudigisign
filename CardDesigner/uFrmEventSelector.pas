unit uFrmEventSelector;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Actions,
  FMX.ActnList, System.ImageList, FMX.ImgList, FMX.StdCtrls, FMX.Layouts,
  FMX.ListBox, FMX.Edit, FMX.Controls.Presentation;

type
  TFrmEventSelector = class(TForm)
    Label1: TLabel;
    edControlName: TEdit;
    Label2: TLabel;
    LstEvents: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ActionList1: TActionList;
    ImageList1: TImageList;
    actOK: TAction;
    actCancel: TAction;
    actCreateEvent: TAction;
    actDisassociateEvent: TAction;
    Button4: TButton;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure actOKExecute(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
    procedure actCreateEventExecute(Sender: TObject);
    procedure actDisassociateEventExecute(Sender: TObject);
  private
    { Private declarations }
    UserSelected: boolean;
  public
    { Public declarations }
    RequireCreateEvent: boolean;
  end;

var
  FrmEventSelector: TFrmEventSelector;

implementation

{$R *.fmx}

procedure TFrmEventSelector.FormShow(Sender: TObject);
begin
  UserSelected := LstEvents.ItemIndex = -1;
end;

procedure TFrmEventSelector.actCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFrmEventSelector.actCreateEventExecute(Sender: TObject);
begin
  ModalResult := mrContinue;
end;

procedure TFrmEventSelector.actDisassociateEventExecute(Sender: TObject);
begin
  ModalResult := mrRetry;
end;

procedure TFrmEventSelector.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  actOK.Enabled                := LstEvents.ItemIndex <> -1;
  actCreateEvent.Enabled       := (LstEvents.ItemIndex = -1) or UserSelected;
  actDisassociateEvent.Enabled := not UserSelected;
end;

procedure TFrmEventSelector.actOKExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
