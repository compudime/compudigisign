unit uFrmPreview;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  uCards, FMX.Controls.Presentation, FMX.Edit, FMX.EditBox, FMX.SpinBox;

const
  DefaultBackground = 'Images\Lago-Moraine-Parque-Nacional-Banff-Alberta-Canada-1440x810.jpg';

type
  TFrmPreview = class(TForm)
    StatusBar1: TStatusBar;
    lbPages: TLabel;
    pnlError: TPanel;
    lbError: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure ChangePage (Sender: TObject);
    procedure AnalyzeJSON (Sender: TObject; JSON: string; var RootItem: string; var DataDepth: integer);
  public
    { Public declarations }
    ImageBackground: TBackgroundImage;
    Grid: TGridCard;
    Showing: boolean;
    procedure UpdatePags;
    procedure ChangeGridError (Sender: TObject; ErrorMsg: string);
  end;

var
  FrmPreview: TFrmPreview;

implementation
uses uMainIDECards;

{$R *.fmx}

procedure TFrmPreview.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Grid.Stop;
  Showing                       := False;
  FMainDesigner.Settings.Spread := False;
end;

procedure TFrmPreview.FormCreate(Sender: TObject);
begin
  Top                        := Screen.Height - Height - 40;
  Left                       := Screen.Width  - Width  - 40;
  ImageBackground            := TBackgroundImage.Create(Self);
  ImageBackground.Parent     := Self;
  ImageBackground.Position.X := 0;
  ImageBackground.Position.Y := 0;
  ImageBackground.Width      := ClientWidth;
  ImageBackground.Height     := ClientHeight - 10;
  ImageBackground.Align      := TAlignLayout.Client;
  ImageBackground.Anchors    := [TAnchorKind.akTop, TAnchorKind.akLeft, TAnchorKind.akRight, TAnchorKind.akBottom];

  if FileExists (DefaultBackground) then
  begin
    ImageBackground.LoadBackgroundBitmap(DefaultBackground);
    ImageBackground.BackgroundType := btImage;
  end;

  Grid               := TGridCard.Create(Self);
  Grid.BeginUpdate;
  Grid.Parent        := ImageBackground;
  Grid.Position.X    := 0;
  Grid.Position.Y    := 0;
  Grid.Align         := TAlignLayout.Client;
  Grid.Visible       := True;
  Grid.OnChangePage  := ChangePage;
  Grid.OnAnalyzeJSON := AnalyzeJSON;
  Grid.OnGridError   := ChangeGridError;
  Showing            := False;
  Grid.EndUpdate;
  UpdatePags;
end;

procedure TFrmPreview.FormDestroy(Sender: TObject);
begin
  Grid.Free;
  ImageBackground.Free;
end;

procedure TFrmPreview.FormResize(Sender: TObject);
begin
  ImageBackground.PaintBackground;
end;

procedure TFrmPreview.FormShow(Sender: TObject);
begin
  Showing                       := True;
  FMainDesigner.Settings.Spread := True;
  Grid.Start;
end;

procedure TFrmPreview.ChangePage (Sender: TObject);
begin
  UpdatePags;
end;

procedure TFrmPreview.AnalyzeJSON (Sender: TObject; JSON: string; var RootItem: string; var DataDepth: integer);
begin
  uCards.AnalyzeJSON(JSON, RootItem, DataDepth);
end;

procedure TFrmPreview.UpdatePags;
begin
  lbPages.Text := Format ('%d/%d', [Grid.CurrentPage + 1, Grid.PageCount]);
end;

procedure TFrmPreview.ChangeGridError (Sender: TObject; ErrorMsg: string);
begin
  pnlError.Visible := not ErrorMsg.IsEmpty;
  lbError.Text     := ErrorMsg;
end;



end.
