unit uFrmRTML;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation, System.ImageList,
  FMX.ImgList, System.Actions, FMX.ActnList, uCards, Math, FMX.Memo.Types;

type
  TFrmRTMLPreview = class(TForm)
    Label1: TLabel;
    Source: TMemo;
    pnlRTML: TPanel;
    Button1: TButton;
    Button2: TButton;
    ImageList1: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SourceChangeTracking(Sender: TObject);
    procedure SourceChange(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    { Private declarations }
    RTML:     TJSONLabel;
    Resizing: boolean;

    procedure UpdateRTML;
  public
    { Public declarations }
  end;

var
  FrmRTMLPreview: TFrmRTMLPreview;

implementation

{$R *.fmx}

procedure TFrmRTMLPreview.FormCreate(Sender: TObject);
begin
  RTML             := TJSONLabel.Create(Self);
  RTML.Parent      := pnlRTML;
  RTML.Align       := TAlignLayout.Client;
  RTML.HitTest     := False;
  RTML.ParseToRTML := True;
  RTML.ShowHint    := True;
  RTML.Hint        := pnlRTML.Hint;
end;

procedure TFrmRTMLPreview.FormDestroy(Sender: TObject);
begin
  RTML.Free;
end;

procedure TFrmRTMLPreview.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if Math.InRange(pnlRTML.BoundsRect.Right  - X, 0, 15) and
     Math.InRange(pnlRTML.BoundsRect.Bottom - Y, 0, 15) then
  begin
    Cursor   := crSizeNWSE;
    Resizing := True;
  end
  else
  begin
    Cursor   := crArrow;
    Resizing := False;
  end;
end;

procedure TFrmRTMLPreview.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Single);
begin
  if Math.InRange(pnlRTML.BoundsRect.Right  - X, 0, 15) and
     Math.InRange(pnlRTML.BoundsRect.Bottom - Y, 0, 15) then
    Cursor := crSizeNWSE
  else
    Cursor := crArrow;

  if Resizing then
  begin
    pnlRTML.Size.Width  := X - pnlRTML.Position.X;
    pnlRTML.Size.Height := Y - pnlRTML.Position.Y;
  end;
end;

procedure TFrmRTMLPreview.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  Resizing := False;
  Cursor   := crArrow;
end;

procedure TFrmRTMLPreview.FormShow(Sender: TObject);
begin
  UpdateRTML;
end;

procedure TFrmRTMLPreview.SourceChange(Sender: TObject);
begin
  UpdateRTML;
end;

procedure TFrmRTMLPreview.SourceChangeTracking(Sender: TObject);
begin
   UpdateRTML;
end;

procedure TFrmRTMLPreview.UpdateRTML;
begin
  RTML.Text := Source.Lines.Text;
end;


end.
