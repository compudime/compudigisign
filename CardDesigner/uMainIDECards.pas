unit uMainIDECards;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  {$IFDEF MACOS}
  MacApi.Appkit,Macapi.CoreFoundation, Macapi.Foundation,
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Winapi.Messages, Winapi.Windows,
  {$ENDIF}
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, System.Actions,
  FMX.ActnList, System.ImageList, FMX.ImgList, FMX.Menus, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, System.TypInfo, FMX.Layouts,
  FMX.TreeView, FMX.Edit, FMX.ListBox, FMX.Colors, FMX.EditBox, FMX.SpinBox,
  uCards, FMX.Memo, FMX.Memo.Types, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.TabControl,
  System.JSON, REST.JSON, FMX.ScripterInit, atScript, atScripter,
  System.IOUtils, uFrmPreview, uFrmEventSelector, uFrmRTML, FMX.NumberBox;

type
  RPropertyValue = record
    Obj:   TObject;
    Prop:  TRttiProperty;
    Value: TValue;
  end;

  TTreeViewItemProp = class (TTreeViewItem)
  private
    PropName:      string;
    PropClassName: string;
    PropType:      TTypeKind;
    CtrlEdit:      TControl;
    Obj:           TObject;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TScriptInfo = class
  private
    FCode:             string;
    FFileName:         string;
    FChanged:          boolean;
    FOnChangeFileName: TNotifyEvent;
    procedure SetCode (Value: string);
    procedure SetFileName (Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile (AFileName: string);
    procedure SaveToFile (AFileName: string = '');
    property Code: string read FCode write SetCode;
    property FileName: string read FFileName write SetFileName;
    property Changed: boolean read FChanged write FChanged;
    property OnChangeFileName: TNotifyEvent read FOnChangeFileName write FOnChangeFileName;
  end;

  TWindowEventResult = (werCancel, werSelectExistent, werCreateNew, werDisassociate);

  TFMainDesigner = class(TForm)
    ActionList1: TActionList;
    MenuBar1: TMenuBar;
    ToolBar1: TToolBar;
    ImageListCommands: TImageList;
    actFileCardOpen: TAction;
    actFileCardSave: TAction;
    actFileCardSaveAs: TAction;
    actEditCopy: TAction;
    actEditPaste: TAction;
    actPaletteDeselectItem: TAction;
    actPaletteLabel: TAction;
    actPaletteImage: TAction;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    actAlignVertTop: TAction;
    actAlignVertMiddle: TAction;
    actAlignVertBottom: TAction;
    actAlignHorizLeft: TAction;
    actAlignHorizCenter: TAction;
    actAlignHorizRight: TAction;
    actPaletteLine: TAction;
    actPaletteRectangle: TAction;
    actPaletteRoundedRect: TAction;
    actPaletteEllipse: TAction;
    actPaletteCircle: TAction;
    actPaletteArc: TAction;
    TabControl1: TTabControl;
    tabCard: TTabItem;
    tabPascal: TTabItem;
    Panel6: TPanel;
    StringGrid1: TStringGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel4: TPanel;
    pnlDesign: TPanel;
    ToolBar2: TToolBar;
    Panel2: TPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    Panel1: TPanel;
    btnDeselectItem: TSpeedButton;
    btnLabel: TSpeedButton;
    btnImage: TSpeedButton;
    btnLine: TSpeedButton;
    btnRectangle: TSpeedButton;
    btnRoundRect: TSpeedButton;
    btnEllipse: TSpeedButton;
    btnCircle: TSpeedButton;
    btnArc: TSpeedButton;
    sbCard: TStatusBar;
    Splitter2: TSplitter;
    SaveDlg: TSaveDialog;
    OpenDlg: TOpenDialog;
    SpeedButton12: TSpeedButton;
    actFileCardNew: TAction;
    actPreviewDisplay: TAction;
    OpenImgDlg: TOpenDialog;
    actFileOpenJSON: TAction;
    OpenJSONDlg: TOpenDialog;
    actDeleteSelection: TAction;
    Panel5: TPanel;
    SpeedButton15: TSpeedButton;
    Panel8: TPanel;
    Panel9: TPanel;
    Splitter3: TSplitter;
    cbLstControls: TComboBox;
    Rectangle1: TRectangle;
    labTitleObjectInspector: TLabel;
    tvObjectInspector: TTreeView;
    Rectangle5: TRectangle;
    Label10: TLabel;
    tvCardStructure: TTreeView;
    StatusBar1: TStatusBar;
    Rectangle6: TRectangle;
    labVersion: TLabel;
    StatusBar2: TStatusBar;
    Rectangle8: TRectangle;
    sbPascalPanelCaret: TLabel;
    Rectangle9: TRectangle;
    sbPascalPanelCompileStatus: TLabel;
    MemoPascal: TMemo;
    actFilePascalOpen: TAction;
    actFilePascalSave: TAction;
    actFilePascalSaveAs: TAction;
    OpenPascalDlg: TOpenDialog;
    SavePascalDlg: TSaveDialog;
    actPascalCompile: TAction;
    actCardBackgroundImage: TAction;
    actPreviewOpenBackroundImage: TAction;
    actFileToggleCardScript: TAction;
    actFilePascalNewScript: TAction;
    actFileSaveAll: TAction;
    SpeedButton20: TSpeedButton;
    Panel10: TPanel;
    SpeedButton13: TSpeedButton;
    SpeedButton5: TSpeedButton;
    tabSettings: TTabItem;
    Rectangle11: TRectangle;
    Label7: TLabel;
    edCardFileName: TEdit;
    Label14: TLabel;
    edScriptFileName: TEdit;
    Panel11: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    spWidth: TSpinBox;
    spHeight: TSpinBox;
    Label11: TLabel;
    cbCardBackgroundType: TComboBox;
    Button1: TButton;
    Label3: TLabel;
    cbCardColor: TColorComboBox;
    Label4: TLabel;
    spOpacity: TSpinBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    spRows: TSpinBox;
    Label6: TLabel;
    spColumns: TSpinBox;
    cbPreviewAutoAdjust: TCheckBox;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    spGridTopMargin: TSpinBox;
    Label17: TLabel;
    spGridLeftMargin: TSpinBox;
    Label19: TLabel;
    spGridInnerHoriz: TSpinBox;
    Label20: TLabel;
    spGridInnerVert: TSpinBox;
    GroupBox4: TGroupBox;
    Label22: TLabel;
    edRESTServerAddress: TEdit;
    Label23: TLabel;
    edRESTRequest: TEdit;
    Label24: TLabel;
    spRESTInterval: TSpinBox;
    Label25: TLabel;
    cbTransitionType: TComboBox;
    Label26: TLabel;
    spTransitionInterval: TSpinBox;
    Label27: TLabel;
    edDeviceID: TEdit;
    ImageList32x32: TImageList;
    actFileProjectOpen: TAction;
    actFileProjectSave: TAction;
    OpenIniDlg: TOpenDialog;
    SaveIniDlg: TSaveDialog;
    actFileProjectSaveAs: TAction;
    actFileProjectNew: TAction;
    actFileSave: TAction;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    actFileNew: TAction;
    actFileOpen: TAction;
    actFileSaveAs: TAction;
    SpeedButton4: TSpeedButton;
    actFileSaveAsRelativePaths: TAction;
    SpeedButton16: TSpeedButton;
    actJSONRESTRequest: TAction;
    Label8: TLabel;
    spTransitionSpeed: TSpinBox;
    Label9: TLabel;
    edJSONFile: TEdit;
    Button3: TButton;
    Label16: TLabel;
    cbJSONSource: TComboBox;
    Button4: TButton;
    Panel12: TPanel;
    Panel7: TPanel;
    MemoJSON: TMemo;
    sbJSON: TStatusBar;
    Rectangle3: TRectangle;
    sbJSONPanelCaret: TLabel;
    Rectangle4: TRectangle;
    sbJSONPanelStatus: TLabel;
    Rectangle2: TRectangle;
    sbJSONPanel2: TLabel;
    Splitter4: TSplitter;
    StatusBar3: TStatusBar;
    Rectangle7: TRectangle;
    sbJSONDataTable: TLabel;
    Label18: TLabel;
    spMaxGridCards: TSpinBox;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    cbPreviewBackgroundType: TComboBox;
    Button2: TButton;
    Label13: TLabel;
    cbPreviewBackgroundColor: TColorComboBox;
    Label21: TLabel;
    edImageFileName: TEdit;
    btnSetClr: TButton;
    ComboColorBox_backround: TComboColorBox;
    edtBackrndColor: TEdit;
    Label28: TLabel;
    lbl1: TLabel;
    spTransitionSteps: TSpinBox;
    lbl2: TLabel;
    cbAutoAdjustCardSize: TCheckBox;
    GroupBoxPlayer: TGroupBox;
    spPlayerMonitor: TSpinBox;
    Label29: TLabel;
    procedure actPaletteDeselectItemExecute(Sender: TObject);
    procedure actPaletteLabelExecute(Sender: TObject);
    procedure actPaletteImageExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actAlignHorizLeftExecute(Sender: TObject);
    procedure actAlignHorizCenterExecute(Sender: TObject);
    procedure actAlignHorizRightExecute(Sender: TObject);
    procedure actAlignVertTopExecute(Sender: TObject);
    procedure actAlignVertMiddleExecute(Sender: TObject);
    procedure actAlignVertBottomExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure cbLstControlsChange(Sender: TObject);
    procedure actPaletteLineExecute(Sender: TObject);
    procedure actPaletteRectangleExecute(Sender: TObject);
    procedure actPaletteRoundedRectExecute(Sender: TObject);
    procedure actPaletteEllipseExecute(Sender: TObject);
    procedure actPaletteCircleExecute(Sender: TObject);
    procedure actPaletteArcExecute(Sender: TObject);
    procedure spWidthChange(Sender: TObject);
    procedure spHeightChange(Sender: TObject);
    procedure cbCardColorChange(Sender: TObject);
    procedure spOpacityChange(Sender: TObject);
    procedure MemoJSONChange(Sender: TObject);
    procedure MemoJSONKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure MemoJSONKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure MemoJSONMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure MemoJSONMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure MemoJSONMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure MemoJSONEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actFileCardSaveExecute(Sender: TObject);
    procedure actFileCardOpenExecute(Sender: TObject);
    procedure actFileCardSaveAsExecute(Sender: TObject);
    procedure actFileCardNewExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure spRowsChange(Sender: TObject);
    procedure spColumnsChange(Sender: TObject);
    procedure actPreviewDisplayExecute(Sender: TObject);
    procedure actFileOpenJSONExecute(Sender: TObject);
    procedure edJSONRootItemChange(Sender: TObject);
    procedure spJSONDepthChange(Sender: TObject);
    procedure actDeleteSelectionExecute(Sender: TObject);
    procedure tvCardStructureMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure tvCardStructureMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure tvCardStructureMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure cbPreviewAutoAdjustChange(Sender: TObject);
    procedure MemoPascalEnter(Sender: TObject);
    procedure MemoPascalKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure MemoPascalKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure MemoPascalMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure MemoPascalMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure MemoPascalMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure MemoPascalChange(Sender: TObject);
    procedure actFilePascalOpenExecute(Sender: TObject);
    procedure actFilePascalSaveExecute(Sender: TObject);
    procedure actFilePascalSaveAsExecute(Sender: TObject);
    procedure actPascalCompileExecute(Sender: TObject);
    procedure cbCardBackgroundTypeChange(Sender: TObject);
    procedure actCardBackgroundImageExecute(Sender: TObject);
    procedure cbPreviewBackgroundTypeChange(Sender: TObject);
    procedure cbPreviewBackgroundColorChange(Sender: TObject);
    procedure actPreviewOpenBackroundImageExecute(Sender: TObject);
    procedure MemoJSONChangeTracking(Sender: TObject);
    procedure MemoPascalChangeTracking(Sender: TObject);
    procedure actFileToggleCardScriptExecute(Sender: TObject);
    procedure actFilePascalNewScriptExecute(Sender: TObject);
    procedure actFileSaveAllExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spGridTopMarginChange(Sender: TObject);
    procedure spGridLeftMarginChange(Sender: TObject);
    procedure spGridInnerHorizChange(Sender: TObject);
    procedure spGridInnerVertChange(Sender: TObject);
    procedure actFileProjectOpenExecute(Sender: TObject);
    procedure actFileProjectSaveExecute(Sender: TObject);
    procedure actFileProjectSaveAsExecute(Sender: TObject);
    procedure actFileProjectNewExecute(Sender: TObject);
    procedure edRESTServerAddressChange(Sender: TObject);
    procedure edRESTRequestChange(Sender: TObject);
    procedure spRESTIntervalChange(Sender: TObject);
    procedure cbTransitionTypeChange(Sender: TObject);
    procedure spTransitionIntervalChange(Sender: TObject);
    procedure edDeviceIDChange(Sender: TObject);
    procedure actFileNewExecute(Sender: TObject);
    procedure actFileOpenExecute(Sender: TObject);
    procedure actFileSaveExecute(Sender: TObject);
    procedure actFileSaveAsExecute(Sender: TObject);
    procedure actFileSaveAsRelativePathsExecute(Sender: TObject);
    procedure actJSONRESTRequestExecute(Sender: TObject);
    procedure btnSetClrClick(Sender: TObject);
    procedure spTransitionSpeedChange(Sender: TObject);
    procedure edJSONFileChange(Sender: TObject);
    procedure cbJSONSourceChange(Sender: TObject);
    procedure ComboColorBox_backroundChange(Sender: TObject);
    procedure spMaxGridCardsChange(Sender: TObject);
    procedure spTransitionStepsChange(Sender: TObject);
    procedure cbAutoAdjustCardSizeChange(Sender: TObject);
    procedure spPlayerMonitorChange(Sender: TObject);
  private
    { Private declarations }
    FDisplayGrid:        boolean;
    FGridSize:           integer;
    FLoadingCard:        boolean;
    Card:                TJSONCard;
    FCardFileName:       string;
    ObjectInspectorCtrl: TControl;
    ReloadingCtrl:       boolean;
    LoadingPascal:       boolean;
    Script:              TScriptInfo;
    FSettings:           TProyectSettings;

    //General routines
    procedure HandlerProcedure (Sender: TObject; E: Exception);

    //Properties setters and getters
    procedure SetCardFileName (Value: string);

    //Object inspector routines
    procedure PopulateComboPropValues (Obj: TObject; Prop: TRttiProperty; Cb: TComboBox);
    procedure PopulateComboFontNames (CB: TComboBox);
    procedure PopulateComboJSONFields (CB: TComboBox);
    procedure PopulateComboParents (Ctrl: TControl; CB: TComboBox);
    procedure PopulateComboEvents (Ctrl: TControl; CB: TComboBox; EventName: string);
    procedure LaunchEventsWindow (Ctrl: TControl; EventName, PropValue: string; var EventAction: TWindowEventResult; var Value: string);
    function GetPropertyValue (Ctx: TRttiContext; Item: TFMXObject; var PropVal: RPropertyValue): string;
    procedure UpdateCBLstControl;
    procedure ReadCtrlProperties (Ctrl: TControl);
    procedure EditChangeProperty(Sender: TObject);
    procedure ColorComboBoxChangeProperty(Sender: TObject);
    procedure ComboBoxChangeProperty(Sender: TObject);
    procedure ParentComboBoxChangeProperty(Sender: TObject);
    procedure ButtonImageProperty(Sender: TObject);
    procedure ButtonEventProperty(Sender: TObject);
    procedure ButtonRTMLTextProperty(Sender: TObject);
    procedure CheckSetChange (Sender: TObject);
    function BuildPropertySet (Item: TTreeViewItem): string;

    //Card routines and events
    procedure ChangePaletteTool (Sender: TObject);
    procedure ChangeSelection (Sender: TObject);
    procedure ControlNotify (Sender: TObject; Ctrl: TControl; AOperation: TOperation);
    procedure CardChange (Sender: TObject);
    procedure ControlChangeName(Sender: TObject; Ctrl: TControl; OldName, NewName: string);
    procedure GetStructureImageIndex (Sender: TObject; Tool: TPaletteTool; var ImageIndex: integer);
    procedure PreviewGridChangeColRows (Sender: TObject);
    procedure LoadCard;
    procedure ClearCard;
    procedure OpenCard (FileName: string);

    //JSON source routines
    procedure UpdateJSON;
    procedure UpdateJSONCaret;

    //Pascal routines
    procedure UpdatePascalCaret;
    procedure UpdatePascalCode;
    procedure LoadPascalCodeFromFile (FileName: string);
    procedure LoadPascalCode (Code: string; FileName: string = '');
    procedure ChangePascalFileName (Sender: TObject);

    //Project routines
    procedure ReloadProject;
    procedure ChangeProjectFileName (Sender: TObject);
    procedure UpdateCardSize(AWidth,AHeight : boolean);
  protected
    procedure Notification (AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    property CardFileName: string read FCardFileName write SetCardFileName;
    property Settings: TProyectSettings read FSettings;
  end;

var
  FMainDesigner: TFMainDesigner;

implementation
{$R *.fmx}

const
  TabSettingsCaption = 'Project';
  TabCardCaption     = 'Card';
  TabScriptCaption   = 'Pascal script';

  PascalTemplate = 'procedure BeforeDisplayCard;'                      + sLineBreak +
                   'begin'                                             + sLineBreak +
                   'end;';

{$region 'Font list'}
{$IFDEF MSWINDOWS}
function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
  FontType: Integer; Data: Pointer): Integer; stdcall;
var
  S: TStrings;
  Temp: string;
begin
  S := TStrings(Data);
  Temp := LogFont.lfFaceName;
  if (S.Count = 0) or (AnsiCompareText(S[S.Count-1], Temp) <> 0) then
    S.Add(Temp);
  Result := 1;
end;
{$ENDIF}

procedure CollectFonts(FontList: TStringList);
var
{$IFDEF MACOS}
  fManager: NsFontManager;
  list:NSArray;
  lItem:NSString;
  i: Integer;
{$ENDIF}
{$IFDEF MSWINDOWS}
  DC: HDC;
  LFont: TLogFont;
{$ENDIF}
begin
  {$IFDEF MACOS}
    fManager := TNsFontManager.Wrap(TNsFontManager.OCClass.sharedFontManager);
    list := fManager.availableFontFamilies;
    if (List <> nil) and (List.count > 0) then
    begin
      for i := 0 to List.Count-1 do
      begin
        lItem := TNSString.Wrap(List.objectAtIndex(i));
        FontList.Add(String(lItem.UTF8String))
      end;
    end;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
    DC := GetDC(0);
    FillChar(LFont, sizeof(LFont), 0);
    LFont.lfCharset := DEFAULT_CHARSET;
    EnumFontFamiliesEx(DC, LFont, @EnumFontsProc, Winapi.Windows.LPARAM(FontList), 0);
    ReleaseDC(0, DC);
  {$ENDIF}
end;
{$endregion}

{$region 'Version routines'}
function GetAppVersionStr: string;
var
{$IFDEF MACOS}
  CFStr: CFStringRef;
  Range: CFRange;
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
{$ENDIF}
begin
{$IFDEF MACOS}
  CFStr := CFBundleGetValueForInfoDictionaryKey(
    CFBundleGetMainBundle, kCFBundleVersionKey);
  Range.location := 0;
  Range.length := CFStringGetLength(CFStr);
  SetLength(Result, Range.length);
  CFStringGetCharacters(CFStr, Range, PChar(Result));
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
{$ENDIF}
end;
{$endregion}

{$region 'TTreeViewItemProp'}
constructor TTreeViewItemProp.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  PropName      := '';;
  PropClassName := '';
  PropType      := tkUnknown;
  Obj           := nil;
  CtrlEdit      := nil;
end;

destructor TTreeViewItemProp.Destroy;
begin
  PropName      := '';;
  PropClassName := '';
  Obj           := nil;
  CtrlEdit      := nil;

  inherited Destroy;
end;
{$endregion}

{$region 'TScriptInfo'}
constructor TScriptInfo.Create;
begin
  FCode     := '';
  FFileName := '';
  FChanged  := False;
end;

destructor TScriptInfo.Destroy;
begin
  FCode     := '';
  FFileName := '';

  inherited Destroy;
end;

procedure TScriptInfo.SetCode (Value: string);
begin
  if FCode <> Value then
  begin
    FCode   := Value;
    FChanged := True;
  end;
end;

procedure TScriptInfo.SetFileName (Value: string);
begin
  if FFileName <> Value then
  begin
    FFileName := Value;

    if Assigned (FOnChangeFileName) then
      FOnChangeFileName (Self);
  end;
end;

procedure TScriptInfo.LoadFromFile (AFileName: string);
begin
  try
    FCode     := TFile.ReadAllText(AFileName);
    FChanged  := False;
    FFileName := AFileName;
  except

  end;
end;

procedure TScriptInfo.SaveToFile (AFileName: string = '');
begin
  try
    if AFileName.IsEmpty then
      AFileName := FFileName;

    if AFileName.IsEmpty then
      raise Exception.Create('FileName required');

    TFile.WriteAllText(AFileName, FCode);
    FFileName := AFileName;
    FChanged  := False;
  except

  end;
end;
{$endregion}

{$region 'TFMainDesigner'}

{$region 'Form events'}
procedure TFMainDesigner.FormCreate(Sender: TObject);
begin
  labVersion.Text         := 'Card Designer ' + GetAppVersionStr;
  Application.OnException := HandlerProcedure;

  Card                          := TJSONCard.Create(Self);
  Card.Name                     := 'Card';
  Card.Parent                   := pnlDesign;
  Card.Position.X               := 0;
  Card.Position.Y               := ToolBar2.Height;
  Card.Height                   := 250;
  Card.Width                    := 350;
  Card.DesignMode               := True;
  Card.CurrentTool              := ptDeselect;
  Card.OnChangeCurrentTool      := ChangePaletteTool;
  Card.OnChangeCtrlSelection    := ChangeSelection;
  Card.OnControlNotification    := ControlNotify;
  Card.OnChange                 := CardChange;
  Card.OnControlChangeName      := ControlChangeName;
  Card.OnGetStructureImageIndex := GetStructureImageIndex;
  Card.TreeStructure            := tvCardStructure;

  TabControl1.ActiveTab      := tabSettings;
  FDisplayGrid               := True;
  ReloadingCtrl              := False;
  FGridSize                  := 5;
  ObjectInspectorCtrl        := nil;
  FLoadingCard               := True;
  spWidth.Value              := Card.Width;
  spHeight.Value             := Card.Height;
  cbCardColor.Color          := Card.BackgroundColor;
  spOpacity.Value            := Card.Opacity * 100;
  FLoadingCard               := False;
  LoadingPascal              := False;
  Script                     := TScriptInfo.Create;
  Script.OnChangeFileName    := ChangePascalFileName;
  FSettings                  := TProyectSettings.Create (Self);
  FSettings.Spread           := False;
  FSettings.OnChangeFileName := ChangeProjectFileName;

  UpdateJSON;
end;

procedure TFMainDesigner.FormDestroy(Sender: TObject);
begin
  FSettings.Free;
  Script.Free;
  Card.Free;
  Halt (0);
end;

procedure TFMainDesigner.FormShow(Sender: TObject);
begin
  FrmPreview.Grid.OnRecalculeColRows := PreviewGridChangeColRows;
  FSettings.Grid                     := FrmPreview.Grid;
  actPaletteLabel.Execute;
  actPaletteDeselectItem.Execute;
  actFileCardNew.Execute;
end;

procedure TFMainDesigner.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMainDesigner.Settings.Spread := False;
end;

procedure TFMainDesigner.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Settings.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Card project has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileProjectSave.Execute;
      mrCancel: CanClose := False;
    end;
  end;

  if Card.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Card template has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileCardSave.Execute;
      mrCancel: CanClose := False;
    end;
  end;

  if Script.Changed then
  begin
    TabControl1.ActiveTab := tabPascal;
    case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFilePascalSave.Execute;
      mrCancel: CanClose := False;
    end;
  end;

  if CanClose and FrmPreview.Showing then
  begin
    FrmPreview.Grid.Stop;
    FrmPreview.Close;
  end;
end;

procedure TFMainDesigner.Notification (AComponent: TComponent; Operation: TOperation);
begin
  if (Operation = opRemove) and (AComponent = ObjectInspectorCtrl) then
    ObjectInspectorCtrl := nil;
end;

procedure TFMainDesigner.HandlerProcedure (Sender: TObject; E: Exception);
begin

end;
{$endregion}

{$region 'File commands'}
procedure TFMainDesigner.actFileCardNewExecute(Sender: TObject);
begin
  if Card.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Card template has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileCardSave.Execute;
      mrCancel: exit;
    end;
  end;

  ClearCard;

  if Script.Changed then
  begin
    TabControl1.ActiveTab := tabPascal;
    case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFilePascalSave.Execute;
      mrCancel: exit;
    end;
  end;

  LoadPascalCode (PascalTemplate);
  Script.Changed := False;
end;

procedure TFMainDesigner.actFileCardOpenExecute(Sender: TObject);
var
  PasFileName: string;
  RootItem: string;
  DataDepth: integer;
begin
  if Card.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Card template has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileCardSave.Execute;
      mrCancel: exit;
    end;
  end;

  if OpenDlg.Execute then
  begin
    OpenCard(OpenDlg.FileName);

    PasFileName := TPath.GetFileNameWithoutExtension(OpenDlg.FileName) + '.pas';
    PasFileName := ExtractFilePath(OpenDlg.FileName) + PasFileName;
    if FileExists (PasFileName) then
    begin
      if Script.Changed then
      begin
        TabControl1.ActiveTab := tabPascal;
        case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                    TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
          mrYes:    actFilePascalSave.Execute;
          mrCancel: exit;
        end;
      end;

      LoadPascalCodeFromFile(PasFileName);
    end;
  end;
end;

procedure TFMainDesigner.actFileCardSaveAsExecute(Sender: TObject);
begin
  if SaveDlg.Execute then
  begin
    Card.SaveToFile (SaveDlg.FileName);
    CardFileName := SaveDlg.FileName;
  end;
end;

procedure TFMainDesigner.actFileCardSaveExecute(Sender: TObject);
begin
  if CardFileName <> '' then
    Card.SaveToFile(CardFileName)
  else
    actFileCardSaveAs.Execute;
end;

procedure TFMainDesigner.SetCardFileName (Value: string);
begin
  if FCardFileName <> Value then
  begin
    FCardFileName := Value;

    if FCardFileName.IsEmpty then
    begin
      tabCard.Text          := TabCardCaption + ' - [New card]';
      edCardFileName.Text   := '[New card]';
      Settings.CardFileName := '';
    end
    else
    begin
      tabCard.Text          := Format ('%s - [%s]', [TabCardCaption, ExtractFileName(FCardFileName)]);
      edCardFileName.Text   := FCardFileName;
      Settings.CardFileName := FCardFileName;
    end;
  end;
end;
{$endregion}

{$region 'Card design'}
procedure TFMainDesigner.actAlignHorizCenterExecute(Sender: TObject);
begin
  Card.AlignHorizCenter;
end;

procedure TFMainDesigner.actAlignHorizLeftExecute(Sender: TObject);
begin
  Card.AlignHorizLeft;
end;

procedure TFMainDesigner.actAlignHorizRightExecute(Sender: TObject);
begin
  Card.AlignHorizRight;
end;

procedure TFMainDesigner.actAlignVertBottomExecute(Sender: TObject);
begin
  Card.AlignVertBottom;
end;

procedure TFMainDesigner.actAlignVertMiddleExecute(Sender: TObject);
begin
  Card.AlignVertMiddle;
end;

procedure TFMainDesigner.actAlignVertTopExecute(Sender: TObject);
begin
  Card.AlignVertTop;
end;

procedure TFMainDesigner.actCardBackgroundImageExecute(Sender: TObject);
begin
  if OpenImgDlg.Execute then
    Card.LoadBackgroundFile(OpenImgDlg.FileName);
end;

procedure TFMainDesigner.actDeleteSelectionExecute(Sender: TObject);
begin
  Card.DeleteSelectedControls;
end;

procedure TFMainDesigner.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  FlagAlign, FlagGralFile: boolean;
begin
  FlagAlign                   := (Card.SelectedCtrls.Count > 1) and (TabControl1.ActiveTab = tabCard);
  FlagGralFile                := (TabControl1.ActiveTab = tabSettings) or (TabControl1.ActiveTab = tabCard) or (TabControl1.ActiveTab = tabPascal);
  actAlignVertTop.Enabled     := FlagAlign;
  actAlignVertMiddle.Enabled  := FlagAlign;
  actAlignVertBottom.Enabled  := FlagAlign;
  actAlignHorizLeft.Enabled   := FlagAlign;
  actAlignHorizCenter.Enabled := FlagAlign;
  actAlignHorizRight.Enabled  := FlagAlign;
  actFileNew.Enabled          := FlagGralFile;
  actFileOpen.Enabled         := FlagGralFile;
  actFileSaveAs.Enabled       := FlagGralFile;
  actFileSave.Enabled         := ((TabControl1.ActiveTab = tabSettings) and Settings.Changed) or ((TabControl1.ActiveTab = tabCard) and Card.Changed) or ((TabControl1.ActiveTab = tabPascal) and Script.Changed);
  actFileSaveAll.Enabled      := Settings.Changed or Card.Changed or Script.Changed;
  actFileCardSave.Enabled     := Card.Changed;
  actFilePascalSave.Enabled   := Script.Changed;
  actFilePascalOpen.Enabled   := Assigned (Script);
  actFilePascalSaveAs.Enabled := Assigned (Script);
  actPascalCompile.Enabled    := (TabControl1.ActiveTab = tabPascal) and (MemoPascal.Lines.Text <> '');
  actPreviewDisplay.Enabled   := not FrmPreview.Showing;
  actDeleteSelection.Enabled  := (Card.SelectedCtrls.Count > 0) and (TabControl1.ActiveTab = tabCard);
  actFileProjectSave.Enabled  := Settings.Changed;
  spColumns.Enabled           := not cbPreviewAutoAdjust.IsChecked;
  spRows.Enabled              := not cbPreviewAutoAdjust.IsChecked;
//  spGridTopMargin.Enabled     := not cbPreviewAutoAdjust.IsChecked;
//  spGridLeftMargin.Enabled    := not cbPreviewAutoAdjust.IsChecked;
  tabSettings.ImageIndex      := Ord (Settings.Changed) - 1;
  tabCard.ImageIndex          := Ord (Card.Changed)     - 1;
  tabPascal.ImageIndex        := Ord (Script.Changed)   - 1;
  actFileSaveAsRelativePaths.Enabled := not Settings.CardFileName.IsEmpty or not Settings.ScriptFileName.IsEmpty or not Settings.BackgroundFileName.IsEmpty;
end;

procedure TFMainDesigner.actJSONRESTRequestExecute(Sender: TObject);
var
  RESTRequest: TRESTJSONServiceClient;
  RootItem:    string;
  DataDepth:   integer;
begin
  if Settings.RESTServerAddress.IsEmpty or Settings.RESTRequest.IsEmpty then
  begin
    FMX.Dialogs.MessageDlg('REST Server Address and Request to run are required',
                           TMsgDlgType.mtInformation, [TMsgDlgBtn.mbOK], 0);

    TabControl1.ActiveTab := tabSettings;

    if Settings.RESTServerAddress.IsEmpty then
      edRESTServerAddress.SetFocus
    else if Settings.RESTRequest.IsEmpty then
      edRESTRequest.SetFocus;

    exit;
  end;

  RESTRequest                := TRESTJSONServiceClient.Create (Self);
  RESTRequest.BaseURL        := Settings.RESTServerAddress;
  RESTRequest.ServiceRequest := Settings.RESTRequest;
  MemoJSON.Lines.Text        := RESTRequest.Execute;
  RESTRequest.Free;
  UpdateJSON;
end;

procedure TFMainDesigner.actPaletteArcExecute(Sender: TObject);
begin
  actPaletteArc.Checked := True;
  btnArc.StaysPressed   := True;
  Card.CurrentTool      := ptArc;
end;

procedure TFMainDesigner.actPaletteCircleExecute(Sender: TObject);
begin
  actPaletteCircle.Checked := True;
  btnCircle.StaysPressed   := True;
  Card.CurrentTool         := ptCircle;
end;

procedure TFMainDesigner.actPaletteDeselectItemExecute(Sender: TObject);
begin
  actPaletteDeselectItem.Checked := True;
  btnDeselectItem.StaysPressed   := True;
  Card.CurrentTool               := ptDeselect;
end;

procedure TFMainDesigner.actPaletteEllipseExecute(Sender: TObject);
begin
  actPaletteEllipse.Checked := True;
  btnEllipse.StaysPressed   := True;
  Card.CurrentTool          := ptEllipse;
end;

procedure TFMainDesigner.actPaletteImageExecute(Sender: TObject);
begin
  actPaletteImage.Checked := True;
  btnImage.StaysPressed   := True;
  Card.CurrentTool        := ptImage;
end;

procedure TFMainDesigner.actPaletteLabelExecute(Sender: TObject);
begin
  actPaletteLabel.Checked := True;
  btnLabel.StaysPressed   := True;
  Card.CurrentTool        := ptLabel;
end;

procedure TFMainDesigner.actPaletteLineExecute(Sender: TObject);
begin
  actPaletteLine.Checked := True;
  btnLine.StaysPressed   := True;
  Card.CurrentTool       := ptLine;
end;

procedure TFMainDesigner.actPaletteRectangleExecute(Sender: TObject);
begin
  actPaletteRectangle.Checked := True;
  btnRectangle.StaysPressed   := True;
  Card.CurrentTool            := ptRectangle;
end;

procedure TFMainDesigner.actPaletteRoundedRectExecute(Sender: TObject);
begin
  actPaletteRoundedRect.Checked := True;
  btnRoundRect.StaysPressed     := True;
  Card.CurrentTool              := ptRoundedRect;
end;

procedure TFMainDesigner.cbAutoAdjustCardSizeChange(Sender: TObject);
begin
  Settings.AutoAdjustCardSize := cbAutoAdjustCardSize.IsChecked;
  UpdateCardSize(true,true);
  spWidth.Enabled   := not cbAutoAdjustCardSize.IsChecked;
  spHeight.Enabled  := not cbAutoAdjustCardSize.IsChecked;
end;

procedure TFMainDesigner.cbCardBackgroundTypeChange(Sender: TObject);
begin
  if FLoadingCard then exit;

  case cbCardBackgroundType.ItemIndex of
    0: Card.BackgroundType := TBackgroundType.btColor;
    1: Card.BackgroundType := TBackgroundType.btImage;
  end;
end;

procedure TFMainDesigner.cbCardColorChange(Sender: TObject);
begin
  if FLoadingCard then exit;

  Card.BackgroundColor := cbCardColor.Color;
end;

procedure TFMainDesigner.spColumnsChange(Sender: TObject);
begin
  Settings.ColCount := Trunc (spColumns.Value);
  UpdateCardSize(true, false);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spMaxGridCardsChange(Sender: TObject);
begin
  Settings.MaxGridCards := Trunc (spMaxGridCards.Value);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spGridInnerHorizChange(Sender: TObject);
begin
  Settings.HorizInnerMargin := Trunc (spGridInnerHoriz.Value);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spGridLeftMarginChange(Sender: TObject);
begin
  Settings.LeftMargin := Trunc (spGridLeftMargin.Value);
  UpdateCardSize(true, false);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spGridTopMarginChange(Sender: TObject);
begin
  Settings.TopMargin := Trunc (spGridTopMargin.Value);
  UpdateCardSize(false, true);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spHeightChange(Sender: TObject);
begin
  if FLoadingCard then exit;

  Card.CardHeight := spHeight.Value;
end;

procedure TFMainDesigner.spGridInnerVertChange(Sender: TObject);
begin
  Settings.VertInnerMargin := Trunc (spGridInnerVert.Value);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.cbPreviewAutoAdjustChange(Sender: TObject);
begin
  Settings.AutoAdjust := cbPreviewAutoAdjust.IsChecked;
  cbAutoAdjustCardSize.Enabled := not cbPreviewAutoAdjust.IsChecked;
  if not cbAutoAdjustCardSize.Enabled then
    cbAutoAdjustCardSize.IsChecked := False;
end;

procedure TFMainDesigner.actPreviewDisplayExecute(Sender: TObject);
var
  mem:       TMemoryStream;
  RootItem:   string;
  DataDepth: integer;
begin
  mem := TMemoryStream.Create;
  FrmPreview.Grid.BeginUpdate;
  try
    if Settings.RESTServerAddress.IsEmpty and Settings.RESTRequest.IsEmpty then
    begin
      AnalyzeJSON(MemoJSON.Lines.Text, RootItem, DataDepth);
      FrmPreview.Grid.JSONDataSet.ScanDepth      := DataDepth;
      FrmPreview.Grid.JSONDataSet.RootPath       := RootItem;
      FrmPreview.Grid.JSONDataSet.JSONSource     := MemoJSON.Lines.Text;
    end;
    FrmPreview.Grid.Script := Script.Code;
    Settings.Spread        := True;
    Card.SaveToStream(mem);
    mem.Position := 0;
    FrmPreview.Grid.LoadTemplate(mem);
  finally
    mem.Free;
    FrmPreview.Grid.EndUpdate;
    try
      FrmPreview.Grid.Update;
    except

    end;
  end;
  FrmPreview.Show;
end;

procedure TFMainDesigner.spOpacityChange(Sender: TObject);
begin
  if FLoadingCard then exit;

  Card.CardOpacity := spOpacity.Value / 100;
end;

procedure TFMainDesigner.spPlayerMonitorChange(Sender: TObject);
begin
  Settings.PlayerMonitorNum := Trunc (spPlayerMonitor.Value);
end;

procedure TFMainDesigner.spRowsChange(Sender: TObject);
begin
  Settings.RowCount := Trunc (spRows.Value);
  UpdateCardSize(false,true);
  if FrmPreview.Showing then
    FrmPreview.ImageBackground.PaintBackground;
end;

procedure TFMainDesigner.spTransitionIntervalChange(Sender: TObject);
begin
  Settings.TransitionInterval := Trunc (spTransitionInterval.Value);
end;

procedure TFMainDesigner.spTransitionSpeedChange(Sender: TObject);
begin
  Settings.TransitionSpeed := Trunc (spTransitionSpeed.Value);
end;

procedure TFMainDesigner.spWidthChange(Sender: TObject);
begin
  if FLoadingCard then exit;

  Card.CardWidth := spWidth.Value;
end;

procedure TFMainDesigner.cbPreviewBackgroundColorChange(Sender: TObject);
begin
   ComboColorBox_backround.Color :=  cbPreviewBackgroundColor.Color ;
end;

procedure TFMainDesigner.cbPreviewBackgroundTypeChange(Sender: TObject);
begin
  Settings.BackgroundType                   := TBackgroundType (cbPreviewBackgroundType.ItemIndex);
  FrmPreview.ImageBackground.BackgroundType := Settings.BackgroundType;
end;

procedure TFMainDesigner.cbTransitionTypeChange(Sender: TObject);
begin
  Settings.TransitionType := TTransitionType (cbTransitionType.ItemIndex);
end;

procedure TFMainDesigner.actPreviewOpenBackroundImageExecute(Sender: TObject);
begin
  if OpenImgDlg.Execute then
  begin
    Settings.BackgroundFileName := OpenImgDlg.FileName;
    edImageFileName.Text        := OpenImgDlg.FileName;
    FrmPreview.ImageBackground.LoadBackgroundBitmap(OpenImgDlg.FileName);
  end;
end;

procedure TFMainDesigner.edRESTRequestChange(Sender: TObject);
begin
  Settings.RESTRequest := edRESTRequest.Text;
end;

procedure TFMainDesigner.edRESTServerAddressChange(Sender: TObject);
begin
  Settings.RESTServerAddress := edRESTServerAddress.Text;
end;

procedure TFMainDesigner.spRESTIntervalChange(Sender: TObject);
begin
  Settings.RESTInterval := Trunc(spRESTInterval.Value);
end;

procedure TFMainDesigner.edJSONFileChange(Sender: TObject);
begin
  Settings.JSONFileName := edJSONFile.Text;
end;

procedure TFMainDesigner.cbJSONSourceChange(Sender: TObject);
begin
  if TJSONSourceType (cbJSONSource.ItemIndex) = jtManual then
    Settings.JSONSource := MemoJSON.Lines.Text;

  Settings.JSONSourceType := TJSONSourceType (cbJSONSource.ItemIndex);
  frmPreview.UpdatePags;
end;

procedure TFMainDesigner.edDeviceIDChange(Sender: TObject);
begin
  Settings.DeviceID := edDeviceID.Text;
end;

procedure TFMainDesigner.tvCardStructureMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  Item: TTreeViewItem;
begin
  tvCardStructure.Cursor := crArrow;
  Item := tvCardStructure.ItemByPoint(X, Y);
  if Assigned (Item) and Item.IsSelected then
  begin
    tvCardStructure.Cursor    := crMultiDrag;
    tvCardStructure.AllowDrag := True;
    tvCardStructure.Tag       := 1;
  end;
end;

procedure TFMainDesigner.tvCardStructureMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Single);
var
  Item: TTreeViewItem;
begin
  if tvCardStructure.Tag = 0 then exit;

  Item := tvCardStructure.ItemByPoint(X, Y);
  if Assigned (Item) and not Item.IsSelected then
    tvCardStructure.Cursor := crMultiDrag
  else
  begin
    tvCardStructure.Cursor := crNoDrop;
  end;
end;

procedure TFMainDesigner.tvCardStructureMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  tvCardStructure.Cursor    := crArrow;
  tvCardStructure.AllowDrag := False;
end;
{$endregion}

{$region 'Object Inspector'}
procedure TFMainDesigner.cbLstControlsChange(Sender: TObject);
var
  Idx: integer;
begin
  if Card.UpdatingSelection then exit;

  Idx := cbLstControls.ItemIndex;
  Card.ClearSelection;

  if Idx = -1 then exit;

  Card.AddSelection (TControl (cbLstControls.Items.Objects [Idx]));
end;

procedure TFMainDesigner.UpdateCardSize(AWidth, AHeight: boolean);
begin
  if cbAutoAdjustCardSize.IsChecked then
  begin
    if AWidth then
      spWidth.Value  := Round(Screen.Width/spColumns.Value) - (spColumns.Value * spGridLeftMargin.Value);
    if AHeight then
      spHeight.Value := Round(Screen.Height/spRows.Value) - (spRows.Value * spGridTopMargin.Value);
  end;
end;

procedure TFMainDesigner.UpdateCBLstControl;
var
  i: integer;
begin
  if Card.Selected = nil then
    cbLstControls.ItemIndex := -1
  else
  begin
    for i := 0 to cbLstControls.Items.Count - 1 do
      if cbLstControls.Items.Objects [i] = Card.Selected then
      begin
        cbLstControls.ItemIndex := i;
        exit;
      end;
  end;
end;

function TFMainDesigner.GetPropertyValue (Ctx: TRttiContext; Item: TFMXObject; var PropVal: RPropertyValue): string;
var
  Path:    TList;
  Curr:    TFMXObject;
  i:       integer;
  Node:    TTreeViewItem;
  ObjType: TRttiType;
begin
  result        := '';
  PropVal.Obj   := nil;
  PropVal.Prop  := nil;
  PropVal.Value := nil;

  Path := TList.Create;
  try
    Curr := Item;
    while Curr <> tvObjectInspector do
    begin
      if Curr is TTreeViewItem then
        Path.Insert(0, Curr);

      Curr := Curr.Parent;
    end;

    PropVal.Obj := ObjectInspectorCtrl;
    for i := 0 to Path.Count - 1 do
    begin
      Node          := TTreeViewItem (Path [i]);
      ObjType       := Ctx.GetType(PropVal.Obj.ClassInfo);
      PropVal.Prop  := objType.GetProperty(Node.Text);
      PropVal.Value := PropVal.Prop.GetValue(PropVal.Obj);
      result        := result + Node.Text + '.';

      if not PropVal.Value.IsObject or (PropVal.Value.AsObject = nil) then break;

      PropVal.Obj := PropVal.Value.AsObject;
    end;
  finally
    Path.Free;

    if result <> '' then
      Delete (result, Length (result), 1);
  end;
end;

procedure TFMainDesigner.ColorComboBoxChangeProperty(Sender: TObject);
var
  Item:    TTreeViewItem;
  Ctx:     TRttiContext;
  PropVal: RPropertyValue;
  Color:   TColorComboBox;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TColorComboBox) or not (TColorComboBox (Sender).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Color := TColorComboBox (Sender);
    Item  := TTreeViewItem (Color.Parent);
    GetPropertyValue (Ctx, Item, PropVal);
    Card.SetPropText (PropVal.Obj, PropVal.Prop, IntToStr (Color.Color));
    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.ComboBoxChangeProperty(Sender: TObject);
var
  Item:    TTreeViewItem;
  Ctx:     TRttiContext;
  PropVal: RPropertyValue;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TComboBox) or not (TComboBox (Sender).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Item := TTreeViewItem (TComboBox(Sender).Parent);
    GetPropertyValue (Ctx, Item, PropVal);
    if (PropVal.Prop.Name = 'JSONField') and (TComboBox (Sender).ItemIndex < 2) then
      Card.SetPropText (PropVal.Obj, PropVal.Prop, '')
    else
      Card.SetPropText (PropVal.Obj, PropVal.Prop, TComboBox (Sender).Items [TComboBox (Sender).ItemIndex]);
    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.ParentComboBoxChangeProperty(Sender: TObject);
var
  Ctx: TRttiContext;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TComboBox) or not (TComboBox (Sender).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Card.ChangeControlParent(ObjectInspectorCtrl, TControl (TComboBox (Sender).Items.Objects [TComboBox (Sender).ItemIndex]));
    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.ButtonImageProperty(Sender: TObject);
var
  Item:    TTreeViewItem;
  Img:     TJSONImage;
  Ctx:     TRttiContext;
  PropVal: RPropertyValue;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (ObjectInspectorCtrl is TJSONImage) or not (Sender is TButton) or not (TButton (Sender).Parent is TEdit) or
     not (TEdit (TButton (Sender).Parent).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Img  := TJSONImage (ObjectInspectorCtrl);
    Item := TTreeViewItem (TEdit (TButton (Sender).Parent).Parent);
    GetPropertyValue (Ctx, Item, PropVal);

    if (PropVal.Prop.Name = 'Bitmap') and OpenImgDlg.Execute then
    begin
      Img.Bitmap.LoadFromFile(OpenImgDlg.FileName);
      Img.IsBitmapEmpty := False;
      Card.Changed      := True;
    end;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.ButtonEventProperty(Sender: TObject);
var
  Item:        TTreeViewItem;
  Ctx:         TRttiContext;
  PropVal:     RPropertyValue;
  s:           string;
  Value:       string;
  EventAction: TWindowEventResult;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TButton) or not (TButton (Sender).Parent is TEdit) or
     not (TEdit (TButton (Sender).Parent).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Item := TTreeViewItem (TEdit (TButton (Sender).Parent).Parent);
    GetPropertyValue (Ctx, Item, PropVal);
    s := Card.GetPropText(PropVal.Obj, PropVal.Prop);
    LaunchEventsWindow(ObjectInspectorCtrl, Item.Text, s, EventAction, Value);
    case EventAction of
      werSelectExistent,
      werDisassociate: begin
        Card.SetPropText(PropVal.Obj, PropVal.Prop, Value);
      end;
      werCreateNew: begin
        s := Format ('procedure %s (', [Value]);
        if      PropVal.Prop.Name.ToUpper = UpperCase ('OnDisplayJSONLabel')    then
          s := s + 'Sender: TJSONLabel; JSONLabelName, JSONField: string; var Text: string'
        else if PropVal.Prop.Name.ToUpper = UpperCase ('OnBeforeObjectDisplay') then
          s := s + 'Sender: TControl';
        s := s + ');' + sLineBreak + 'begin' + sLineBreak + '  ' + sLineBreak + 'end;' + sLinebreak;
        MemoPascal.Lines.Text := MemoPascal.Lines.Text + sLineBreak + sLineBreak + s;
        Card.SetPropText(PropVal.Obj, PropVal.Prop, Value);
        TabControl1.ActiveTab := tabPascal;
        MemoPascal.SetFocus;
        MemoPascal.CaretPosition := MemoPascal.CaretPosition.Create(MemoPascal.Lines.Count - 3, 2);
      end;
    end;
    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.ButtonRTMLTextProperty(Sender: TObject);
var
  Item:        TTreeViewItem;
  Ctx:         TRttiContext;
  PropVal:     RPropertyValue;
  s:           string;
  Value:       string;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TButton) or not (TButton (Sender).Parent is TEdit) or
     not (TEdit (TButton (Sender).Parent).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Item := TTreeViewItem (TEdit (TButton (Sender).Parent).Parent);
    GetPropertyValue (Ctx, Item, PropVal);
    s := Card.GetPropText(PropVal.Obj, PropVal.Prop);
    FrmRTMLPreview.Source.Lines.Text := s;
    if FrmRTMLPreview.ShowModal = mrOK then
    begin
      s := FrmRTMLPreview.Source.Lines.Text;
      Card.SetPropText (PropVal.Obj, PropVal.Prop, s);
      Card.UpdateSelections;
    end;
  finally
    Ctx.Free;
  end;
end;


procedure TFMainDesigner.EditChangeProperty(Sender: TObject);
var
  Item:    TTreeViewItem;
  Ctx:     TRttiContext;
  PropVal: RPropertyValue;
  Ed:      TEdit;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TEdit) or not (TEdit (Sender).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Ed   := TEdit (Sender);
    Item := TTreeViewItem(Ed.Parent);
    GetPropertyValue (Ctx, Item, PropVal);
    Card.SetPropText (PropVal.Obj, PropVal.Prop, Ed.Text);

    if Card.IsPropertyDefaultValue (PropVal.Prop, Ed.Text) then
      Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style - [TFontStyle.fsBold]
    else
      Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style + [TFontStyle.fsBold];

    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

function TFMainDesigner.BuildPropertySet (Item: TTreeViewItem): string;
var
  i, j: integer;
  Child: TTreeViewItem;
begin
  result := '';
  for i := 0 to Item.Count - 1 do
  begin
    Child := Item.Items [i];

    for j := 0 to Child.ControlsCount - 1 do
      if Child.Controls [j] is TCheckBox then
      begin
        if TCheckBox (Child.Controls [j]).IsChecked then
          result := result + Child.Text + ',';

        break;
      end;
  end;

  if result <> '' then
    Delete (result, Length (result), 1);

  result := '[' + result + ']';
end;


procedure TFMainDesigner.CheckSetChange (Sender: TObject);
var
  Item:     TTreeViewItem;
  Ctx:      TRttiContext;
  PropVal:  RPropertyValue;
  PVParent: RPropertyValue;
  Check:    TCheckBox;
  Path, s:  string;
  Obj:      TObject;
begin
  if ReloadingCtrl or not Assigned (ObjectInspectorCtrl) or not (Sender is TCheckBox) or not (TCheckBox (Sender).Parent is TTreeViewItem) then exit;

  Ctx := TRttiContext.Create;
  try
    Check      := TCheckBox(Sender);
    Check.Text := BoolToStr(Check.IsChecked, True);
    Item       := TTreeViewItem(Check.Parent);
    Path       := GetPropertyValue (Ctx, Item.ParentItem, PropVal);
    s          := BuildPropertySet (Item.ParentItem);
    if Item.ParentItem.ParentItem <> nil then
    begin
      Path := GetPropertyValue (Ctx, Item.ParentItem.ParentItem, PVParent);
      Obj  := PVParent.Obj;
    end
    else
      Obj := ObjectInspectorCtrl;

    Card.SetPropText(Obj, PropVal.Prop, s);
    Card.UpdateSelections;
  finally
    Ctx.Free;
  end;
end;

procedure TFMainDesigner.PopulateComboPropValues (Obj: TObject; Prop: TRttiProperty; Cb: TComboBox);
var
  i:        integer;
  V:        TValue;
  EnumData: PTypeData;
begin
  if not Assigned (Obj) or not Assigned (Prop) or not Assigned (Cb) then exit;

  V        := Prop.GetValue(Obj);
  EnumData := GetTypeData(V.TypeInfo);
  Cb.BeginUpdate;
  try
    Cb.Items.Clear;
    for I := EnumData.MinValue to EnumData.MaxValue do
      Cb.Items.Add(GetEnumName(V.TypeInfo, i));
  finally
    Cb.EndUpdate;
  end;
end;

procedure TFMainDesigner.PopulateComboFontNames (CB: TComboBox);
var
  fList: TStringList;
  i:     Integer;
begin
  CB.Items.Clear;
  fList := TStringList.Create;
  CollectFonts(fList);
  for i := 0 to fList.Count -1 do
  begin
     CB.Items.Add(FList[i]);
  end;
  fList.Free;
end;

procedure TFMainDesigner.PopulateComboJSONFields (CB: TComboBox);
var
  i: integer;
begin
  CB.Items.Clear;
  CB.Items.Add('<None>');
  for i := 0 to Card.JSONDataSet.MemTable.FieldList.Count - 1 do
    CB.Items.Add(Card.JSONDataSet.MemTable.FieldList [i].FieldName);
end;

procedure TFMainDesigner.PopulateComboParents (Ctrl: TControl; CB: TComboBox);
var
  i, Idx: integer;
  c: TControl;
begin
  CB.BeginUpdate;
  try
    CB.Items.Clear;
    CB.Items.AddObject('Card', Card.Background);
    for i := 0 to Card.LstControls.Count - 1 do
    begin
      c := TControl (Card.LstControls [i]);
      if (c <> Ctrl) and not Card.IsChildOf (c, Ctrl) then
        CB.Items.AddObject(c.Name, c);
    end;
  finally
    Idx := CB.Items.IndexOf(Ctrl.Parent.Name);
    if Idx <> -1 then
      CB.ItemIndex := Idx
    else
      CB.ItemIndex := 0;  //Card

    CB.EndUpdate;
  end;
end;

procedure TFMainDesigner.PopulateComboEvents (Ctrl: TControl; CB: TComboBox; EventName: string);
var
  s, ProcName: string;
  i:           integer;
  UserCodes:   TatScript;
  Len:         integer;
begin
  CB.Items.Clear;
  Card.ScripterCreate;
  Card.Scripter.SourceCode.Text := MemoPascal.Lines.Text;
  UserCodes                     := nil;
  EventName                     := EventName + '_';
  Delete (EventName, 1, 2);
  Len := Length (EventName);
  try
    UserCodes := Card.Scripter.Scripts.Add;
    try
      UserCodes.SourceCode.Text := Card.Scripter.SourceCode.Text;
      FrmPreview.Grid.RegisterControlsAndJSONFields (Card, UserCodes);
      UserCodes.Compile;
      for i := 0 to UserCodes.ScriptInfo.Routines.Count - 1 do
      begin
        ProcName := UserCodes.ScriptInfo.Routines.Items [i].Name;
        s        := Copy (ProcName, 1, Len);
        if s.ToUpper = EventName.ToUpper then
          CB.Items.Add(ProcName);
      end;
    except
      on E: EatCompileError do ;
//        TDialogServiceSync.MessageDialog('Compiler error: ' + E.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0)
    end;
  finally
    UserCodes.Free;
  end;
end;

procedure TFMainDesigner.LaunchEventsWindow (Ctrl: TControl; EventName, PropValue: string; var EventAction: TWindowEventResult; var Value: string);
var
  s, ProcName: string;
  i:           integer;
  UserCodes:   TatScript;
  Len:         integer;
begin
  EventAction              := werCancel;
  Value                    := '';
  FrmEventSelector.Caption := EventName + ' Event';
  FrmEventSelector.LstEvents.Items.Clear;
  FrmEventSelector.edControlName.Text := Ctrl.Name;
  Card.ScripterCreate;
  Card.Scripter.SourceCode.Text := MemoPascal.Lines.Text;
  UserCodes                     := nil;
  EventName                     := EventName + '_';
  Delete (EventName, 1, 2);
  Len := Length (EventName);
  try
    UserCodes := Card.Scripter.Scripts.Add;
    try
      UserCodes.SourceCode.Text := Card.Scripter.SourceCode.Text;
      FrmPreview.Grid.RegisterControlsAndJSONFields (Card, UserCodes);
      UserCodes.Compile;
      for i := 0 to UserCodes.ScriptInfo.Routines.Count - 1 do
      begin
        ProcName := UserCodes.ScriptInfo.Routines.Items [i].Name;
        s        := Copy (ProcName, 1, Len);
        if s.ToUpper = EventName.ToUpper then
          FrmEventSelector.LstEvents.Items.Add(ProcName);
      end;
      FrmEventSelector.LstEvents.ItemIndex := FrmEventSelector.LstEvents.Items.IndexOf(PropValue);

      case FrmEventSelector.ShowModal of
        mrOk: begin
          EventAction := werSelectExistent;
          Value       := FrmEventSelector.LstEvents.Items [FrmEventSelector.LstEvents.ItemIndex];
        end;
        mrContinue: begin
          EventAction := werCreateNew;
          Value       := EventName + Ctrl.Name;
        end;
        mrRetry: begin
          EventAction := werDisassociate;
          Value       := '';
        end;
      end;

    except
      on E: EatCompileError do ;
//        TDialogServiceSync.MessageDialog('Compiler error: ' + E.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0)
    end;
  finally
    UserCodes.Free;
  end;
end;

function PropNameIsEvent (PropName: string): boolean;
begin
  result := (PropName.ToUpper = UpperCase ('OnDisplayJSONLabel')) or
            (PropName.ToUpper = UpperCase ('OnBeforeObjectDisplay'));
end;

function CompareProperties (Item1, Item2: Pointer): integer;
begin
  result := Ord (PropNameIsEvent(TRttiProperty(Item1).Name)) -
            Ord (PropNameIsEvent(TRttiProperty(Item2).Name));

  if result = 0 then
    result := CompareText(TRttiProperty(Item1).Name, TRttiProperty (Item2).Name);
end;

procedure TFMainDesigner.ReadCtrlProperties (Ctrl: TControl);
var
  Ctx:     TRttiContext;
  Compare: TFMXObjectSortCompare;

  {$region 'Create property items'}
  procedure CreateSetItems (Node: TTreeViewItemProp; Prop: TRttiProperty; Obj: TObject; EdWidth: Single);
  var
    i, p:   integer;
    Item:   TTreeViewItemProp;
    Check:  TCheckBox;
    s1, s2: string;
    A:      TArray<string>;
  begin
    s1 := Card.GetPropText(Obj, Prop);
    A  := s1.Split([',']);
    for i := 0 to Length (A) - 1 do
    begin
      Item            := TTreeViewItemProp.Create(tvObjectInspector);
      Item.Parent     := Node;
      p               := Pos (':', A [i]);
      s1              := Copy (A [i], 1, p - 1);
      s2              := Copy (A [i], p + 1, Length (A [i]));
      Item.Text       := s1;
      Check           := TCheckBox.Create(Item);
      Check.Text      := s2;
      Check.IsChecked := s2.ToUpper = 'TRUE';
      Check.Parent    := Item;
      Check.Align     := TAlignLayout.Right;
      Check.Width     := EdWidth;
      Check.OnChange  := CheckSetChange;
      Item.CtrlEdit   := Check;
    end;
  end;

  procedure ReadSubclass (Node: TTreeViewItem; Prop: TRttiProperty; Obj: TObject); forward;

  procedure ListObjectProperties (NodeParent: TFMXObject; Properties: TArray<TRttiProperty>; Obj: TObject);
  var
    i:      integer;
    Item:   TTreeViewItemProp;
    Ed:     TEdit;
    Cb:     TComboBox;
    Btn:    TButton;
    Prop:   TRttiProperty;
    Color:  TColorComboBox;
    List:   TList;
    Pnl:    TPanel;
  begin
    List := TList.Create;
    for i := 0 to Length (Properties) - 1 do
    begin
      Prop := Properties [i];
      if ((Prop.Visibility = mvPublished) and
         //Not events
         not (Prop.PropertyType.TypeKind in [TTypeKind.tkMethod]) and
         not Card.IsExcludedProperty(Obj.ClassName, Prop.Name))
         //Remove when drag and drop will be solved
         or ((Prop.Visibility = mvPublic) and (Prop.Name = 'Parent'))
         then
           List.Add(Prop);
    end;
    List.Sort(@CompareProperties);

    for i := 0 to List.Count - 1 do
    begin
      Prop               := TRttiProperty (List [i]);
      Item               := TTreeViewItemProp.Create(tvObjectInspector);
      Item.Parent        := NodeParent;
      Item.Text          := Prop.Name;
      Item.ShowHint      := True;
      Item.Hint          := Prop.PropertyType.ToString;
      Item.PropName      := Prop.Name;
      Item.PropClassName := Prop.PropertyType.ToString;
      Item.PropType      := Prop.PropertyType.TypeKind;
      Item.Obj           := Obj;

      if PropNameIsEvent (Prop.Name) then
      begin
        Ed                := TEdit.Create(Item);
        Ed.Visible        := False;
        Ed.Parent         := Item;
        Ed.Align          := TAlignLayout.Right;
        Ed.Text           := Card.GetPropText (Obj, Prop);
        Ed.StyledSettings := [];
        Ed.FontColor      := TAlphaColors.Gray;
        Ed.ReadOnly       := True;
        Btn               := TButton.Create(Ed);
        Btn.Parent        := Ed;
        Btn.Text          := '...';
        Btn.Align         := TAlignLayout.Right;
        Btn.Width         := 20;
        Btn.Visible       := True;
        Btn.OnClick       := ButtonEventProperty;
        Ed.Visible        := True;
        Item.CtrlEdit     := Ed;
      end
      else if (Prop.Name = 'Text') and (Obj is TJSONLabel) then
      begin
        Ed                := TEdit.Create(Item);
        Ed.Visible        := False;
        Ed.Parent         := Item;
        Ed.Align          := TAlignLayout.Right;
        Ed.Text           := Card.GetPropText (Obj, Prop);
        Btn               := TButton.Create(Ed);
        Btn.Parent        := Ed;
        Btn.Text          := '...';
        Btn.Align         := TAlignLayout.Right;
        Btn.Width         := 20;
        Btn.Visible       := True;
        Btn.OnClick       := ButtonRTMLTextProperty;
        Ed.Visible        := True;
        Item.CtrlEdit     := Ed;
      end
      else if (Prop.Name = 'JSONField') and (Obj is TJSONFieldLink) then
      begin
        Cb          := TComboBox.Create(Item);
        Cb.Visible  := False;
        Cb.Parent   := Item;
        Cb.Align    := TAlignLayout.Right;
        PopulateComboJSONFields(Cb);
        Cb.ItemIndex  := Cb.Items.IndexOf(Card.GetPropText (Obj, Prop));
        if Cb.ItemIndex = -1 then
          Cb.ItemIndex := 0;
        Cb.OnChange   := ComboBoxChangeProperty;
        Cb.Visible    := True;
        Item.CtrlEdit := Cb;
      end
      else if Prop.PropertyType.ToString = 'TAlphaColor' then
      begin
        Color          := TColorComboBox.Create(Item);
        Color.Visible  := False;
        Color.Parent   := Item;
        Color.Align    := TAlignLayout.Right;
        Color.Color    := StrToInt64(Card.GetPropText (Obj, Prop));
        Color.OnChange := ColorComboBoxChangeProperty;
        Color.Visible  := True;
        Item.CtrlEdit  := Color;
      end
      else if Prop.PropertyType.ToString = 'TBitmap' then
      begin
        Ed                := TEdit.Create(Item);
        Ed.Visible        := False;
        Ed.Parent         := Item;
        Ed.Align          := TAlignLayout.Right;
        Ed.Text           := '(TBitmap)';
        Ed.StyledSettings := [];
        Ed.FontColor      := TAlphaColors.Gray;
        Ed.ReadOnly       := True;
        Btn               := TButton.Create(Ed);
        Btn.Parent        := Ed;
        Btn.Text          := '...';
        Btn.Align         := TAlignLayout.Right;
        Btn.Width         := 20;
        Btn.Visible       := True;
        Btn.OnClick       := ButtonImageProperty;
        Ed.Visible        := True;
        Item.CtrlEdit     := Ed;
      end
      else if Prop.PropertyType.ToString = 'TFontName' then
      begin
        Cb          := TComboBox.Create(Item);
        Cb.Visible  := False;
        Cb.Parent   := Item;
        Cb.Align    := TAlignLayout.Right;
        PopulateComboFontNames(Cb);
        Cb.Sort(Compare);
        Cb.ItemIndex  := Cb.Items.IndexOf(Card.GetPropText (Obj, Prop));
        Cb.OnChange   := ComboBoxChangeProperty;
        Cb.Visible    := True;
        Item.CtrlEdit := Cb;
      end
      else if Prop.PropertyType.TypeKind = TTypeKind.tkEnumeration then
      begin
        Cb          := TComboBox.Create(Item);
        Cb.Visible  := False;
        Cb.Parent   := Item;
        Cb.Align    := TAlignLayout.Right;
        PopulateComboPropValues(Obj, Prop, Cb);
        Cb.ItemIndex  := Cb.Items.IndexOf(Card.GetPropText (Obj, Prop));
        Cb.OnChange   := ComboBoxChangeProperty;
        Cb.Visible    := True;
        Item.CtrlEdit := Cb;
      end
      else if Prop.PropertyType.TypeKind = TTypeKind.tkSet then
      begin
        Item.ResultingTextSettings.FontColor := TAlphaColors.Blue;
        Ed                := TEdit.Create(Item);
        Ed.Visible        := False;
        Ed.Parent         := Item;
        Ed.Align          := TAlignLayout.Right;
        Ed.Text           := '(' + Prop.PropertyType.ToString + ')';
        Ed.StyledSettings := [];
        Ed.FontColor      := TAlphaColors.Gray;
        Ed.ReadOnly       := True;
        Ed.Visible        := True;
        Item.CtrlEdit     := Ed;

        CreateSetItems(Item, Prop, Obj, Ed.Width);
      end
      //Remove condition when drag and drop will be solved
      else if Prop.Name = 'Parent' then
      begin
        Cb          := TComboBox.Create(Item);
        Cb.Visible  := False;
        Cb.Parent   := Item;
        Cb.Align    := TAlignLayout.Right;
        PopulateComboParents (TControl (Obj), Cb);
        Cb.OnChange   := ParentComboBoxChangeProperty;
        Cb.Visible    := True;
        Item.CtrlEdit := Cb;
        Item.ResultingTextSettings.FontColor := TAlphaColors.Blue;
      end
      else
      begin
        Ed                := TEdit.Create(Item);
        Ed.Visible        := False;
        Ed.Parent         := Item;
        Ed.Align          := TAlignLayout.Right;
        Ed.Text           := Card.GetPropText (Obj, Prop);
        Ed.OnChange       := EditChangeProperty;
        Ed.StyledSettings := [];
        Ed.Visible        := True;
        Item.CtrlEdit     := Ed;

        if Prop.PropertyType.TypeKind in [TTypeKind.tkClass, TTypeKind.tkClassRef] then
        begin
          Ed.FontColor                         := TAlphaColors.Gray;
          Ed.ReadOnly                          := True;
          Item.ResultingTextSettings.FontColor := TAlphaColors.Blue;

          ReadSubclass (Item, Prop, Obj);
        end;

        if Card.IsPropertyDefaultValue (Prop, Ed.Text) then
          Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style - [TFontStyle.fsBold]
        else
          Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style + [TFontStyle.fsBold];
      end;
    end;
    List.Free;
  end;

  procedure ReadSubclass (Node: TTreeViewItem; Prop: TRttiProperty; Obj: TObject);
  var
    V:          TValue;
    ObjType:    TRttiType;
    Properties: TArray<TRttiProperty>;
  begin
    if Prop.PropertyType.TypeKind = TTypeKind.tkClass then
    begin
      V := Prop.GetValue(Obj);
      if V.IsObject and (V.AsObject <> nil) then
      begin
        objType    := Ctx.GetType(V.AsObject.ClassInfo);
        Properties := objType.GetProperties;
        ListObjectProperties (Node, Properties, V.AsObject);
      end;
    end;
  end;
  {$endregion}

  {$region 'Reload properties'}
  procedure ReloadProperties (Item: TTreeViewItemProp; ObjType: TRttiType);
  var
    Prop:         TRttiProperty;
    RealValue:    string;
    Ed:           TEdit;
    Cb:           TComboBox;
    Check:        TCheckBox;
    Color:        TColorComboBox;
    Child:        TTreeViewItemProp;
    ChildObjType: TRttiType;
    i, p:         integer;
    V:            TValue;
    s1, s2:       string;
    A:            TArray<string>;
  begin
    if Item.PropName.IsEmpty or not Assigned (Item.Obj) or not Assigned (Item.CtrlEdit) then exit;

    Prop      := ObjType.GetProperty(Item.PropName);
    RealValue := Card.GetPropText(Item.Obj, Prop);

    try
      if (Prop.Name = 'JSONField') and (Item.Obj is TJSONFieldLink) then
      begin
        Cb := TComboBox(Item.CtrlEdit);
        PopulateComboJSONFields(Cb);
        Cb.ItemIndex := Cb.Items.IndexOf(RealValue);
        if Cb.ItemIndex < 2 then
          Cb.ItemIndex := 0;
      end
      else if Prop.PropertyType.ToString = 'TAlphaColor' then
      begin
        Color       := TColorComboBox(Item.CtrlEdit);
        Color.Color := StrToInt64(RealValue);
      end
      else if Prop.PropertyType.ToString = 'TFontName' then
      begin
        Cb := TComboBox (Item.CtrlEdit);
        PopulateComboFontNames(Cb);
        Cb.Sort(Compare);
        Cb.ItemIndex  := Cb.Items.IndexOf(RealValue);
      end
      else if Prop.PropertyType.TypeKind = TTypeKind.tkEnumeration then
      begin
        Cb := TComboBox (Item.CtrlEdit);
        PopulateComboPropValues(Item.Obj, Prop, Cb);
        Cb.ItemIndex  := Cb.Items.IndexOf(RealValue);
      end
      else if Prop.Name = 'Parent' then
      begin
        Cb := TComboBox (Item.CtrlEdit);
        PopulateComboParents(Ctrl, Cb);
      end
      else if Prop.PropertyType.TypeKind = TTypeKind.tkSet then
      begin
        A := RealValue.Split([',']);
        if Length (A) <> Item.Count then exit;

        for i := 0 to Item.Count - 1 do
        begin
          Child           := TTreeViewItemProp (Item.Items [i]);
          Check           := TCheckBox (Child.CtrlEdit);
          p               := Pos (':', A [i]);
          s1              := Copy (A [i], 1, p - 1);
          s2              := Copy (A [i], p + 1, Length (A [i]));
          Check.Text      := s2;
          Check.IsChecked := s2.ToUpper = 'TRUE';
        end;
      end
      else
      begin
        Ed      := TEdit (Item.CtrlEdit);
        Ed.Text := RealValue;

        if Prop.PropertyType.TypeKind in [TTypeKind.tkClass, TTypeKind.tkClassRef] then
        begin
          V := Prop.GetValue(Item.Obj);
          if V.IsObject and (V.AsObject <> nil) then
          begin
            ChildObjType := Ctx.GetType(V.AsObject.ClassInfo);
            for i := 0 to Item.Count - 1 do
            begin
              Child := TTreeViewItemProp (Item.Items [i]);
              ReloadProperties (Child, ChildObjType);
            end;
          end;
        end;

        if Card.IsPropertyDefaultValue (Prop, Ed.Text) then
          Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style - [TFontStyle.fsBold]
        else
          Ed.TextSettings.Font.Style := Ed.TextSettings.Font.Style + [TFontStyle.fsBold];
      end;
    except

    end;
  end;
  {$endregion}

var
  ObjType:    TRttiType;
  Properties: TArray<TRttiProperty>;
  i:          integer;

begin
  //Define the sort function for Combobox
  Compare := function(Item1, Item2: TFmxObject): Integer
  begin
    if TListBoxItem(Item1).Text = TListBoxItem(Item2).Text then
      Result := 0
    else
    if TListBoxItem(Item1).Text > TListBoxItem(Item2).Text then
      Result := 1
    else
      Result := -1;
  end;

  tvObjectInspector.BeginUpdate;
  ReloadingCtrl := True;
  Card.Cursor   := crHourGlass;
  Ctx           := TRttiContext.Create;
  try
    try
      if (Ctrl = ObjectInspectorCtrl) and Assigned (Ctrl) then
      begin
        //With same control, just reload values
        ObjectInspectorCtrl := Ctrl;
        objType := Ctx.GetType(Ctrl.ClassInfo);
        for i := 0 to tvObjectInspector.Count - 1 do
          ReloadProperties(TTreeViewItemProp (tvObjectInspector.Items [i]), ObjType);
      end
      else
      begin
        //Distinct control, must clear and read all properties
        ObjectInspectorCtrl := Ctrl;
        tvObjectInspector.Clear;
        if not Assigned (Ctrl) then exit;

        objType    := Ctx.GetType(Ctrl.ClassInfo);
        Properties := objType.GetProperties;
        ListObjectProperties (tvObjectInspector, Properties, Ctrl);
      end;
    except
    end;
  finally
    tvObjectInspector.EndUpdate;
    Card.Cursor         := crArrow;
    ObjectInspectorCtrl := Ctrl;
    ReloadingCtrl       := False;
    Ctx.Free;
  end;
end;
{$endregion}

{$region 'Card routines and event handlers'}
procedure TFMainDesigner.ChangePaletteTool (Sender: TObject);
begin
  case Card.CurrentTool of
    ptDeselect:    actPaletteDeselectItem.Execute;
    ptLabel:       actPaletteLabel.Execute;
    ptImage:       actPaletteImage.Execute;
    ptLine:        actPaletteLine.Execute;
    ptRectangle:   actPaletteRectangle.Execute;
    ptRoundedRect: actPaletteRoundedRect.Execute;
    ptEllipse:     actPaletteEllipse.Execute;
    ptCircle:      actPaletteCircle.Execute;
    ptArc:         actPaletteArc.Execute;
  end;
end;

procedure TFMainDesigner.ChangeSelection (Sender: TObject);
begin
  UpdateCBLstControl;
  ReadCtrlProperties(Card.Selected);
end;

procedure TFMainDesigner.ControlNotify (Sender: TObject; Ctrl: TControl; AOperation: TOperation);
var
  Idx: integer;
begin
  if not Assigned (Ctrl) then exit;

  if AOperation = opInsert then
  begin
    cbLstControls.Items.AddObject(Ctrl.Name, Ctrl);
  end
  else
  begin
    Idx := cbLstControls.Items.IndexOfObject(Ctrl);
    if Idx <> -1 then
      cbLstControls.Items.Delete(Idx);
  end;
end;

procedure TFMainDesigner.CardChange (Sender: TObject);
var
  mem: TMemoryStream;
begin
  ReadCtrlProperties(Card.Selected);
  if FrmPreview.Showing then
  begin
    mem := TMemoryStream.Create;
    try
      Card.SaveToStream(mem);
      mem.Position := 0;
      FrmPreview.Grid.LoadTemplate(mem);
      FrmPreview.ImageBackground.PaintBackground;
    finally
      mem.Free;
    end;
  end;
end;

procedure TFMainDesigner.ControlChangeName(Sender: TObject; Ctrl: TControl; OldName, NewName: string);
var
  i: integer;
begin
  for i := 0 to cbLstControls.Count - 1 do
    if cbLstControls.Items.Objects [i] = Ctrl then
    begin
      cbLstControls.Items [i] := NewName;

      if cbLstControls.ItemIndex = i then
        cbLstControls.Repaint;

      exit;
    end;
end;

procedure TFMainDesigner.GetStructureImageIndex (Sender: TObject; Tool: TPaletteTool; var ImageIndex: integer);
begin
  case Tool of
    ptLabel:       ImageIndex := 16;
    ptImage:       ImageIndex := 17;
    ptLine:        ImageIndex := 18;
    ptRectangle:   ImageIndex := 19;
    ptRoundedRect: ImageIndex := 20;
    ptCircle:      ImageIndex := 21;
    ptEllipse:     ImageIndex := 22;
    ptArc:         ImageIndex := 23;
  end;
end;

procedure TFMainDesigner.PreviewGridChangeColRows (Sender: TObject);
begin
  spColumns.Value := FrmPreview.Grid.ColCount;
  spRows.Value    := FrmPreview.Grid.RowCount;
end;

procedure TFMainDesigner.LoadCard;
begin
  FLoadingCard := True;
  try
    spWidth.Value                  := Card.Width;
    spHeight.Value                 := Card.Height;
    spOpacity.Value                := Card.Opacity * 100;
    cbCardBackgroundType.ItemIndex := Ord (Card.BackgroundType);
    cbCardColor.Color              := Card.BackgroundColor;
  finally
    FLoadingCard := False;
  end;
end;

procedure TFMainDesigner.ClearCard;
begin
  Card.Clear;
  Card.DesignMode := True;
  CardFileName    := '';
  UpdateJSON;
end;

procedure TFMainDesigner.OpenCard (FileName: string);
var
  ShowingPreview: boolean;
begin
  ShowingPreview := FrmPreview.Showing;
  try
    if ShowingPreview then
      FrmPreview.Close;

    Card.LoadFromFile(FileName);
    Card.DesignMode := True;
    CardFileName    := FileName;
    LoadCard;
    UpdateJSON;
  finally
    if ShowingPreview then
      actPreviewDisplay.Execute;
  end;
end;

{$endregion}

{$region 'JSON Routines'}
procedure TFMainDesigner.actFileOpenJSONExecute(Sender: TObject);
begin
  if OpenJSONDlg.Execute then
  begin
    edJSONFile.Text := OpenJSONDlg.FileName;
    MemoJSON.Lines.LoadFromFile(OpenJSONDlg.FileName);
    UpdateJSON;
  end;
end;

procedure TFMainDesigner.UpdateJSON;
var
  RootItem: string;
  DataDepth: integer;
begin
  try
    AnalyzeJSON(MemoJSON.Lines.Text, RootItem, DataDepth);
    BindSourceDB1.DataSet       := Card.JSONDataSet.MemTable;
    Card.JSONDataSet.BeginUpdate;
    Card.JSONDataSet.JSONSource := MemoJSON.Lines.Text;
    Card.JSONDataSet.ScanDepth  := DataDepth;
    Card.JSONDataSet.RootPath   := RootItem;
    Card.JSONDataSet.EndUpdate;
    sbJSONDataTable.Text        := 'RowCount: ' + Card.JSONDataSet.MemTable.RecordCount.ToString;
    sbJSONPanelStatus.Text      := 'OK';
    ReadCtrlProperties (Card.Selected);
  except
    on E: Exception do
    begin
      if (Trim (MemoJSON.Lines.Text) <> '') or (E.Message <> 'Invalid JSON') then
        sbJSONPanelStatus.Text := E.Message;
    end;
  end;
end;

procedure TFMainDesigner.UpdateJSONCaret;
begin
  sbJSONPanelCaret.Text := Format ('Lin: %d, Col: %d', [MemoJSON.CaretPosition.Line + 1, MemoJSON.CaretPosition.Pos + 1]);
end;

procedure TFMainDesigner.edJSONRootItemChange(Sender: TObject);
begin
  UpdateJSON;
end;

procedure TFMainDesigner.spJSONDepthChange(Sender: TObject);
begin
  UpdateJSON;
end;

procedure TFMainDesigner.MemoJSONChange(Sender: TObject);
begin
  UpdateJSON;
end;

procedure TFMainDesigner.MemoJSONChangeTracking(Sender: TObject);
begin
  UpdateJSON;
end;

procedure TFMainDesigner.MemoJSONEnter(Sender: TObject);
begin
  UpdateJSONCaret;
end;

procedure TFMainDesigner.MemoJSONKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  UpdateJSONCaret;
end;

procedure TFMainDesigner.MemoJSONKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  UpdateJSONCaret;
end;

procedure TFMainDesigner.MemoJSONMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  UpdateJSONCaret;
end;

procedure TFMainDesigner.MemoJSONMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
begin
  UpdateJSONCaret;
end;

procedure TFMainDesigner.MemoJSONMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  UpdateJSONCaret;
end;
{$endregion}

{$region 'Pascal memo'}
procedure TFMainDesigner.UpdatePascalCaret;
begin
  sbPascalPanelCaret.Text := Format ('Lin: %d, Col: %d', [MemoPascal.CaretPosition.Line + 1, MemoPascal.CaretPosition.Pos + 1]);
end;

procedure TFMainDesigner.UpdatePascalCode;
begin
  if LoadingPascal then exit;

  Script.Code                     := MemoPascal.Lines.Text;
  sbPascalPanelCompileStatus.Text := '';
end;

procedure TFMainDesigner.LoadPascalCodeFromFile(FileName: string);
begin
  LoadingPascal := True;
  try
    Script.LoadFromFile(FileName);
    Settings.ScriptFileName := FileName;
    edScriptFileName.Text   := FileName;
    MemoPascal.Lines.Text   := Script.Code;
    if MemoPascal.Lines.Text <> '' then
      actPascalCompile.Execute;
  finally
    LoadingPascal := False;
  end;
end;

procedure TFMainDesigner.LoadPascalCode (Code: string; FileName: string = '');
begin
  LoadingPascal := True;
  try
    Script.FileName       := FileName;
    Script.Code           := Code;
    MemoPascal.Lines.Text := Script.Code;
    if MemoPascal.Lines.Text <> '' then
      actPascalCompile.Execute;
  finally
    LoadingPascal := False;
  end;
end;

procedure TFMainDesigner.ChangePascalFileName (Sender: TObject);
begin
  if Script.FileName.IsEmpty then
    tabPascal.Text := TabScriptCaption
  else
    tabPascal.Text := Format ('%s - [%s]', [TabScriptCaption, ExtractFileName (Script.FileName)])
end;

procedure TFMainDesigner.MemoPascalChange(Sender: TObject);
begin
  UpdatePascalCode;
end;

procedure TFMainDesigner.MemoPascalChangeTracking(Sender: TObject);
begin
  UpdatePascalCode;
end;

procedure TFMainDesigner.MemoPascalEnter(Sender: TObject);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.MemoPascalKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.MemoPascalKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.MemoPascalMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.MemoPascalMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Single);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.MemoPascalMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  UpdatePascalCaret;
end;

procedure TFMainDesigner.actFilePascalNewScriptExecute(Sender: TObject);
begin
  if Script.Changed then
  begin
    TabControl1.ActiveTab := tabPascal;
    case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFilePascalSave.Execute;
      mrCancel: exit;
    end;
  end;

  LoadPascalCode (PascalTemplate);
end;

procedure TFMainDesigner.actFilePascalOpenExecute(Sender: TObject);
begin
  if Script.Changed then
  begin
    TabControl1.ActiveTab := tabPascal;
    case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFilePascalSave.Execute;
      mrCancel: exit;
    end;
  end;

  if OpenPascalDlg.Execute then
    LoadPascalCodeFromFile(OpenPascalDlg.FileName);
end;

procedure TFMainDesigner.actFilePascalSaveAsExecute(Sender: TObject);
begin
  if SavePascalDlg.Execute then
  begin
    Script.Code := MemoPascal.Lines.Text;
    Script.SaveToFile (SavePascalDlg.FileName);
  end;
end;

procedure TFMainDesigner.actFilePascalSaveExecute(Sender: TObject);
begin
  if Script.FileName <> '' then
    Script.SaveToFile
  else
    actFilePascalSaveAs.Execute;
end;

procedure TFMainDesigner.actPascalCompileExecute(Sender: TObject);
const
  coSourcePosition = 'Source position: ';
var
  s: string;
  p, l, c: integer;
  A: TArray<string>;
  UserCodes: TatScript;
begin
  Card.ScripterCreate;
  sbPascalPanelCompileStatus.Text := '';
  Card.Scripter.SourceCode.Text   := MemoPascal.Lines.Text;
  UserCodes                       := nil;
  try
    UserCodes := Card.Scripter.Scripts.Add;
    try
      UserCodes.SourceCode.Text := Card.Scripter.SourceCode.Text;
      FrmPreview.Grid.RegisterControlsAndJSONFields (Card, UserCodes);
      UserCodes.Compile;
      sbPascalPanelCompileStatus.TextSettings.FontColor := TAlphaColors.Black;
      sbPascalPanelCompileStatus.Text                   := 'OK';
    except
      on E: EatCompileError do
      begin
        sbPascalPanelCompileStatus.TextSettings.FontColor := TAlphaColors.Red;
        sbPascalPanelCompileStatus.Text                   := StringReplace(StringReplace(E.Message, #10, ' ', [rfReplaceAll]), #13, ' ', [rfReplaceAll]);
        s := sbPascalPanelCompileStatus.Text;
        p := Pos (coSourcePosition, s);
        Delete (s, 1, p + Length (coSourcePosition) - 1);
        A := s.Split([',']);

        if (Length (A) = 2) and TryStrToInt (A [0], l) and TryStrToInt (A [1], c) then
        begin
          MemoPascal.SetFocus;
          MemoPascal.CaretPosition := MemoPascal.CaretPosition.Create(l - 1, c - 1);
        end;
      end;
    end;
  finally
    UserCodes.Free;
  end;
end;
{$endregion}

{$region 'Project routines'}
procedure TFMainDesigner.actFileProjectNewExecute(Sender: TObject);
begin
  if Settings.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Current project has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileProjectSave.Execute;
      mrCancel: exit;
    end;
  end;

  Settings.Clear;
  ReloadProject;
  Settings.Changed := False;

  if Card.Changed then
  begin
    case FMX.Dialogs.MessageDlg('Card template has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFileCardSave.Execute;
      mrCancel: exit;
    end;
  end;

  ClearCard;

  if Script.Changed then
  begin
    TabControl1.ActiveTab := tabPascal;
    case FMX.Dialogs.MessageDlg('Pascal script has changed, do you want save it?',
                                TMsgDlgType.mtConfirmation, mbYesNoCancel, 0) of
      mrYes:    actFilePascalSave.Execute;
      mrCancel: exit;
    end;
  end;

  LoadPascalCode (PascalTemplate);
  Script.Changed := False;
end;

procedure TFMainDesigner.actFileProjectOpenExecute(Sender: TObject);
begin
  if OpenIniDlg.Execute then
  begin
    //Clear environment
    ClearCard;
    Card.Clear;
    Card.Changed    := False;
    CardFileName    := '';
    Script.Code     := '';
    Script.FileName := '';
    Script.Changed  := False;

    //Load file and basic properties
    Settings.LoadFromIniFile(OpenIniDlg.FileName);
    ReloadProject;
    Settings.Changed := False;
  end;
end;

procedure TFMainDesigner.actFileProjectSaveAsExecute(Sender: TObject);
begin
  if SaveIniDlg.Execute then
    Settings.SaveToIniFile(SaveIniDlg.FileName);
end;

procedure TFMainDesigner.actFileProjectSaveExecute(Sender: TObject);
begin
  if Settings.FileName.IsEmpty then
    actFileProjectSaveAs.Execute
  else
    Settings.SaveToIniFile;
end;

procedure TFMainDesigner.ReloadProject;
begin
  edCardFileName.Text               := Settings.CardFileName;
  CardFileName                      := Settings.CardFileName;
  edScriptFileName.Text             := Settings.ScriptFileName;
  Script.FileName                   := Settings.ScriptFileName;
  spRows.Value                      := Settings.RowCount;
  spColumns.Value                   := Settings.ColCount;
  spMaxGridCards.Value              := Settings.MaxGridCards;
  spGridLeftMargin.Value            := Settings.LeftMargin;
  spGridTopMargin.Value             := Settings.TopMargin;
  cbAutoAdjustCardSize.IsChecked    := Settings.AutoAdjustCardSize;
  cbPreviewAutoAdjust.IsChecked     := Settings.AutoAdjust;
  spPlayerMonitor.Value             := Settings.PlayerMonitorNum;
  spGridInnerVert.Value             := Settings.VertInnerMargin;
  spGridInnerHoriz.Value            := Settings.HorizInnerMargin;
  cbPreviewBackgroundType.ItemIndex := Ord (Settings.BackgroundType);
  ComboColorBox_backround.Color     := Settings.BackgroundColor;
  edImageFileName.Text              := Settings.BackgroundFileName;
  edRESTServerAddress.Text          := Settings.RESTServerAddress;
  edRESTRequest.Text                := Settings.RESTRequest;
  spRESTInterval.Value              := Settings.RESTInterval;
  edJSONFile.Text                   := Settings.JSONFileName;
  cbJSONSource.ItemIndex            := Ord (Settings.JSONSourceType);
  cbTransitionType.ItemIndex        := Ord (Settings.TransitionType);
  spTransitionInterval.Value        := Settings.TransitionInterval;
  spTransitionSpeed.Value           := Settings.TransitionSpeed;
  spTransitionSteps.Value           := Settings.TransitionSteps;
  edDeviceID.Text                   := Settings.DeviceID;

  if not Settings.CardFileName.IsEmpty and FileExists (Settings.CardFileName) then
    OpenCard (Settings.CardFileName)
  else
    UpdateJSON;

  if not Settings.ScriptFileName.IsEmpty and FileExists (Settings.ScriptFileName) then
    LoadPascalCodeFromFile (Settings.ScriptFileName)
  else
    MemoPascal.Lines.Text := PascalTemplate;

  if not Settings.BackgroundFileName.IsEmpty and FileExists (Settings.BackgroundFileName) then
    FrmPreview.ImageBackground.LoadBackgroundBitmap(Settings.BackgroundFileName);
end;

procedure TFMainDesigner.ChangeProjectFileName (Sender: TObject);
begin
  if Settings.FileName.IsEmpty then
    tabSettings.Text := TabSettingsCaption
  else
    tabSettings.Text := Format ('%s - [%s]', [TabSettingsCaption, ExtractFileName (Settings.FileName)]);
end;

{$endregion}

{$region 'General File Routines'}
procedure TFMainDesigner.actFileNewExecute(Sender: TObject);
begin
  if      TabControl1.ActiveTab = tabSettings then
    actFileProjectNew.Execute
  else if TabControl1.ActiveTab = tabCard     then
    actFileCardNew.Execute
  else if TabControl1.ActiveTab = tabPascal   then
    actFilePascalNewScript.Execute
end;

procedure TFMainDesigner.actFileOpenExecute(Sender: TObject);
begin
  if      TabControl1.ActiveTab = tabSettings then
    actFileProjectOpen.Execute
  else if TabControl1.ActiveTab = tabCard     then
    actFileCardOpen.Execute
  else if TabControl1.ActiveTab = tabPascal   then
    actFilePascalOpen.Execute
end;

procedure TFMainDesigner.actFileSaveExecute(Sender: TObject);
begin
  if      TabControl1.ActiveTab = tabSettings then
    actFileProjectSave.Execute
  else if TabControl1.ActiveTab = tabCard     then
    actFileCardSave.Execute
  else if TabControl1.ActiveTab = tabPascal   then
    actFilePascalSave.Execute
end;

procedure TFMainDesigner.actFileSaveAsExecute(Sender: TObject);
begin
  if      TabControl1.ActiveTab = tabSettings then
    actFileProjectSaveAs.Execute
  else if TabControl1.ActiveTab = tabCard     then
    actFileCardSaveAs.Execute
  else if TabControl1.ActiveTab = tabPascal   then
    actFilePascalSaveAs.Execute
end;

procedure TFMainDesigner.actFileSaveAsRelativePathsExecute(Sender: TObject);

  function TransformToRelativePath (FileName: string): string;
  var
    ExePath, FilePath, RelativePath, FName: string;
  begin
    ExePath      := ExtractFilePath (ParamStr (0));
    FilePath     := ExtractFilePath (FileName);
    FName        := ExtractFileName (FileName);
    RelativePath := ExtractRelativePath(ExePath, FilePath);
    result       := RelativePath + FName;
  end;
begin
  if not Settings.CardFileName.IsEmpty then
  begin
    Settings.CardFileName := TransformToRelativePath (Settings.CardFileName);
    edCardFileName.Text   := Settings.CardFileName;
  end;

  if not Settings.ScriptFileName.IsEmpty then
  begin
    Settings.ScriptFileName := TransformToRelativePath (Settings.ScriptFileName);
    edScriptFileName.Text   := Settings.ScriptFileName;
  end;

  if not Settings.BackgroundFileName.IsEmpty then
  begin
    Settings.BackgroundFileName := TransformToRelativePath (Settings.BackgroundFileName);
    edImageFileName.Text        := Settings.BackgroundFileName;
  end;
end;

procedure TFMainDesigner.actFileSaveAllExecute(Sender: TObject);
begin
  if Settings.Changed then
    actFileProjectSave.Execute;

  if Card.Changed then
    actFileCardSave.Execute;

  if Script.Changed then
    actFilePascalSave.Execute;
end;

procedure TFMainDesigner.actFileToggleCardScriptExecute(Sender: TObject);
begin
  if TabControl1.ActiveTab = tabCard then
    TabControl1.ActiveTab := tabPascal
  else
    TabControl1.ActiveTab := tabCard;
end;


procedure TFMainDesigner.btnSetClrClick(Sender: TObject);
begin
   ComboColorBox_backround.Color := TAlphaColor(StrToInt(edtBackrndColor.Text)) ;
end;

procedure TFMainDesigner.ComboColorBox_backroundChange(Sender: TObject);
begin
  Settings.BackgroundColor                   := ComboColorBox_backround.Color ;
  FrmPreview.ImageBackground.BackgroundColor  := Settings.BackgroundColor;
end;

procedure TFMainDesigner.spTransitionStepsChange(Sender: TObject);
begin
    Settings.TransitionSteps := Trunc (spTransitionSteps.Value);
end;

{$endregion}

{$endregion}

end.
