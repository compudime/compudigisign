unit uMainCardPlayer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, uCards,
  FMX.StdCtrls, System.IOUtils, FMX.Objects, FMX.Gestures, System.Actions,
  FMX.ActnList, FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  FMX.TabControl, System.ImageList, FMX.ImgList, FMX.Edit, FMX.Layouts,
  FMX.EditBox, FMX.SpinBox, FMX.Colors, FMX.ListBox, IniFiles,
  //fmx.Dialogs.Android,
  FMX.Memo.Types;

type
  TFMainPlayer = class(TForm)
    pnlConfig: TPanel;
    ActionList1: TActionList;
    actApply: TAction;
    ImageList1: TImageList;
    actConfig: TAction;
    GestureManager1: TGestureManager;
    toolbarConfig: TToolBar;
    btnReturnDisplay: TSpeedButton;
    Label1: TLabel;
    edCardFileName: TEdit;
    btnOpenCard: TSpeedButton;
    Label2: TLabel;
    edSettingsFileName: TEdit;
    btnOpenSettings: TSpeedButton;
    Label3: TLabel;
    edScriptFileName: TEdit;
    btnOpenScript: TSpeedButton;
    btnApply: TButton;
    pnlDisplay: TPanel;
    StatusBar1: TStatusBar;
    labVersion: TLabel;
    labPages: TLabel;
    labSystem: TLabel;
    pnlError: TPanel;
    lbError: TLabel;
    btnGetFileLocation: TButton;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReturnDisplayClick(Sender: TObject);
    procedure btnConfigPageClick(Sender: TObject);
    procedure actApplyExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure btnOpenCardClick(Sender: TObject);
    procedure btnOpenSettingsClick(Sender: TObject);
    procedure btnOpenScriptClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure actConfigExecute(Sender: TObject);
    procedure btnGetFileLocationClick(Sender: TObject);
    procedure FormGesture(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
  private
    { Private declarations }
    Background:       TBackgroundImage;
    Grid:             TGridCard;
    Settings:         TProyectSettings;
    SettingsFileName: string;
    CardFileName:     string;
    ScriptFileName:   string;
    JSONFileName:     string;
    FCurrentPanel:    TPanel;
    {$IFDEF ANDROID}
    GrantedReadPriv:  boolean;
    {$ENDIF}
    procedure ReadParameters;
    procedure UpdatePageNumber;
    procedure RecalculateRowCols (Sender: TObject);
    procedure ChangePage (Sender: TObject);
    procedure ReloadProject;
    function IsConfigured: boolean;
    procedure SetCurrentPanel (Value: TPanel);
    procedure AnalyzeJSON (Sender: TObject; JSON: string; var RootItem: string; var DataDepth: integer);
    procedure ChangeGridError (Sender: TObject; ErrorMsg: string);
    procedure MoveApplicationToScreen;
    {$IFDEF MSWINDOWS}
    procedure OpenFile (FileExt: string; Edit: TEdit);
    {$ENDIF}
    {$IFDEF ANDROID}
    procedure CheckIfReadExternalStorageIsGranted;
    function LocalIniFile: string;
    function AndroidPath (FileName: string): string;
    {$ENDIF}
  public
    { Public declarations }
    property CurrentPanel: TPanel read FCurrentPanel write SetCurrentPanel;
  end;

var
  FMainPlayer: TFMainPlayer;

implementation
{$IFDEF MSWINDOWS}
uses Windows, FMX.DialogService.Sync;
{$ENDIF}
{$IFDEF ANDROID}
uses Androidapi.Helpers, Androidapi.JNI.JavaTypes, Androidapi.JNI.GraphicsContentViewText,
  System.Permissions, Androidapi.JNI.App, Androidapi.JNI.OS;
{$ENDIF}

{$R *.fmx}
{$IFDEF ANDROID}
const
  coAndroidIniFile = 'Player.ini';
{$ENDIF}

function RemoveQuotes (s: string): string;
begin
  if (Length (s) >= 2) and (s [1] = '"') and (s [Length (s)] = '"') then
  begin
    Delete (s, 1, 1);
    Delete (s, Length (s), 1);
  end;
  result := s;
end;

{$region 'Version routines'}
function GetAppVersionStr: string;
var
{$IFDEF ANDROID}
   PackageManager: JPackageManager;
   PackageInfo: JPackageInfo;
{$ENDIF}
{$IFDEF MACOS}
  CFStr: CFStringRef;
  Range: CFRange;
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
{$ENDIF}
begin
{$IFDEF MACOS}
  CFStr := CFBundleGetValueForInfoDictionaryKey(
    CFBundleGetMainBundle, kCFBundleVersionKey);
  Range.location := 0;
  Range.length := CFStringGetLength(CFStr);
  SetLength(Result, Range.length);
  CFStringGetCharacters(CFStr, Range, PChar(Result));
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
{$ENDIF}
{$IFDEF ANDROID}
   PackageManager := SharedActivityContext.getPackageManager;
   PackageInfo := PackageManager.getPackageInfo
     (SharedActivityContext.getPackageName, 0);
   result := JStringToString(PackageInfo.versionName);
{$ENDIF}
end;
{$endregion}

procedure TFMainPlayer.FormCreate(Sender: TObject);
begin
  {$IFDEF ANDROID}
  GrantedReadPriv         := False;
  btnOpenCard.Visible     := False;
  btnOpenSettings.Visible := False;
  btnOpenScript.Visible   := False;
  {$ENDIF}
  pnlConfig.Visible       := False;
  toolbarConfig.Visible   := False;
  pnlDisplay.Align        := TAlignLayout.Client;
  pnlError.Visible        := False ;
  labVersion.Text         := GetAppVersionStr;
  SettingsFileName        := '';
  CardFileName            := '';
  ScriptFileName          := '';
  JSONFileName            := '';
  Background              := TBackgroundImage.Create(pnlDisplay);
  Background.Parent       := pnlDisplay;
  Background.Align        := TAlignLayout.Client;
  Grid                    := TGridCard.Create (Background);
  Grid.Parent             := Background;
  Grid.Align              := TAlignLayout.Client;
  Settings                := TProyectSettings.Create(Self);
  Grid.OnRecalculeColRows := RecalculateRowCols;
  Grid.OnChangePage       := ChangePage;
  Grid.OnAnalyzeJSON      := AnalyzeJSON;
  Grid.OnGridError        := ChangeGridError;
  FCurrentPanel           := pnlDisplay;
end;

procedure TFMainPlayer.FormDestroy(Sender: TObject);
begin
  SettingsFileName := '';
  CardFileName     := '';
  ScriptFileName   := '';
  JSONFileName     := '';

  Settings.Free;
  Grid.Free;
  Background.Free;
end;

procedure TFMainPlayer.FormGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
  {$IFDEF ANDROID}
  if (EventInfo.GestureID = igiDoubleTap) and (CurrentPanel = pnlDisplay) then
  begin
    CurrentPanel := pnlConfig;
    Handled      := True;
  end;
  {$ENDIF}
end;

procedure TFMainPlayer.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  {$IFDEF MSWINDOWS}
  if (CurrentPanel = pnlDisplay) then
  begin
    if Key = Ord (27) then
    begin
      if TDialogServiceSync.MessageDialog('Do you want to exit the application?',
                                           TMsgDlgType.mtConfirmation, mbOKCancel, TMsgDlgBtn.mbYes, 0) = mrOk  then
        Self.Close;

    end
  end;
  {$ENDIF}
end;

procedure TFMainPlayer.FormShow(Sender: TObject);
begin
  {$IFDEF ANDROID}
  CheckIfReadExternalStorageIsGranted;
  {$ENDIF}



  ReadParameters;

  if IsConfigured then
  begin
    ReloadProject;
    MoveApplicationToScreen;
    Grid.Start;
  end
  else
    CurrentPanel := pnlConfig;
end;

procedure TFMainPlayer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Grid.Stop;
end;

procedure TFMainPlayer.SetCurrentPanel (Value: TPanel);
begin
  if FCurrentPanel <> Value then
  begin
    FCurrentPanel         := Value;
    pnlDisplay.Visible    := FCurrentPanel = pnlDisplay;
    Background.Visible    := FCurrentPanel = pnlDisplay;
    Grid.Visible          := FCurrentPanel = pnlDisplay;
    pnlConfig.Visible     := FCurrentPanel = pnlConfig;
    toolbarConfig.Visible := FCurrentPanel = pnlConfig;

    if pnlDisplay.Visible then
      Grid.Start
    else
    begin
      Grid.Stop;
      edCardFileName.Text     := CardFileName;
      edSettingsFileName.Text := SettingsFileName;
      edScriptFileName.Text   := ScriptFileName;
    end;
  end;
end;

procedure TFMainPlayer.btnOpenCardClick(Sender: TObject);
begin
  {$IFDEF MSWINDOWS}
  OpenFile ('.card', edCardFileName);
  {$ENDIF}
end;

procedure TFMainPlayer.btnOpenSettingsClick(Sender: TObject);
begin
  {$IFDEF MSWINDOWS}
  OpenFile ('.ini', edSettingsFileName);
  {$ENDIF}
end;

procedure TFMainPlayer.btnOpenScriptClick(Sender: TObject);
begin
  {$IFDEF MSWINDOWS}
  OpenFile ('.pas', edScriptFileName);
  {$ENDIF}
end;

procedure TFMainPlayer.actApplyExecute(Sender: TObject);
{$IFDEF ANDROID}
var
  Ini: TIniFile;
{$ENDIF}
begin
  CardFileName     := edCardFileName.Text;
  SettingsFileName := edSettingsFileName.Text;
  ScriptFileName   := edScriptFileName.Text;

  {$IFDEF ANDROID}
  Ini := TIniFile.Create(LocalIniFile);
  try
    Ini.WriteString('Settings', 'CardFileName',     CardFileName);
    Ini.WriteString('Settings', 'SettingsFileName', SettingsFileName);
    Ini.WriteString('Settings', 'ScriptFileName',   ScriptFileName);
  finally
    Ini.Free;
  end;
  {$ENDIF}

  ReloadProject;
end;

procedure TFMainPlayer.actConfigExecute(Sender: TObject);
begin
  CurrentPanel := pnlConfig;
end;

{$IFDEF MSWINDOWS}
procedure TFMainPlayer.OpenFile (FileExt: string; Edit: TEdit);
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(Self);
  try
    OpenDlg.Title      := 'Select file';
    OpenDlg.DefaultExt := FileExt;
    OpenDlg.FileName   := Edit.Text;
    if FileExt = '.card' then
      OpenDlg.Filter := 'Card templates|*.card|All files|*.*'
    else if FileExt = '.ini' then
      OpenDlg.Filter := 'Card project|*.ini|All files|*.*'
    else if FileExt = '.pas' then
      OpenDlg.Filter := 'Pascal script|*.pas|All files|*.*';

    if OpenDlg.Execute then
      Edit.Text := OpenDlg.FileName;
  finally
    OpenDlg.Free;
  end;
end;
{$ENDIF}

procedure TFMainPlayer.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  InConfig:  boolean;
begin
  InConfig         := CurrentPanel = pnlConfig;
  actApply.Enabled := (CurrentPanel = pnlConfig) and
                      ((edCardFileName.Text     <> CardFileName)     or
                       (edSettingsFileName.Text <> SettingsFileName) or
                       (edScriptFileName.Text   <> ScriptFileName));
end;

procedure TFMainPlayer.btnConfigPageClick(Sender: TObject);
begin
  CurrentPanel := pnlConfig;
end;

procedure TFMainPlayer.btnReturnDisplayClick(Sender: TObject);
begin
  CurrentPanel := pnlDisplay;
  MoveApplicationToScreen;
end;

procedure TFMainPlayer.UpdatePageNumber;
begin
  labPages.Text := Format ('%d/%d', [Grid.CurrentPage + 1, Grid.PageCount]);
end;

procedure TFMainPlayer.RecalculateRowCols (Sender: TObject);
begin
  UpdatePageNumber;
end;

procedure TFMainPlayer.ChangePage (Sender: TObject);
begin
  UpdatePageNumber;
end;

procedure TFMainPlayer.ReadParameters;
var
  {$IFDEF MSWINDOWS}
  i: integer;
  FileName: string;
  {$ENDIF}
  {$IFDEF ANDROID}
  Ini: TIniFile;
  {$ENDIF}
begin
  {$IFDEF MSWINDOWS}
  for i := 1 to ParamCount do
  begin
    FileName := ParamStr (i);
    if ExtractFilePath (FileName) = '' then
      FileName := ExtractFilePath (ParamStr (0)) + FileName;

    if UpperCase (ExtractFileExt (FileName)) = '.CARD' then
    begin
      CardFileName := FileName;
      if SettingsFileName.IsEmpty then
        SettingsFileName := ExtractFilePath (FileName) + System.IOUtils.TPath.GetFileNameWithoutExtension (FileName) + '.ini';

      if ScriptFileName.IsEmpty then
        ScriptFileName := ExtractFilePath (FileName) + System.IOUtils.TPath.GetFileNameWithoutExtension (FileName) + '.pas';
    end
    else if UpperCase (ExtractFileExt (FileName)) = '.INI' then
      SettingsFileName := FileName
    else if UpperCase (ExtractFileExt (FileName)) = '.PAS' then
      ScriptFileName   := FileName
  end;
  {$ENDIF}
  {$IFDEF ANDROID}
  Ini := TIniFile.Create(LocalIniFile);
  try
    CardFileName     := Ini.ReadString('Settings', 'CardFileName',     '');
    SettingsFileName := Ini.ReadString('Settings', 'SettingsFileName', '');
    ScriptFileName   := Ini.ReadString('Settings', 'ScriptFileName',   '');
  finally
    Ini.Free;
  end;
  {$ENDIF}
end;

procedure TFMainPlayer.ReloadProject;
begin
  try
    Grid.ErrorMsg := '';
    Settings.Grid := nil;
    Grid.Script   := '';
    {$IFDEF MSWINDOWS}
    Settings.LoadFromIniFile(SettingsFileName);
    {$ELSE}
    Settings.LoadFromIniFile(AndroidPath (SettingsFileName));
    {$ENDIF}
    Background.ClearBackgroundBitmap;
    Background.BackgroundType  := Settings.BackgroundType;
    Background.BackgroundColor := Settings.BackgroundColor;

    {$IFDEF MSWINDOWS}
    if not Settings.BackgroundFileName.IsEmpty and FileExists (Settings.BackgroundFileName) then
      Background.LoadBackgroundBitmap(Settings.BackgroundFileName);

    if FileExists (ScriptFileName) then
      Grid.Script := TFile.ReadAllText(ScriptFileName);

    if FileExists (CardFileName) then
      Grid.LoadTemplate(CardFileName)
    else
      Grid.ErrorMsg := Format ('Card file [%s] not found', [CardFileName]);
    {$ENDIF}
    {$IFDEF ANDROID}
    if not Settings.BackgroundFileName.IsEmpty and FileExists (AndroidPath (Settings.BackgroundFileName)) then
      Background.LoadBackgroundBitmap(AndroidPath (Settings.BackgroundFileName));

    if FileExists (AndroidPath (ScriptFileName)) then
      Grid.Script := TFile.ReadAllText(AndroidPath (ScriptFileName));

    if FileExists (AndroidPath (CardFileName)) then
      Grid.LoadTemplate(AndroidPath (CardFileName))
    else
      Grid.ErrorMsg := Format ('Card file [%s] not found', [CardFileName]);

    if (Settings.JSONSourceType = jtFile) and not Settings.JSONFileName.IsEmpty then
      Settings.JSONFileName := AndroidPath(Settings.JSONFileName);
    {$ENDIF}
  finally
    Settings.Grid := Grid;
  end;
end;

function TFMainPlayer.IsConfigured: boolean;
begin
  {$IFDEF MSWINDOWS}
  result := not CardFileName.IsEmpty and FileExists (CardFileName);
  {$ENDIF}
  {$IFDEF ANDROID}
  result := not CardFileName.IsEmpty and FileExists (AndroidPath (CardFileName));
  {$ENDIF}
end;

procedure TFMainPlayer.MoveApplicationToScreen;
begin
  Screen.UpdateDisplayInformation;
  if Screen.DisplayCount > 1 then
  begin
    self.WindowState := TWindowState.wsNormal;
    self.SetBounds(Screen.Displays[Settings.PlayerMonitorNum - 1].BoundsRect);
    self.WindowState := TWindowState.wsMaximized;
  end;
end;

{$IFDEF ANDROID}
procedure TFMainPlayer.CheckIfReadExternalStorageIsGranted;
begin

  PermissionsService.RequestPermissions([JStringToString(TJManifest_permission.JavaClass.READ_EXTERNAL_STORAGE)],
    procedure(const APermissions: TArray<string>; const AGrantResults: TArray<TPermissionStatus>)
    begin
      GrantedReadPriv := (Length(AGrantResults) = 1) and (AGrantResults[0] = TPermissionStatus.Granted);
    end);
end;

function TFMainPlayer.LocalIniFile: string;
begin

  result := System.IOUtils.TPath.GetPublicPath + System.SysUtils.PathDelim + coAndroidIniFile;
end;

function TFMainPlayer.AndroidPath (FileName: string): string;
begin
  //result := System.IOUtils.TPath.GetSharedDocumentsPath + System.SysUtils.PathDelim + FileName;
 result := System.IOUtils.TPath.GetPublicPath + System.SysUtils.PathDelim + FileName;
end;
{$ENDIF}

procedure TFMainPlayer.AnalyzeJSON (Sender: TObject; JSON: string; var RootItem: string; var DataDepth: integer);
begin
  uCards.AnalyzeJSON(JSON, RootItem, DataDepth);
end;

procedure TFMainPlayer.btnGetFileLocationClick(Sender: TObject);
begin
Memo1.Lines.Add('GetSharedDocumentsPath: '+System.IOUtils.TPath.GetSharedDocumentsPath) ;
Memo1.Lines.Add('GetDocumentsPath: '+System.IOUtils.TPath.GetDocumentsPath) ;
Memo1.Lines.Add('GetPublicPath: ' + System.IOUtils.TPath.GetPublicPath) ;
Memo1.Lines.Add('GetSharedDownloadsPath: '+   System.IOUtils.TPath.GetSharedDownloadsPath) ;
end;

procedure TFMainPlayer.ChangeGridError (Sender: TObject; ErrorMsg: string);
begin
  pnlError.Visible := not ErrorMsg.IsEmpty;
  lbError.Text     := ErrorMsg;
end;


end.
