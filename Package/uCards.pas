{$DEFINE RTML}
unit uCards;

interface
uses System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FMX.StdCtrls, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  REST.Response.Adapter, Data.Bind.EngExt, FMX.Types, FMX.Controls, FMX.Forms,
  FMX.Graphics, FMX.Dialogs, FMX.Objects, System.Rtti, Data.DBXJSONCommon,
  FMX.ScripterInit, atScript, atScripter, FMX.TreeView, System.MaskUtils,
  IniFiles, DateUtils, System.IOUtils
  {$IFDEF RTML}
  ,uRTMLLabel
  {$ENDIF}
  ;

type
  TRESTJSONServiceClient = class (TComponent)
  private
    FBaseURL:        string;
    FServiceRequest: string;
    FErrorMsg:       string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    function Execute: string;
    property BaseURL: string read FBaseURL write FBaseURL;
    property ServiceRequest: string read FServiceRequest write FServiceRequest;
    property ErrorMsg: string read FErrorMsg;
  end;

  TPaletteTool = (ptDeselect, ptLabel, ptImage, ptLine, ptRectangle, ptRoundedRect, ptEllipse, ptCircle, ptArc);
  TMouseAction = (maNone, maSelect, maMultiSelect, maCreateObject, maStartDrag, maDragDrop, maDrop);
  TJSONDataSet = class (TComponent)
  private
    FJSONSource:                string;
    FRootPath:                  string;
    FScanDepth:                 integer;
    RESTResponseDataSetAdapter: TRESTResponseDataSetAdapter;
    FMemTable:                  TFDMemTable;
    FRecordCount:               integer;
    FUpdating:                  integer;
    procedure SetJSONSource (Value: string);
    procedure SetRootPath (Value: string);
    procedure SetScanDepth (Value: integer);
    procedure UpdateTable;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    property MemTable: TFDMemTable read FMemTable;
    property JSONSource: string read FJSONSource write SetJSONSource;
    property RootPath: string read FRootPath write SetRootPath;
    property ScanDepth: integer read FScanDepth write SetScanDepth default 2;
    property RecordCount: integer read FRecordCount;
  end;

  TJSONFieldLink = class (TComponent)
  private
    FJSONField:   string;
    FJSONDataSet: TJSONDataSet;
    procedure SetJSONField (Value: string);
    procedure SetJSONDataSet (Value: TJSONDataSet);
  protected
    procedure Notification (AComponent: TComponent; Operation: TOperation); override;
    property Name;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    function GetFieldValue: string;
    property JSONDataSet: TJSONDataSet read FJSONDataSet write SetJSONDataSet;
  published
    property JSONField: string read FJSONField write SetJSONField;
  end;

  TJSONLabel = class;
  TStringCase = (scNormal, scUpperCase, scLowerCase, scNameCase);
  TLabelBehavior = (lbRightTrim, lbLeftTrim, lbHideIfEmpty);
  TSetLabelBehavior = set of TLabelBehavior;
  TLabelFormat = class
  private
    FJSONLabel:     TJSONLabel;
    FStringCase:    TStringCase;
    FLabelBehavior: TSetLabelBehavior;
    FStringFormat:  string;
  public
    constructor Create (AJSONLabel: TJSONLabel);
    destructor Destroy; override;
    function ApplyFormat (s: string): string;
    property JSONLabel: TJSONLabel read FJSONLabel;
  published
    property StringCase: TStringCase read FStringCase write FStringCase default scNormal;
    property LabelBehavior: TSetLabelBehavior read FLabelBehavior write FLabelBehavior;
    property StringFormat: string read FStringFormat write FStringFormat;
  end;

  {$IFDEF RTML}
  TJSONLabel = class (TRTMLLabel)
  {$ELSE}
  TJSONLabel = class (TLabel)
  {$ENDIF}
  private
    FJSONLink:              TJSONFieldLink;
    FLabelFormat:           TLabelFormat;
    FOnDisplayJSONLabel:    string;
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property JSONLink: TJSONFieldLink read FJSONLink;
    property LabelFormat: TLabelFormat read FLabelFormat;
    property OnDisplayJSONLabel: string read FOnDisplayJSONLabel write FOnDisplayJSONLabel;
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TJSONImage = class (TImage)
  private
    FIsBitmapEmpty:         boolean;
    FJSONLink:              TJSONFieldLink;
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    property IsBitmapEmpty: boolean read FIsBitmapEmpty write FIsBitmapEmpty;
  published
    property JSONLink: TJSONFieldLink read FJSONLink;
    property Bitmap;
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardLine = class (TLine)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardRectangle = class (TRectangle)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardRoundRect = class (TRoundRect)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardEllipse = class (TEllipse)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardCircle = class (TCircle)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TCardArc = class (TArc)
  private
    FOnBeforeObjectDisplay: string;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnBeforeObjectDisplay: string read FOnBeforeObjectDisplay write FOnBeforeObjectDisplay;
  end;

  TSelectionCtrl = class (TSelection)
  private
    FControl: TControl;
    procedure SetControl (Value: TControl);
  public
    constructor Create (AOwner: TComponent); override;
    procedure UpdatePosition;
    procedure AdjustCtrlToSelection;
    property Control: TControl read FControl write SetControl;
  end;

  TTreeViewStructureItem = class (TTreeViewItem)
  private
    Ctrl: TControl;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TBackgroundType = (btColor, btImage);
  TBackgroundImage = class (TImage)
  private
    FBackgroundType:     TBackgroundType;
    FBackgroundColor:    TAlphaColor;
    FBackgroundBitmap:   TBitmap;
    FOnChangeBackground: TNotifyEvent;
    procedure SetBackgroundType (Value: TBackgroundType);
    procedure SetBackgroundColor (Value: TAlphaColor);
    procedure SetBackgroundBitmap (Value: TBitmap);
  protected
    procedure Resize; override;
    procedure ChangeBackground; virtual;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PaintBackground;
    procedure LoadBackgroundBitmap (FileName: string);
    procedure ClearBackgroundBitmap;
    property BackgroundBitmap: TBitmap read FBackgroundBitmap write SetBackgroundBitmap;
  published
    property BackgroundType: TBackgroundType read FBackgroundType write SetBackgroundType default btColor;
    property BackgroundColor: TAlphaColor read FBackgroundColor write SetBackgroundColor;
    property OnChangeBackground: TNotifyEvent read FOnChangeBackground write FOnChangeBackground;
  end;

  PJSONFieldValue = ^RJSONFieldValue;
  RJSONFieldValue = record
    FieldName: string;
    Value:     string;
  end;

  TChangeControlEvent = procedure (Sender: TObject; Ctrl: TControl; AOperation: TOperation) of Object;
  TControlChangeNameEvent = procedure (Sender: TObject; Ctrl: TControl; OldName, NewName: string) of Object;
  TGetStructureImageIndex = procedure (Sender: TObject; Tool: TPaletteTool; var ImageIndex: integer) of Object;
  TInvalidFileNameEvent = procedure (Sender: TObject; FileName: string) of Object;
  TJSONCard = class (TPanel)
  private
    FLoading:                  boolean;
    FChanged:                  boolean;
    FBackground:               TBackgroundImage;
    FJSONDataSet:              TJSONDataSet;
    FDesignMode:               boolean;
    FCurrentTool:              TPaletteTool;
    FExcludedProperties:       TStringList;
    FSelectedCtrls:            TList;
    FLstControls:              TList;
    FGarbageCtrls:             TList;
    FOnChangeCtrlSelection:    TNotifyEvent;
    FOnChangeCurrentTool:      TNotifyEvent;
    FOnClearLstControls:       TNotifyEvent;
    FOnChange:                 TNotifyEvent;
    FOnControlNotitication:    TChangeControlEvent;
    FOnControlChangeName:      TControlChangeNameEvent;
    FOnGetStructureImageIndex: TGetStructureImageIndex;
    FUpdatingSelection:        boolean;
    TimerPurgeGarbage:         TTimer;
    DragEnabled:               boolean;
    Resizing:                  boolean;
    MouseDownPos,
    LastPosition:              TPointF;
    ResizingCtrl:              TControl;
    TimerChange:               TTimer;
    FTreeStructure:            TTreeView;
    FDestroying:               boolean;
    FScripter:                 TatScripter;
    FJSONFields:               TList;
    {$IFDEF ANDROID}
    CtrlNames:                 TStringList;
    {$ENDIF}

    //Properties setters and getters
    procedure SetDesignMode (Value: boolean);
    procedure SetCurrentTool (Value: TPaletteTool);
    function GetBackGroundColor: TAlphaColor;
    procedure SetBackgroundColor (Value: TAlphaColor);
    procedure SetChanged (Value: boolean);
    procedure SetCardWidth (Value: Single);
    function GetCardWidth: Single;
    procedure SetCardHeight (Value: Single);
    function GetCardHeight: Single;
    procedure SetCardOpacity (Value: Single);
    function GetCardOpacity: Single;
    function GetBackgroundType: TBackgroundType;
    procedure SetBackgroundType (Value: TBackgroundType);
    procedure SetTreeStructure (Value: TTreeView);
    procedure TimerChangeTimer (Sender: TObject);
    procedure UpdateMode;
    procedure PopulateExcludeProperties;
    procedure BackgroundImageChange (Sender: TObject);

    //Grid routines
    procedure UpdateGrid;

    //Control routines
    procedure CtrlsPostOrder (Ctrls: TList);

    {$IFDEF ANDROID}
    //Android Control Routines
    procedure ClearControlNames;
    procedure AssignControlNames;
    {$ENDIF}

    //Garbage routines
    procedure PurgeGarbageCtrls;
    procedure AddGarbageCtrl (Ctrl: TControl);
    procedure TimerPurgeGarbageTimer(Sender: TObject);

    //Tree structure routines
    procedure BuildTreeStructure;
    function FindItemTreeStructure (Ctrl: TControl): TTreeViewStructureItem;
    procedure UpdateNameTreeStructure (Ctrl: TControl);
    procedure AddCtrlToTreeStructure (Ctrl: TControl);
    procedure UpdateSelectionTreeStructure;
    procedure UserChangeSelectionTreeStructure;
    procedure TreeStructureMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);

  protected
    procedure Notification (AComponent: TComponent; Operation: TOperation); override;

    //Event handlers
    procedure ChangeCtrlSelection; virtual;
    procedure ChangeCurrentTool; virtual;
    procedure ControlNotification (Ctrl: TControl; AOperation: TOperation); virtual;
    procedure Change; virtual;
    procedure ChangeControlName (Ctrl: TControl; OldName, NewName: string); virtual;
    procedure GetStructureImageIndex (Tool: TPaletteTool; var ImageIndex: integer); virtual;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;

    //Stream and File routines
    procedure SaveToStream (Stream: TStream);
    procedure LoadFromStream (Stream: TStream);
    procedure SaveToFile (FileName: string);
    procedure LoadFromFile (FileName: string);
    procedure LoadBackgroundFile (FileName: string);

    //RTTI and JSON routines
    function GetPropText(Obj: TObject; Prop: TRttiProperty; SetAsValues: boolean = True): string;
    procedure SetPropText(Obj: TObject; Prop: TRttiProperty; Text: string);
    function IsPropertyDefaultValue (Prop: TRttiProperty; const Text: string): boolean;
    function IsExcludedProperty (ClassName, PropertyName: string): boolean;
    function CtrlToJSON (Ctrl: TControl): string;
    procedure ReadCtrlFromJSON (Ctrl: TControl; JSON: string);

    //Mouse routines
    function PnlMouseAction (Button: TMouseButton; Shift: TShiftState): TMouseAction;
    procedure BackgroundMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure BackgroundMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
    procedure BackgroundMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);

    //Selection routines
    procedure ClearSelection;
    function GetSelected: TControl;
    procedure AddSelection (Ctrl: TControl);
    procedure DeleteSelection (Ctrl: TControl);
    function IndexsSelectedCtrl (Ctrl: TControl): integer;
    function IsSelectedCtrl (Ctrl: TControl): boolean;
    function IsSelectedAncestorCtrl (Ctrl: TControl): boolean;
    procedure UpdateSelections;

    //Control routines
    function ControlAtPos (X, Y: Single): TControl;
    function GetControlByName (CtrlName: string): TControl;
    function GenerateCtrlName (Ctrl: TControl): string;
    function CreateCtrl (Tool: TPaletteTool; CtrlParent: TControl; X, Y: Single; JSON: string = ''): TControl;
    procedure ClearLstControls;
    procedure AddControl (Ctrl: TControl);
    procedure DeleteControl (Ctrl: TControl);
    procedure DeleteSelectedControls;
    function GetControlRealPosition (Ctrl: TControl): TBounds;
    function GetControlDepth (Ctrl: TControl): integer;
    function GetControlType (Control: TControl): TPaletteTool;
    function ChangeControlParent (Ctrl, NewParent: TControl): boolean;
    function IsChildOf (Tested, Ctrl: TControl): boolean;
    procedure ForceCtrlIntoViewport (Ctrl: TControl);
    procedure RepaintControls;

    //Align routines
    procedure AlignVertTop;
    procedure AlignVertMiddle;
    procedure AlignVertBottom;
    procedure AlignHorizLeft;
    procedure AlignHorizCenter;
    procedure AlignHorizRight;

    //Scripter routines
    procedure ScripterCreate;
    procedure ScripterRelease;
    procedure ScripterRegisterComponents;
    procedure ClearJSONFields;
    function IndexOfJSONField (FieldName: string): integer;
    function GetJSONFieldCount: integer;
    function GetJSONFieldName (Index: integer): string;
    function GetJSONFieldValueByIndex (Index: integer): string;
    function GetJSONFieldValue (FieldName: string): string;
    procedure AddJSONFieldValue (FieldName, Value: string);

    //Public properties
    property JSONDataSet: TJSONDataSet read FJSONDataSet;
    property ExcludedProperties: TStringList read FExcludedProperties;
    property Selected: TControl read GetSelected;
    property SelectedCtrls: TList read FSelectedCtrls;
    property LstControls: TList read FLstControls;
    property UpdatingSelection: boolean read FUpdatingSelection;
    property Changed: boolean read FChanged write SetChanged;
    property Scripter: TatScripter read FScripter;
    property Background: TBackgroundImage read FBackground;
  published
    //Events
    property OnChangeCtrlSelection: TNotifyEvent read FOnChangeCtrlSelection write FOnChangeCtrlSelection;
    property OnChangeCurrentTool: TNotifyEvent read FOnChangeCurrentTool write FOnChangeCurrentTool;
    property OnControlNotification: TChangeControlEvent read FOnControlNotitication write FOnControlNotitication;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnControlChangeName: TControlChangeNameEvent read FOnControlChangeName write FOnControlChangeName;
    property OnGetStructureImageIndex: TGetStructureImageIndex read FOnGetStructureImageIndex write FOnGetStructureImageIndex;

    //Card properties
    property DesignMode: boolean read FDesignMode write SetDesignMode default True;
    property CurrentTool: TPaletteTool read FCurrentTool write SetCurrentTool default ptDeselect;
    property BackgroundType: TBackgroundType read GetBackgroundType write SetBackgroundType;
    property BackgroundColor: TAlphaColor read GetBackGroundColor write SetBackgroundColor;
    property CardWidth: Single read GetCardWidth write SetCardWidth;
    property CardHeight: Single read GetCardHeight write SetCardHeight;
    property CardOpacity: Single read GetCardOpacity write SetCardOpacity;
    property TreeStructure: TTreeView read FTreeStructure write SetTreeStructure;
  end;

  TRESTThread = class (TThread)
  private
    FBaseURL:        string;
    FServiceRequest: string;
    FJSONResult:     string;
    FErrorMsg:       string;
    FFinished:       boolean;
  public
    destructor Destroy; override;
    procedure Initialize;
    procedure Execute; override;
    property BaseURL: string read FBaseURL write FBaseURL;
    property ServiceRequest: string read FServiceRequest write FServiceRequest;
    property JSONResult: string read FJSONResult;
    property Finished: boolean read FFinished;
    property ErrorMsg: string read FErrorMsg;
  end;

  PCardPageInfo = ^RCardPageInfo;
  RCardPageInfo = record
    CardIndex: integer;
    Top, Left:    Single;
    Top2, Left2:  Single;
    Row, Col:     integer;
  end;
  PRowInfo = ^RRowInfo;
  RRowInfo = record
    Row, First, Last: integer;
  end;
  PPageInfo = ^RPageInfo;
  RPageInfo = record
    PageIndex:   integer;
    First, Last: integer;
    Cards: TList;
    Rows:  TList;
  end;

  TTransitionType = (ttNone, ttZoomInOut, ttScrollFullRow, ttFadeInOut);
  TJSONSourceType = (jtManual, jtRESTService, jtFile);
  TAnalyzeJSONEvent = procedure (Sender: TObject; JSON: string; var RootItem: string; var DataDepth: integer) of Object;
  TGridErrorMessage = procedure (Sender: TObject; ErrorMsg: string) of Object;
  TGridCard = class (TImage)
  private
    FCanResize:           boolean;
    FJSONDataSet:         TJSONDataSet;
    FBaseCard:            TJSONCard;
    FRowCount:            integer;
    FColCount:            integer;
    FMaxGridCards:        integer;
    FGridMargin:          TBounds;
    FPageCount:           integer;
    FCurrentPage:         integer;
    FHorizInnerMargin:    Single;
    FVertInnerMargin:     Single;
    FOnRecalculeColRows:  TNotifyEvent;
    FOnChangePage:        TNotifyEvent;
    FOnAnalyzeJSON:       TAnalyzeJSONEvent;
    FOnGridError:         TGridErrorMessage;
    FCards:               TList;
    FAutoAdjust:          boolean;
    FRESTServerAddress:   string;
    FRESTRequest:         string;
    FRESTInterval:        integer;
    FJSONFileName:        string;
    FJSONSourceType:      TJSONSourceType;
    FJSONSource:          string;
    FTransitionType:      TTransitionType;
    FTransitionInterval:  integer;
    FTransitionSpeed:     integer;
    FTransitionSteps:     integer;
    FScript:              string;
    Timer:                TTimer;
    RESTThread:           TRESTThread;
    FPlaying:             boolean;
    LastTransition:       TDateTime;
    LastRESTRequest:      TDateTime;
    LstPages:             TList;
    TransitionActive:     boolean;
    TransitionStage:      integer;
    TransitionStepMSecs:  integer;
    TransitionStageSteps: integer;
    TransitionCurrPage:   integer;
    TransitionNextPage:   integer;
    TransitionStep:       integer;
    TransitionRow:        integer;
    TransitionRowChange:  boolean;
    TransitionTimer:      TTimer;
    FErrorMsg:            string;
    {$IFDEF ANDROID}
    FBaseStream:          TMemoryStream;
    {$ENDIF}

    //Properties setters and getters
    procedure SetRowCount (Value: integer);
    procedure SetColCount (Value: integer);
    procedure SetMaxGridCards (Value: integer);
    procedure SetCurrentPage (Value: integer);
    procedure SetHorizInnerMargin (Value: Single);
    procedure SetVertInnerMargin (Value: Single);
    procedure SetCardVisible (Show: boolean);
    procedure SetAutoAdjust (Value: boolean);
    function GetTopMargin: Single;
    procedure SetTopMargin (Value: Single);
    function GetLeftMargin: Single;
    procedure SetLeftMargin (Value: Single);
    function GetRightMargin: Single;
    procedure SetRightMargin (Value: Single);
    function GetBottomMargin: Single;
    procedure SetBottomMargin (Value: Single);
    procedure SetTransitionType (Value: TTransitionType);
    procedure SetTransitionInterval (Value: integer);
    procedure SetTransitionSpeed (Value: integer);
    procedure SetTransitionSteps (Value: integer);
    procedure SetRESTServerAddress (Value: string);
    procedure SetRESTRequest (Value: string);
    procedure SetRESTInterval (Value: integer);
    procedure SetJSONFile (Value: string);
    procedure SetJSONSourceType (Value: TJSONSourceType);
    procedure SetJSONSource (Value: string);
    function GetInternalJSONSource: string;
    procedure SetInternalJSONSource (Value: string);
    procedure SetErrorMsg (Value: string);
    procedure UpdateJSON;
    procedure AnalyzeJSON (JSON: string; var RootItem: string; var DataDepth: integer);

    //Display routines
    procedure ClearCards;
    procedure ClearCardPages;
    procedure CalculateGridDimensions (var ARowCount, AColCount: integer);
    procedure CalculatePages;
    procedure PlacePageCards (PageNumber: integer);
    procedure FadeInOut (PageNumber: integer; FirstTime: boolean = False);
    procedure ZoomInOut (PageNumber: integer; FirstTime: boolean = False);
    procedure RowByRow (PageNumber: integer; FirstTime: boolean = False);
    procedure MoveToPage (NewPage: integer);
    procedure TimerTimer (Sender: TObject);
    procedure BaseCardNotFound (Sender: TObject; FileName: string);
    procedure CreateThread;

    procedure RestoreAllPageProperties (APosition, AScale, AOpacity: boolean);
    procedure SetVisiblePage (PageNumber: integer);
    procedure TimerTransitionTimer (Sender: TObject);
    procedure FadeInOutStep;
    procedure ZoomInOutStep;
    procedure RowByRowStep;

    //Scripter routines
    procedure BeforeDisplay (Card: TJSONCard);
    procedure BeforeObjectDisplay (Card: TJSONCard; Sender: TControl; EventHandler: string);
    procedure DisplayJSONLabel (Card: TJSONCard; JSONLabelName, JSONField: string; var Text: string);
  protected
    //Event handlers
    procedure Resize; override;
    procedure RecalculateColRows; virtual;
    property InternalJSONSource: string read GetInternalJSONSource write SetInternalJSONSource;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;

    //File and stream routines
    procedure LoadTemplate (FileName: string); overload;
    procedure LoadTemplate (Stream: TStream); overload;

    //Scripter routines
    procedure RegisterControlsAndJSONFields (Card: TJSONCard; UserCodes: TatScript);

    //Display routines
    procedure Update (CardChanges: boolean = True; JSONOrPasChanges: boolean = True);
    procedure Start;
    procedure Stop;
    procedure Step (Up: boolean);
    property JSONDataSet: TJSONDataSet read FJSONDataSet;
    property PageCount: integer read FPageCount;
    property CurrentPage: integer read FCurrentPage write SetCurrentPage;
    property Playing: boolean read FPlaying;
    property ErrorMsg: string read FErrorMsg write SetErrorMsg;
  published
    property OnRecalculeColRows: TNotifyEvent read FOnRecalculeColRows write FOnRecalculeColRows;
    property OnChangePage: TNotifyEvent read FOnChangePage write FOnChangePage;
    property OnAnalyzeJSON: TAnalyzeJSONEvent read FOnAnalyzeJSON write FOnAnalyzeJSON;
    property OnGridError: TGridErrorMessage read FOnGridError write FOnGridError;
    property RowCount: integer read FRowCount write SetRowCount default 2;
    property ColCount: integer read FColCount write SetColCount default 2;
    property MaxGridCards: integer read FMaxGridCards write SetMaxGridCards;
    property GridMargin: TBounds read FGridMargin;
    property GridTopMargin: Single read GetTopMargin write SetTopMargin;
    property GridLeftMargin: Single read GetLeftMargin write SetLeftMargin;
    property GridRightMargin: Single read GetRightMargin write SetRightMargin;
    property GridBottomMargin: Single read GetBottomMargin write SetBottomMargin;
    property TransitionType: TTransitionType read FTransitionType write SetTransitionType;
    property TransitionInterval: integer read FTransitionInterval write SetTransitionInterval;
    property TransitionSpeed: integer read FTransitionSpeed write SetTransitionSpeed;
    property TransitionSteps: integer read FTransitionSteps write  SetTransitionSteps;
    property RESTServerAddress: string read FRESTServerAddress write SetRESTServerAddress;
    property RESTRequest: string read FRESTRequest write SetRESTRequest;
    property RESTInterval: integer read FRESTInterval write SetRESTInterval;
    property JSONFileName: string read FJSONFileName write SetJSONFile;
    property JSONSourceType: TJSONSourceType read FJSONSourceType write SetJSONSourceType;
    property JSONSource: string read FJSONSource write SetJSONSource;
    property HorizInnerMargin: Single read FHorizInnerMargin write SetHorizInnerMargin;
    property VertInnerMargin: Single read FVertInnerMargin write SetVertInnerMargin;
    property AutoAdjust: boolean read FAutoAdjust write SetAutoAdjust;
    property Script: string read FScript write FScript;
  end;

  TProyectSettings = class (TComponent)
  private
    FFileName:           string;
    FCardFileName:       string;
    FScriptFileName:     string;
    FChanged:            boolean;
    FGrid:               TGridCard;
    FRowCount:           integer;
    FColCount:           integer;
    FMaxGridCards:       integer;
    FPlayerMonitorNum:   integer;
    FAutoAdjust:         boolean;
    FAutoAdjustCardSize: boolean;
    FGridMargin:         TBounds;
    FHorizInnerMargin:   Single;
    FVertInnerMargin:    Single;
    FRESTServerAddress:  string;
    FRESTRequest:        string;
    FRESTInterval:       integer;
    FJSONFileName:       string;
    FJSONSourceType:     TJSONSourceType;
    FJSONSource:         string;
    FTransitionType:     TTransitionType;
    FTransitionInterval: integer;
    FTransitionSpeed:    integer;
    FTransitionSteps:    integer;
    FDeviceID:           string;
    FBackgroundType:     TBackgroundType;
    FBackgroundColor:    TAlphaColor;
    FBackgroundFileName: string;
    FSpread:             boolean;
    FOnChange:           TNotifyEvent;
    FOnChangeFileName:   TNotifyEvent;
    FOnBackgroundChange: TNotifyEvent;
    procedure SetFileName (Value: string);
    procedure SetCardFileName (Value: string);
    procedure SetScriptFileName (Value: string);
    procedure SetGrid (Value: TGridCard);
    procedure SetChanged (Value: boolean);
    procedure SetRowCount (Value: integer);
    procedure SetColCount (Value: integer);
    procedure SetMaxGridCards (Value: integer);
    procedure SetAutoAdjust (Value: boolean);
    function GetTopMargin: Single;
    procedure SetTopMargin (Value: Single);
    function GetLeftMargin: Single;
    procedure SetLeftMargin (Value: Single);
    function GetRightMargin: Single;
    procedure SetRightMargin (Value: Single);
    function GetBottomMargin: Single;
    procedure SetBottomMargin (Value: Single);
    procedure SetHorizInnerMargin (Value: Single);
    procedure SetVertInnerMargin (Value: Single);
    procedure SetRESTServerAddress (Value: string);
    procedure SetRESTRequest (Value: string);
    procedure SetRESTInterval (Value: integer);
    procedure SetJSONFile (Value: string);
    procedure SetJSONSourceType (Value: TJSONSourceType);
    procedure SetJSONSource (Value: string);
    procedure SetTransitionType (Value: TTransitionType);
    procedure SetTransitionInterval (Value: integer);
    procedure SetTransitionSpeed (Value: integer);
    procedure SetTransitionSteps (Value: integer);
    procedure SetDeviceID (Value: string);
    procedure SetBackgroundType (Value: TBackgroundType);
    procedure SetBackgroundColor (Value: TAlphaColor);
    procedure SetBackgroundFileName (Value: string);
    procedure SetSpread (Value: boolean);
    procedure DoChange;
    procedure DoBackgroundChange;
    procedure Initialize;
    procedure SetAutoAdjustCardSize(const Value: boolean);
    procedure SetPlayerMonitorNum(const Value: integer);
  protected
    procedure Notification (AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    procedure ApplyGridSettings;
    procedure LoadFromIniFile (AFileName: string);
    procedure SaveToIniFile (AFileName: string = '');

    property Spread: boolean read FSpread write SetSpread;
    property FileName: string read FFileName write SetFileName;
    property CardFileName: string read FCardFileName write SetCardFileName;
    property ScriptFileName: string read FScriptFileName write SetScriptFileName;
    property Grid: TGridCard read FGrid write SetGrid;
    property Changed: boolean read FChanged write SetChanged;
    property RowCount: integer read FRowCount write SetRowCount;
    property ColCount: integer read FColCount write SetColCount;
    property MaxGridCards: integer read FMaxGridCards write SetMaxGridCards;
    property AutoAdjust: boolean read FAutoAdjust write SetAutoAdjust;
    property AutoAdjustCardSize: boolean read FAutoAdjustCardSize write SetAutoAdjustCardSize;
    property PlayerMonitorNum: integer read FPlayerMonitorNum write SetPlayerMonitorNum;
    property TopMargin: Single read GetTopMargin write SetTopMargin;
    property LeftMargin: Single read GetLeftMargin write SetLeftMargin;
    property RightMargin: Single read GetRightMargin write SetRightMargin;
    property BottomMargin: Single read GetBottomMargin write SetBottomMargin;
    property HorizInnerMargin: Single read FHorizInnerMargin write SetHorizInnerMargin;
    property VertInnerMargin: Single read FVertInnerMargin write SetVertInnerMargin;
    property RESTServerAddress: string read FRESTServerAddress write SetRESTServerAddress;
    property RESTRequest: string read FRESTRequest write SetRESTRequest;
    property RESTInterval: integer read FRESTInterval write SetRESTInterval;
    property JSONFileName: string read FJSONFileName write SetJSONFile;
    property JSONSourceType: TJSONSourceType read FJSONSourceType write SetJSONSourceType;
    property JSONSource: string read FJSONSource write SetJSONSource;
    property TransitionType: TTransitionType read FTransitionType write SetTransitionType;
    property TransitionInterval: integer read FTransitionInterval write SetTransitionInterval;
    property TransitionSpeed: integer read FTransitionSpeed write SetTransitionSpeed;
    property TransitionSteps: integer read FTransitionSteps write SetTransitionSteps;
    property DeviceID: string read FDeviceID write SetDeviceID;
    property BackgroundType: TBackgroundType read FBackgroundType write SetBackgroundType;
    property BackgroundColor: TAlphaColor read FBackgroundColor write SetBackgroundColor;
    property BackgroundFileName: string read FBackgroundFileName write SetBackgroundFileName;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnChangeFileName: TNotifyEvent read FOnChangeFileName write FOnChangeFileName;
    property OnBackgroundChange: TNotifyEvent read FOnBackgroundChange write FOnBackgroundChange;
  end;

procedure AnalyzeJSON (JSON: string; var RootItem: string; var DataDepth: integer);

implementation
uses System.JSON, REST.JSON, REST.Client, System.TypInfo, Math, zzZOrderControlsFmxU;

type
  TCharSet = set of char;

const
  coSignature = 'CardTemplate';
  WideChars: TCharSet = ['�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                         '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                         '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                         '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'];

type
  TCharBuffer = array [0..(1024 * 16) - 1] of char;
  TByteBuffer = array [0..(1024 * 16) - 1] of Byte;

function InitCap (s: string): string;
var
  i: integer;
  IsStr, CommEOL, CommML: boolean;
begin
  result  := s;
  IsStr   := False;
  CommEOL := False;
  CommML  := False;

  for i := 1 to Length (s) do
  begin
    if not IsStr then
      IsStr := not IsStr and (s [i] = '''')
    else if s [i] = '''' then
      IsStr := False;

    if not IsStr and ((s [i] in ['A'..'Z', 'a'..'z']) or (s [i] in WideChars)) then
    begin
      if (i = 1) or (not (s [i - 1] in ['A'..'Z', 'a'..'z']) and not (s [i] in WideChars)) then
        result [i] := UpCase (s [i])
      else if s [i] in ['A'..'Z'] then
        result [i] := Chr (Ord (s [i]) + 32);
    end;

    if      not CommEOL and not CommML and (i > 0) and (s [i - 1] = '-') and (s [i] = '-') then
      CommEOL := True
    else if not CommEOL and not CommML and (i > 0) and (s [i - 1] = '/') and (s [i] = '*') then
      CommML := True
    else if CommML and (i > 0) and (s [i - 1] = '*') and (s [i] = '/') then
    begin
      CommEOL := False;
      CommML  := False;
      IsStr   := False;
    end
    else if CommEOL and (i > 0) and (s [i] in [#10, #13]) then
    begin
      CommEOL := False;
      CommML  := False;
      IsStr   := False;
    end;
  end;
end;

{$region 'TRESTJSONServiceClient'}
constructor TRESTJSONServiceClient.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FBaseURL        := '';
  FServiceRequest := '';
  FErrorMsg       := '';
end;

destructor TRESTJSONServiceClient.Destroy;
begin
  FErrorMsg       := '';
  FBaseURL        := '';
  FServiceRequest := '';

  inherited Destroy;
end;

function TRESTJSONServiceClient.Execute: string;
var
  RESTResponse: TRESTResponse;
  RESTClient:   TRESTClient;
  RESTRequest:  TRESTRequest;
begin
  result       := '';
  FErrorMsg    := '';
  RESTResponse := TRESTResponse.Create (Self);
  RESTClient   := TRESTClient.Create (Self);
  RESTRequest  := TRESTRequest.Create (Self);
  try
    RESTResponse.ContentType       := 'application/json';

    RESTClient.Accept              := 'application/json, text/plain; q=0.9, text/html;q=0.8,';
    RESTClient.AcceptCharset       := 'utf-8, *;q=0.8';
    RESTClient.BaseURL             := FBaseURL + FServiceRequest;
    RESTClient.RaiseExceptionOn500 := False;

    RESTRequest.Client             := RESTClient;
    RESTRequest.Response           := RESTResponse;
    RESTRequest.SynchronizedEvents := False;

    try
      RESTRequest.Execute;
      result := RESTResponse.Content;
    except
      FErrorMsg := 'Can''t connect to server. Please check your internet connection.';
    end;
  finally
    RESTResponse.Free;
    RESTClient.Free;
    RESTRequest.Free;
  end;
end;

procedure AnalyzeJSON (JSON: string; var RootItem: string; var DataDepth: integer);
var
  Value:   TJSONValue;
  Obj:     TJSONObject;
  i:       integer;
  IsArray: boolean;
begin
  RootItem   := '';
  DataDepth  := 1;
  Value      := System.JSON.TJSONObject.ParseJSONValue(JSON);
  if Assigned (Value) then
  begin
    Obj := Value as TJSONObject;
    for i := 0 to Obj.Count - 1 do
      if Obj.Pairs [i].JsonValue Is TJSONArray then
      begin
        RootItem  :=  Obj.Pairs [i].JsonString.ToString.Replace('"', '');
        DataDepth := 2;
        exit;
      end;
  end;
end;
{$endregion}

{$region 'TAdapterJSONValue'}
type
  TAdapterJSONValue =  class(TInterfacedObject, IRESTResponseJSON)
  private
    FJSONValue: TJSONValue;
  protected
    { IRESTResponseJSON }
    procedure AddJSONChangedEvent(const ANotify: TNotifyEvent);
    procedure RemoveJSONChangedEvent(const ANotify: TNotifyEvent);
    procedure GetJSONResponse(out AJSONValue: TJSONValue; out AHasOwner: Boolean);
    function HasJSONResponse: Boolean;
    function HasResponseContent: Boolean;
  public
    constructor Create(const AJSONValue: TJSONValue);
    destructor Destroy; override;
  end;

procedure TAdapterJSONValue.AddJSONChangedEvent(const ANotify: TNotifyEvent);
begin
  // Not implemented because we pass JSON in constructor and do not change it
end;

constructor TAdapterJSONValue.Create(const AJSONValue: TJSONValue);
begin
  FJSONValue := AJSONValue;
end;

destructor TAdapterJSONValue.Destroy;
begin
  // We own the JSONValue, so free it.
  FJSONValue.Free;
  inherited;
end;

procedure TAdapterJSONValue.GetJSONResponse(out AJSONValue: TJSONValue;
  out AHasOwner: Boolean);
begin
  AJSONValue := FJSONValue;
  AHasOwner := True; // We own this object
end;

function TAdapterJSONValue.HasJSONResponse: Boolean;
begin
  Result := FJSONValue <> nil;
end;

function TAdapterJSONValue.HasResponseContent: Boolean;
begin
  Result := FJSONValue <> nil;
end;

procedure TAdapterJSONValue.RemoveJSONChangedEvent(const ANotify: TNotifyEvent);
begin
  // Not implemented because we pass JSON in constructor and do not change it
end;
{$endregion}

{$region 'TJSONDataSet'}
constructor TJSONDataSet.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FJSONSource                        := '';
  FRootPath                          := '';
  FScanDepth                         := 2;
  RESTResponseDataSetAdapter         := TRESTResponseDataSetAdapter.Create (Self);
  FMemTable                          := TFDMemTable.Create (Self);
  RESTResponseDataSetAdapter.Dataset := FMemTable;
  FRecordCount                       := 0;
  FUpdating                          := 0;
end;

destructor TJSONDataSet.Destroy;
begin
  RESTResponseDataSetAdapter.ResponseJSON := nil;
  RESTResponseDataSetAdapter.Free;
  FMemTable.Free;

  inherited Destroy;
end;

procedure TJSONDataSet.SetJSONSource (Value: string);
begin
  if FJSONSource <> Value then
  begin
    FJSONSource := Value;
    UpdateTable;
  end;
end;

procedure TJSONDataSet.SetRootPath (Value: string);
begin
  if FRootPath <> Value then
  begin
    FRootPath := Value;
    UpdateTable;
  end;
end;

procedure TJSONDataSet.SetScanDepth (Value: integer);
begin
  if FScanDepth <> Value then
  begin
    FScanDepth := Value;
    UpdateTable;
  end;
end;

procedure TJSONDataSet.UpdateTable;
var
  LJSON: TJSONValue;
  LIntf: IRESTResponseJSON;
begin
  if FUpdating > 0 then exit;

  FRecordCount := 0;

  // Clear last value
  RESTResponseDataSetAdapter.ResponseJSON := nil;

  // Set options for how the JSON value will be interpreted.
  // Optional.
  if FScanDepth > 1 then
  begin
    // Traverse into nested json objects when scanning for fields
    RESTResponseDataSetAdapter.NestedElementsDepth := FScanDepth;
    RESTResponseDataSetAdapter.NestedElements      := True;
  end
  else
    RESTResponseDataSetAdapter.NestedElements := False;

  // Root element is used to access a nested JSON array.
  // For example set RootElement to 'x' to work with this JSON: { "x": [{"attr1": "row1"}, {"attr1": "row2"}]}
  RESTResponseDataSetAdapter.RootElement := FRootPath;

  if Trim (FJSONSource) <> '' then
  begin
    // Parse the JSON in our memo
    LJSON := TJSONObject.ParseJSONValue(FJSONSource);
    if LJSON = nil then
      raise Exception.Create('Invalid JSON');

    // Provide the JSON value to the adapter
    LIntf                                   := TAdapterJSONValue.Create(LJSON);
    RESTResponseDataSetAdapter.ResponseJSON := LIntf;
    RESTResponseDataSetAdapter.Active       := True;
    FRecordCount                            := FMemTable.RecordCount;
  end;
end;

procedure TJSONDataSet.BeginUpdate;
begin
  Inc (FUpdating);
end;

procedure TJSONDataSet.EndUpdate;
begin
  FUpdating := Max (0, FUpdating - 1);

  if FUpdating = 0 then
    UpdateTable;
end;
{$endregion}

{$region 'TJSONFieldLink'}
constructor TJSONFieldLink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FJSONField   := '';
  FJSONDataSet := nil;
end;

destructor TJSONFieldLink.Destroy;
begin
  FJSONField   := '';
  FJSONDataSet := nil;

  inherited Destroy;
end;

procedure TJSONFieldLink.SetJSONField (Value: string);
begin
  if FJSONField <> Value then
  begin
    FJSONField := Value;
  end;
end;

procedure TJSONFieldLink.SetJSONDataSet (Value: TJSONDataSet);
begin
  if FJSONDataSet <> Value then
  begin
    FJSONDataSet := Value;
  end;
end;

procedure TJSONFieldLink.Notification (AComponent: TComponent; Operation: TOperation);
begin
  if (AComponent = FJSONDataSet) and (Operation = opRemove) then
    FJSONDataSet := nil;
end;

function TJSONFieldLink.GetFieldValue: string;
begin
  result := '';

  if (FJSONField <> '') and Assigned (FJSONDataSet) and (FJSONDataSet.MemTable.Active) and
     (FJSONDataSet.MemTable.FieldList.Find(FJSONField) <> nil) then
    result := FJSONDataSet.MemTable.FieldByName(FJSONField).AsString;
end;
{$endregion}

{$region 'TLabelFormat'}
constructor TLabelFormat.Create (AJSONLabel: TJSONLabel);
begin
  FJSONLabel      := AJSONLabel;
  FStringCase     := scNormal;
  FLabelBehavior  := [];
  FStringFormat   := '';
end;

destructor TLabelFormat.Destroy;
begin
  FStringFormat   := '';

  inherited Destroy;
end;

function TLabelFormat.ApplyFormat (s: string): string;
begin
  result := s;

  case FStringCase of
    scUpperCase: result := result.ToUpper;
    scLowerCase: result := result.ToLower;
    scNameCase:  result := InitCap (result);
  end;

  if lbLeftTrim in FLabelBehavior then
    result := result.TrimLeft;

  if lbRightTrim in FLabelBehavior then
    result := result.TrimRight;

  if not FStringFormat.IsEmpty then
    result := System.MaskUtils.FormatMaskText(FStringFormat, result);
end;
{$endregion}

{$region 'TJSONLabel'}
constructor TJSONLabel.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FJSONLink              := TJSONFieldLink.Create (Self);
  FLabelFormat           := TLabelFormat.Create (Self);
  FOnDisplayJSONLabel    := '';
  FOnBeforeObjectDisplay := '';
end;

destructor TJSONLabel.Destroy;
begin
  FOnDisplayJSONLabel    := '';
  FOnBeforeObjectDisplay := '';
  FJSONLink.Free;
  FLabelFormat.Free;

  inherited Destroy;
end;
{$endregion}

{$region 'TJSONImage'}
constructor TJSONImage.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FJSONLink              := TJSONFieldLink.Create (Self);
  FIsBitmapEmpty         := True;
  FOnBeforeObjectDisplay := '';
end;

destructor TJSONImage.Destroy;
begin
  FOnBeforeObjectDisplay := '';
  FJSONLink.Free;

  inherited Destroy;
end;
{$endregion}

{$region 'TCardLine'}
constructor TCardLine.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardLine.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TCardRectangle'}
constructor TCardRectangle.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardRectangle.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TCardRoundRect'}
constructor TCardRoundRect.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardRoundRect.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TCardEllipse'}
constructor TCardEllipse.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardEllipse.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TCardCircle'}
constructor TCardCircle.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardCircle.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TCardArc'}
constructor TCardArc.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FOnBeforeObjectDisplay := '';
end;

destructor TCardArc.Destroy;
begin
  FOnBeforeObjectDisplay := '';

  inherited Destroy;
end;
{$endregion}


{$region 'TSelectionCtrl'}
constructor TSelectionCtrl.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FControl := nil;
end;

procedure TSelectionCtrl.SetControl (Value: TControl);
begin
  if FControl <> Value then
  begin
    FControl := Value;
    UpdatePosition;
  end;
end;

procedure TSelectionCtrl.UpdatePosition;
begin
  Visible := False;
  if not Assigned (FControl) then exit;

  Parent     := FControl.Parent;
  Position.X := FControl.Position.X;
  Position.Y := FControl.Position.Y;
  Width      := FControl.Width;
  Height     := FControl.Height;
  BringToFront;
  Visible := True;
end;

procedure TSelectionCtrl.AdjustCtrlToSelection;
begin
  FControl.Position.X := Position.X;
  FControl.Position.Y := Position.Y;
  FControl.Width      := Width;
  FControl.Height     := Height;
end;
{$endregion}

{$region 'TTreeViewStructureItem'}
constructor TTreeViewStructureItem.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  Ctrl := nil;
end;

destructor TTreeViewStructureItem.Destroy;
begin
  Ctrl := nil;

  inherited Destroy;
end;
{$endregion}

{$region 'TBackgroundImage'}
constructor TBackgroundImage.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FBackgroundType    := btColor;
  FBackgroundColor  := TAlphaColors.Null;
  FBackgroundBitmap := nil;
end;

destructor TBackgroundImage.Destroy;
begin
  if Assigned (FBackgroundBitmap) then
    FreeAndNil (FBackgroundBitmap);

  inherited Destroy;
end;

procedure TBackgroundImage.SetBackgroundType (Value: TBackgroundType);
begin
  if FBackgroundType <> Value then
  begin
    FBackgroundType := Value;
    PaintBackground;
    ChangeBackground;
  end;
end;

procedure TBackgroundImage.SetBackgroundColor (Value: TAlphaColor);
begin
  if FBackgroundColor <> Value then
  begin
    FBackgroundColor := Value;
    PaintBackground;
    ChangeBackground;
  end;
end;

procedure TBackgroundImage.SetBackgroundBitmap (Value: TBitmap);
begin
  if FBackgroundBitmap <> Value then
  begin
    FBackgroundBitmap := Value;
    PaintBackground;
    ChangeBackground;
  end;
end;

procedure TBackgroundImage.Resize;
begin
  inherited Resize;

  if Self = nil then exit;
  PaintBackground;
end;

procedure TBackgroundImage.ChangeBackground;
begin
  if Assigned (FOnChangeBackground) then
    FOnChangeBackground (Self);
end;


procedure TBackgroundImage.LoadBackgroundBitmap (FileName: string);
begin
  if not Assigned (FBackgroundBitmap) then
    FBackgroundBitmap := TBitmap.Create;

  FBackgroundBitmap.LoadFromFile(FileName);
  PaintBackground;
  ChangeBackground;
end;

procedure TBackgroundImage.ClearBackgroundBitmap;
begin
  if not Assigned (FBackgroundBitmap) then exit;

  FreeAndNil (FBackgroundBitmap);
  PaintBackground;
  ChangeBackground;
end;

procedure TBackgroundImage.PaintBackground;
var
  Bmp: TBitmap;
begin
  if not Assigned (Bitmap) then
  begin
    Bmp := TBitmap.Create;
    Bitmap.Assign(Bmp);
  end;

  Bitmap.SetSize (Round (Width), Round (Height));
  if (FBackgroundType = btImage) and Assigned (FBackgroundBitmap) then
    Bitmap.Assign(FBackgroundBitmap)
  else
    Bitmap.Clear(FBackgroundColor);
end;
{$endregion}

{$region 'TJSONCard'}
constructor TJSONCard.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FDesignMode                 := False;
  FLoading                    := False;
  FChanged                    := False;
  FDestroying                 := False;
  FCurrentTool                := ptDeselect;
  FBackground                 := TBackgroundImage.Create (Self);
  FBackground.Parent          := Self;
  FBackground.Align           := TAlignLayout.Client;
  FBackground.BackgroundType  := btColor;
  FBackground.BackgroundColor := TAlphaColors.Lightgray;
  FBackground.OnMouseDown     := BackgroundMouseDown;
  FBackground.OnMouseMove     := BackgroundMouseMove;
  FBackground.OnMouseUp       := BackgroundMouseUp;
  FBackground.ClipChildren    := True;
  FJSONDataSet                := TJSONDataSet.Create (Self);
  FExcludedProperties         := TStringList.Create;
  FSelectedCtrls              := TList.Create;
  FLstControls                := TList.Create;
  FGarbageCtrls               := TList.Create;
  FOnChangeCtrlSelection      := nil;
  FOnChangeCurrentTool        := nil;
  FOnClearLstControls         := nil;
  FOnChange                   := nil;
  FOnControlChangeName        := nil;
  FOnGetStructureImageIndex   := nil;
  FUpdatingSelection          := True;
  TimerPurgeGarbage           := TTimer.Create(Self);
  TimerPurgeGarbage.Enabled   := False;
  TimerPurgeGarbage.Interval  := 250;
  TimerPurgeGarbage.OnTimer   := TimerPurgeGarbageTimer;
  TimerChange                 := TTimer.Create(Self);
  TimerChange.Enabled         := False;
  TimerChange.Interval        := 250;
  TimerChange.OnTimer         := TimerChangeTimer;
  DragEnabled                 := False;
  Resizing                    := False;
  ResizingCtrl                := nil;
  MouseDownPos                := TPointF.Create(-1, -1);
  LastPosition                := TPointF.Create(-1, -1);
  FTreeStructure              := nil;
  FScripter                   := nil;
  FJSONFields                 := TList.Create;
  {$IFDEF ANDROID}
  CtrlNames                   := TStringList.Create;
  {$ENDIF}

  PopulateExcludeProperties;
end;

destructor TJSONCard.Destroy;
begin
  Clear;
  {$IFDEF ANDROID}
  CtrlNames.Free;
  {$ENDIF}
  ClearJSONFields;
  FJSONFields.Free;
  FreeAndNil (FScripter);
  FDestroying := True;
  if Assigned (FTreeStructure) then
    FTreeStructure.Clear;

  FTreeStructure            := nil;
  TimerChange.Enabled       := False;
  TimerPurgeGarbage.Enabled := False;

  PurgeGarbageCtrls;
  FreeAndNil (TimerPurgeGarbage);
  FreeAndNil (TimerChange);
  FreeAndNil (FExcludedProperties);
  FreeAndNil (FJSONDataSet);
  FreeAndNil (FSelectedCtrls);
  FreeAndNil (FLstControls);
  FreeAndNil (FGarbageCtrls);
  FreeAndNil (FBackground);

  inherited Destroy;
end;

procedure TJSONCard.Clear;
begin
  FJSONDataSet.JSONSource     := '';
  FDesignMode                 := False;
  FCurrentTool                := ptDeselect;
  FBackground.BackgroundType  := btColor;
  FBackground.BackgroundColor := TAlphaColors.Lightgray;
  FBackground.ClearBackgroundBitmap;
  ClearSelection;
  ClearLstControls;
end;

procedure TJSONCard.Notification (AComponent: TComponent; Operation: TOperation);
begin
  if (Operation = opRemove) and (AComponent = FTreeStructure) then
  begin
    FTreeStructure.Clear;
    FTreeStructure := nil;
  end;
end;


{$region 'Properties setters and getters'}
procedure TJSONCard.SetDesignMode (Value: boolean);
begin
  if FDesignMode <> Value then
  begin
    FDesignMode := Value;
    UpdateMode;
  end;
end;

procedure TJSONCard.SetCurrentTool (Value: TPaletteTool);
begin
  if FCurrentTool <> Value then
  begin
    FCurrentTool := Value;
    ChangeCurrentTool;
  end;
end;

function TJSONCard.GetBackGroundColor: TAlphaColor;
begin
  result := FBackground.BackgroundColor;
end;

procedure TJSONCard.SetBackgroundColor (Value: TAlphaColor);
begin
  if Value <> FBackground.BackgroundColor then
  begin
    FBackground.BackgroundColor := Value;
    Changed                     := True;
  end;
end;

procedure TJSONCard.SetChanged (Value: boolean);
begin
  if FChanged <> Value then
    FChanged := Value;

  if FChanged then
    TimerChange.Enabled := True;
end;

procedure TJSONCard.TimerChangeTimer (Sender: TObject);
begin
  TimerChange.Enabled := False;
  Change;
end;

procedure TJSONCard.SetCardWidth (Value: Single);
begin
  if Width <> Value then
  begin
    Width   := Value;
    Changed := True;
  end;
end;

function TJSONCard.GetCardWidth: Single;
begin
  result := Width;
end;

procedure TJSONCard.SetCardHeight (Value: Single);
begin
  if Height <> Value then
  begin
    Height  := Value;
    Changed := True;
  end;
end;

function TJSONCard.GetCardHeight: Single;
begin
  result := Height;
end;

procedure TJSONCard.SetCardOpacity (Value: Single);
begin
  if Opacity <> Value then
  begin
    Opacity := Value;
    Changed := True;
  end;
end;

function TJSONCard.GetCardOpacity: Single;
begin
  result := Opacity;
end;

function TJSONCard.GetBackgroundType: TBackgroundType;
begin
  result := FBackground.BackgroundType;
end;

procedure TJSONCard.SetBackgroundType (Value: TBackgroundType);
begin
  FBackground.BackgroundType := Value;
  Changed                    := True;
end;

procedure TJSONCard.SetTreeStructure (Value: TTreeView);
begin
  if FTreeStructure <> Value then
  begin
    if Assigned (FTreeStructure) then
    begin
      FTreeStructure.OnMouseUp := nil;
      FTreeStructure.Clear;
    end;

    FTreeStructure := Value;
    if Assigned (FTreeStructure) then
    begin
      FTreeStructure.MultiSelect := True;
      FTreeStructure.OnMouseUp   := TreeStructureMouseUp;
      BuildTreeStructure;
    end;
  end;
end;

procedure TJSONCard.UpdateMode;
begin
  if FDesignMode then
  begin
    if Assigned (FTreeStructure) then
      BuildTreeStructure;
  end
  else
  begin
    if Assigned (FTreeStructure) then
    FTreeStructure.Clear;
  end;
end;
{$endregion}

{$region 'Stream and File routines'}
procedure WriteStringToStream (Stream: TStream; S: string);
var
  i64: Int64;
begin
  i64 := ByteLength(s);
  Stream.Write(i64, SizeOf (i64));
  Stream.Write(S [1], i64);
end;

procedure ReadStringFromStream (Stream: TStream; var S: string);
var
  i64: Int64;
  Buffer: TCharBuffer;
begin
  FillChar (Buffer, SizeOf (Buffer), 0);
  Stream.Read(i64, SizeOf (i64));
  Stream.Read(Buffer, i64);
  s := String (Buffer);
end;

procedure TJSONCard.SaveToStream (Stream: TStream);
var
  ApStart, ApEnd, ApContent, ApTableIndex,
  ApTableData, i64Dummy, ApCtrlStart, ApCtrlEnd, ApTableImages, bckSize: Int64;
  VerMaj, VerMin, bDummy: byte;
  intDummy: integer;
  sDummy: Single;
  Color: TAlphaColor;
  i: integer;
  Ctrl: TControl;
  JSON, s: string;
  LstImages: TList;
  mem: TMemoryStream;
  Img: TJSONImage;
  Buffer: TByteBuffer;
  Ctrls: TList;

  function GetCurrentRelativePosition: Int64;
  begin
    result := Stream.Position - ApStart;
  end;

begin
  Ctrls    := TList.Create;
  i64Dummy := 0;
  CtrlsPostOrder (Ctrls);

  //Get real [FileStart]
  ApStart := Stream.Position;

  //Save signature, version major and minor
  VerMaj    := 1;
  VerMin    := 0;
  FillChar (Buffer, SizeOf (Buffer), 0);
  {$IFDEF ANDROID}
  for i := 0 to Length (coSignature) - 1 do
    Buffer [i] := Ord (coSignature [i]);
  {$ELSE}
  for i := 1 to Length (coSignature) do
    Buffer [i - 1] := Ord (coSignature [i]);
  {$ENDIF}

  Stream.Write (Buffer, Length (coSignature));
  Stream.Write(VerMaj, SizeOf (VerMaj));
  Stream.Write(VerMin, SizeOf (VerMin));

  //Save [FileStart] position 0 as relative position
  Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Get position to save [FileEnd]
  ApContent := GetCurrentRelativePosition;

  //Save an empty space for [FileEnd]
  Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Save an empty space for [TableIndex]
  Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Save an empty space for [TableData]
  Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Save an empty space for [ApTableImages]
  Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Write card info
  sDummy := Width;                       Stream.Write(sDummy, SizeOf (sDummy));
  sDummy := Height;                      Stream.Write(sDummy, SizeOf (sDummy));
  sDummy := Opacity;                     Stream.Write(sDummy, SizeOf (sDummy));
  Color  := FBackground.BackgroundColor; Stream.Write(Color,  SizeOf (Color));

  //Write Background type
  bDummy := Ord (FBackground.BackgroundType);
  Stream.Write(bDummy, SizeOf (bDummy));

  //Write image background
  if not Assigned (FBackground.BackgroundBitmap) then
  begin
    bckSize := 0;
    Stream.Write(bckSize, SizeOf (bckSize));
  end
  else
  begin
    mem := TMemoryStream.Create;
    try
      FBackground.BackgroundBitmap.SaveToStream(mem);
      mem.Position := 0;
      bckSize      := mem.Size;
      Stream.Write(bckSize, SizeOf (bckSize));
      Stream.CopyFrom(mem, mem.Size);
    finally
      mem.Free;
    end;
  end;

  //Save control count
  i64Dummy := LstControls.Count;    Stream.Write(i64Dummy, SizeOf (i64Dummy));

  //Get position for [TableIndex]
  ApTableIndex := GetCurrentRelativePosition;

  //Reserve space for pointers to controls
  i64Dummy := 0;
  for i := 0 to LstControls.Count - 1 do
  begin
    //Empty space for start position
    Stream.Write(i64Dummy, SizeOf (i64Dummy));

    //Empty space for end position
    Stream.Write(i64Dummy, SizeOf (i64Dummy));
  end;

  //Get position for [TableData]
  ApTableData := GetCurrentRelativePosition;

  //Write control information
  for i := 0 to Ctrls.Count - 1 do
  begin
    //Get Start position
    ApCtrlStart := GetCurrentRelativePosition;

    //Get Control from list
    Ctrl := TControl (Ctrls [i]);

    //Save Control Type
    bDummy := Ord (GetControlType (Ctrl));
    Stream.Write(bDummy, SizeOf (bDummy));

    //Save ZOrder
    intDummy := zzGetControlZOrder (Ctrl);
    Stream.Write(intDummy, SizeOf (intDummy));

    //Save Parent Control Name
    s := '';
    if Assigned (Ctrl.Parent) and (Ctrl.Parent <> FBackground) then
      s := Ctrl.Parent.Name;
    WriteStringToStream(Stream, s);

    //Save Control Name
    WriteStringToStream(Stream, Ctrl.Name);

    //Save JSON
    JSON := CtrlToJSON(Ctrl);
    WriteStringToStream(Stream, JSON);

    //Get End position
    ApCtrlEnd := GetCurrentRelativePosition;

    //Goto [TableIndex] + Offset to write Start and End position
    Stream.Position := ApTableIndex + (i * SizeOf (i64Dummy) * 2);
    Stream.Write(ApCtrlStart, SizeOf (ApCtrlStart));
    Stream.Write(ApCtrlEnd,   SizeOf (ApCtrlEnd));

    //Return to End position
    Stream.Position := ApCtrlEnd;
  end;

  //Get position for [TableImages]
  ApTableImages := GetCurrentRelativePosition;

  //Create a temporary list with the images that have a Bitmap
  LstImages := TList.Create;
  try
    for i := 0 to Ctrls.Count - 1 do
      if (TControl (Ctrls [i]) is TJSONImage) and not (TJSONImage (Ctrls [i])).IsBitmapEmpty then
        LstImages.Add(Ctrls [i]);

     //Save ImageCount
     intDummy := LstImages.Count;
     Stream.Write(intDummy, SizeOf (intDummy));

     //Save [TableImages]
     for i := 0 to LstImages.Count - 1 do
     begin
       Img := TJSONImage (LstImages [i]);
       mem := TMemoryStream.Create;
       Img.Bitmap.SaveToStream(mem);
       mem.Position := 0;

       //Write Image name, Bitmap stream size and Bitmap stream content
       i64Dummy := mem.Size;
       WriteStringToStream(Stream, Img.Name);
       Stream.Write(i64Dummy, SizeOf (i64Dummy));
       Stream.CopyFrom(mem, mem.Size);

       mem.Free;
     end;
  finally
    LstImages.Free;
  end;
  Ctrls.Free;

  //Get position for [FileEnd]
  ApEnd := GetCurrentRelativePosition;

  //Move to [Content Table] to write positions for [FileEnd], [TableIndex], [TableData] and [TableImages]
  Stream.Position := ApContent;
  Stream.Write(ApEnd,         SizeOf (ApEnd));
  Stream.Write(ApTableIndex,  SizeOf (ApTableIndex));
  Stream.Write(ApTableData,   SizeOf (ApTableData));
  Stream.Write(ApTableImages, SizeOf (ApTableImages));

  //Return to [FileEnd] position
  Stream.Position := ApEnd;
end;

procedure TJSONCard.LoadFromStream (Stream: TStream);
type
  PZOrder = ^RZOrder;
  RZOrder = record
    Ctrl:   TControl;
    ZOrder: integer;
  end;

var
  ApRealStart, ApStart, ApEnd, ApTableIndex, ApTableData,
  ControlCount, ApRealTableIndex,
  ApCtrlStart, ApCtrlEnd, ApTableImages, ImageSize: Int64;
  Buffer: TByteBuffer;
  VerMaj, VerMin: byte;
  sDummy: Single;
  intDummy, ImageCount: integer;
  Color: TAlphaColor;
  i: integer;
  bDummy: byte;
  Ctrl, ParentCtrl: TControl;
  ParentName, CtrlName, JSON, Sign: string;
  LstZOrder: TList;
  mem: TMemoryStream;
  LstLabels: TList;
  Lab: TJSONLabel;

  function NewZOrder (Ctrl: TControl; ZOrder: integer): PZOrder;
  begin
    New (result);
    result^.Ctrl   := Ctrl;+
    result^.ZOrder := ZOrder;
  end;

  procedure ClearZOrder;
  var
    Z: PZOrder;
    i: integer;
  begin
    for i := 0 to LstZOrder.Count - 1 do
    begin
      Z             := PZOrder (LstZOrder [i]);
      LstZOrder [i] := nil;
      Dispose (Z);
    end;
    LstZOrder.Clear;
  end;

  procedure RestoreZOrder;
  var
    i:        integer;
    _changed: boolean;
    Z:        PZOrder;
  begin
    repeat
      _changed := False;

      for i := 0 to LstZOrder.Count - 1 do
      begin
        Z := PZOrder (LstZOrder [i]);
        if Z^.ZOrder <> zzGetControlZOrder (Z^.Ctrl) then
        begin
          _changed := True;
          zzSetControlZOrder(Z^.Ctrl, Z^.ZOrder);
        end;
      end;
    until not _changed;
  end;

begin
  FLoading := True;

  //Get the real stream start position
  ApRealStart := Stream.Position;

  LstZOrder := TList.Create;
  LstLabels := TList.Create;
  BeginUpdate;
  try
    //Initialize all lists
    Clear;

    //Read file signature
    FillChar (Buffer, SizeOf (Buffer), 0);
    Stream.Read(Buffer, Length (coSignature));
    i    := 0;
    Sign := '';
    while Buffer [i] <> 0 do
    begin
      Sign := Sign + Chr (Buffer [i]);
      Inc (i);
    end;

    if Sign <> coSignature then
      raise Exception.Create('Invalid table stream');

    //Read Version Major and Minor
    Stream.Read(VerMaj, SizeOf (VerMaj));
    Stream.Read(VerMin, SizeOf (VerMin));

    if (VerMaj <> 1) or (VerMin <> 0) then
      raise Exception.Create('Invalid card version');

    //Read [FileStart]
    Stream.Read(ApStart,       SizeOf (ApStart));

    //Read [FileEnd]
    Stream.Read(ApEnd,         SizeOf (ApEnd));

    //Read [TableIndex]
    Stream.Read(ApTableIndex,  SizeOf (ApTableIndex));

    //Read [TableData]
    Stream.Read(ApTableData,   SizeOf (ApTableData));

    //Read [TableImages]
    Stream.Read(ApTableImages, SizeOf (ApTableImages));

    //Read card info
    Stream.Read(sDummy, SizeOf (sDummy));   Width   := sDummy;
    Stream.Read(sDummy, SizeOf (sDummy));   Height  := sDummy;
    Stream.Read(sDummy, SizeOf (sDummy));   Opacity := sDummy;
    Stream.Read(Color,  SizeOf (Color));
    FBackground.BackgroundColor := Color;

    //Read background type
    Stream.Read(bDummy, SizeOf (bDummy));
    FBackground.FBackgroundType := TBackgroundType (bDummy);

    //Read card image background
    Stream.Read(ImageSize, SizeOf (ImageSize));
    if ImageSize <> 0 then
    begin
      mem := TMemoryStream.Create;
      try
        //Read image from stream
        mem.CopyFrom(Stream, ImageSize);
        mem.Position := 0;

        if not Assigned (FBackground.FBackgroundBitmap) then
          FBackground.FBackgroundBitmap := TBitmap.Create;

        FBackground.FBackgroundBitmap.LoadFromStream(mem);
      finally
        mem.Free;
      end;
    end;

    //Read ControlCount
    Stream.Read(ControlCount, SizeOf (ControlCount));

    //Get real position for [TableIndex]
    ApRealTableIndex := Stream.Position;

    {$IFDEF ANDROID}
    CtrlNames.Clear;
    {$ENDIF}

    //Read [TableIndex] and Control Information
    for i := 0 to ControlCount - 1 do
    begin
      //Move to [TableIndex] + Offset position
      Stream.Position := ApRealTableIndex + (i * SizeOf (Int64) * 2);

      //Read [ApCtrlStart] & [ApCtrlEnd]
      Stream.Read(ApCtrlStart, SizeOf (ApCtrlStart));
      Stream.Read(ApCtrlEnd,   SizeOf (ApCtrlEnd));

      //Move to real control start position
      Stream.Position := ApRealStart + ApCtrlStart;

      //Read Control information
      //Read Control Type
      Stream.Read(bDummy, SizeOf (bDummy));

      //Read ZOrder
      Stream.Read(intDummy, SizeOf (intDummy));

      //Read Parent Name, Control Name and JSON
      ReadStringFromStream(Stream, ParentName);
      ReadStringFromStream(Stream, CtrlName);
      ReadStringFromStream(Stream, JSON);

      ParentCtrl := GetControlByName(ParentName);
      if not Assigned (ParentCtrl) then
        ParentCtrl := FBackground;

      Ctrl := CreateCtrl(TPaletteTool (bDummy), ParentCtrl, 0, 0, JSON);
      {$IFDEF ANDROID}
      CtrlNames.Add (CtrlName);
      {$ELSE}
      Ctrl.Name := CtrlName;
      ChangeControlName(Ctrl, CtrlName, CtrlName);
      {$ENDIF}
      LstZOrder.Add(NewZOrder(Ctrl, intDummy));

      {$IFDEF RTML}
      if Ctrl is TJSONLabel then
        LstLabels.Add(Ctrl);
      {$ENDIF}
    end;

    //Read [ImageCount]
    Stream.Read(ImageCount, SizeOf (ImageCount));

    //Read [TableImages]
    for i := 0 to ImageCount - 1 do
    begin
      //Read [CtrlName]
      ReadStringFromStream(Stream, CtrlName);

      //Read [ImageSize]
      Stream.Read(ImageSize, SizeOf (ImageSize));

      //Read [Bitmap stream]
      mem := TMemoryStream.Create;
      mem.CopyFrom(Stream, ImageSize);

      //Get control image
      Ctrl := GetControlByName(CtrlName);

      //Assign bitmap stream
      mem.Position := 0;
      TJSONImage (Ctrl).Bitmap.LoadFromStream(mem);
      TJSONImage (Ctrl).IsBitmapEmpty := False;

      mem.Free;
    end;

    {$IFDEF ANDROID}
    ClearControlNames;
    {$ENDIF}

    Stream.Position := ApRealStart + ApEnd;
  finally
//    RestoreZOrder;
    ClearZOrder;
    LstZOrder.Free;
    FBackground.PaintBackground;
    EndUpdate;

    {$IFDEF RTML}
    for i := 0 to LstLabels.Count - 1 do
    begin
{      Ctrl := TControl (LstLabels [i]);
      if Ctrl is TJSONLabel then
      begin
        TJSONLabel (Ctrl).RepaintLabel;
      end;}

      Lab := TJSONLabel (LstLabels [i]);
      Lab.AnalyzeCode;
//      Lab.RepaintLabel;
    end;
    {$ENDIF}

    LstLabels.Free;

    FLoading := False;
    Changed  := False;
  end;
end;

procedure TJSONCard.SaveToFile (FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(Stream);
    Changed := False;
  finally
    Stream.Free;
  end;
end;

procedure TJSONCard.LoadFromFile (FileName: string);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead);
  try
    LoadFromStream(Stream);
    BuildTreeStructure;
  finally
    Stream.Free;
  end;
end;

procedure TJSONCard.LoadBackgroundFile (FileName: string);
begin
  FBackground.LoadBackgroundBitmap(FileName);
  Changed := True;
end;

{$endregion}

{$region 'Grid routines'}
procedure TJSONCard.UpdateGrid;
{var
  X, Y:   integer;
  p:      TPointF;
  p1, p2: TPointF;
  Rect:   TRectF;}
begin
{  Card.FBackground.Canvas.BeginScene;
  Card.FBackground.Canvas.Stroke.Color := TAlphaColors.Black;
  Card.FBackground.Canvas.Fill.Color   := TAlphaColors.Black;
  p  := TPointF.Create(0, 0);
  p1 := TPointF.Create(0, 0);
  p2 := TPointF.Create(0, 0);
  Y  := FGridSize;
  while Card.Height > Y do
  begin
    X := FGridSize;
    while Card.Width > X do
    begin
      p.X := X;
      p.Y := Y;

      p1.X := X - 1;
      p1.Y := Y - 1;
      p2.X := X + 1;
      p2.Y := Y + 1;
      Rect := TRectF.Create(X - 3, Y - 3, X + 3, Y + 3);
//      rectBackground.Canvas.DrawLine(p, p, 1);
      Card.FBackground.Canvas.DrawEllipse(Rect, 1);
//      rectBackground.Canvas.map
      X := X + FGridSize;
    end;

    Y := Y + FGridSize;
  end;
  Card.FBackground.Canvas.EndScene;}
end;
{$endregion}

{$region 'RTTI and JSON routines'}
{function GetOrdValue(Info: PTypeInfo; const SetParam): Integer;
begin
  Result := 0;

  case GetTypeData(Info)^.OrdType of
    otSByte, otUByte:
      Result := Byte(SetParam);
    otSWord, otUWord:
      Result := Word(SetParam);
    otSLong, otULong:
      Result := Integer(SetParam);
  end;
end;

function MySetToString(Info: PTypeInfo; const SetParam; Brackets: Boolean): AnsiString;
var
  S: TIntegerSet;
  TypeInfo: PTypeInfo;
  I: Integer;
begin
  Result := '';

  Integer(S) := GetOrdValue(Info, SetParam);
  TypeInfo := GetTypeData(Info)^.CompType^;
  for I := 0 to SizeOf(Integer) * 8 - 1 do
    if I in S then
    begin
      if Result <> '' then
        Result := Result + ',';
      Result := Result + GetEnumName(TypeInfo, I);
    end;
  if Brackets then
    Result := '[' + Result + ']';
end;}

function GetValuesSet(const v: TValue; SetAsValues: boolean): string;
var
  enumType: PTypeInfo;
  enumData: PTypeData;
  buffer:   set of Byte;
  i:        integer;
begin
  result := '';
  buffer := [];
  v.ExtractRawData(@buffer);
  enumType := v.TypeInfo.TypeData.CompType^;
  enumData := enumType.TypeData;
  for i := enumData.MinValue to enumData.MaxValue do
  begin
    if SetAsValues then
    begin
      result := result + GetEnumName(enumType, i) + ':' + (i in buffer).ToString(TUseBoolStrs.True);
      if i < enumData.MaxValue then
        result := result + ',';
    end
    else if i in buffer then
    begin
      result := result + GetEnumName(enumType, i);
      if i < enumData.MaxValue then
        result := result + ',';
    end;
  end;

  if not SetAsValues then
    result := '[' + result + ']';
end;

function TJSONCard.GetPropText(Obj: TObject; Prop: TRttiProperty; SetAsValues: boolean = True): string;
var
  V: TValue;
  b: boolean;
begin
  result := '';
  V      := Prop.GetValue(Obj);
  case Prop.PropertyType.TypeKind of
    tkInteger,
    tkInt64:   result := IntToStr(V.AsInt64);
    tkFloat:
      if Prop.PropertyType.Handle = TypeInfo(TDateTime) then
        result := DateToStr(V.AsType<TDateTime>)
      else
        result := FloatToStr(V.AsType<Double>);
    tkString,
    tkUString: result := V.AsString;
    tkEnumeration: begin
      result := 'ENUM';
      if V.TryAsType<Boolean>(b) then
        result := BoolToStr(b, TRUE)
      else
        result := GetEnumName(V.TypeInfo, Prop.GetValue(Obj).AsOrdinal);
    end;
    tkClass,
    {$IFDEF NEXTGEN}
    tkClassRef: result := '(CLASS)';
    {$ELSE}
    tkClassRef: result := '(' + V.TypeInfo.Name + ')';
    {$ENDIF}
    tkSet: result := GetValuesSet(v, SetAsValues);

    tkUnknown,
    tkChar,
    tkWChar,
    tkLString,
    tkWString,
    tkVariant,
    tkArray,
    tkRecord,
    tkInterface,
    tkDynArray,
    tkPointer,
    tkProcedure,
    tkMRecord: result := GetEnumName (TypeInfo (TTypeKind), Ord (Prop.PropertyType.TypeKind));
  end;
end;

procedure TJSONCard.SetPropText(Obj: TObject; Prop: TRttiProperty; Text: string);
var
  PV, V: TValue;
  i: integer;
  PrevValue: string;

  function IfEmpty (s1, s2: string): string;
  begin
    if not s1.IsEmpty then
      result := s1
    else
      result := s2;
  end;
begin
  PrevValue := GetPropText(Obj, Prop, False);
  if PrevValue = Text then exit;

  if Prop.PropertyType.ToString = 'TAlphaColor' then
  begin
    Text := IfEmpty(Text, PrevValue);
    V    := StrToInt64(Text)
  end
  else
    case Prop.PropertyType.TypeKind of
      tkInteger,
      tkInt64: begin
        Text := IfEmpty(Text, PrevValue);
        I    := StrToIntDef(Text, 0);
        V    := I;
      end;
      tkFloat: begin
        Text := IfEmpty(Text, PrevValue);
        if Prop.PropertyType.Handle = TypeInfo(TDateTime) then
          V := StrToDateDef(Text, 0)
        else
          V := StrToFloatDef(Text, 0);
      end;
      tkString,
      tkUString: V := Text;
      tkEnumeration:
      begin
        Text := IfEmpty(Text, PrevValue);
        PV   := Prop.GetValue(Obj);
        I    := GetEnumValue (PV.TypeInfo, Text);
        TValue.Make(@I, PV.TypeInfo, V);
      end;
      tkSet: begin
        if Pos ('[', Text) > 0 then
          SetSetProp(Obj, Prop.Name, Text);
      end
    else
      Exit; // Or some reasonable default action
    end;

  if Prop.PropertyType.TypeKind <> tkSet then
    Prop.SetValue(Obj, V);

  if not FLoading then
  begin
    if (Prop.Name = 'Name') and (Obj is TControl) then
      ChangeControlName(TControl (Obj), PrevValue, Text);

    Changed := True;
  end;
end;

function TJSONCard.IsPropertyDefaultValue (Prop: TRttiProperty; const Text: string): boolean;
begin
  result := True;

  if (Prop.PropertyType.TypeKind in [tkInteger, tkInt64, tkEnumeration]) and (TRttiInstanceProperty (Prop).Default.ToString <> Text) then
    result := False;
end;

procedure TJSONCard.PopulateExcludeProperties;
begin
  ExcludedProperties.Add('*.Action');
  ExcludedProperties.Add('*.AutoTranslate');
  ExcludedProperties.Add('*.ClipChildren');
  ExcludedProperties.Add('*.ClipParent');
  ExcludedProperties.Add('*.ControlType');
  ExcludedProperties.Add('*.Cursor');
  ExcludedProperties.Add('*.DragMode');
  ExcludedProperties.Add('*.EnableDragHighlight');
  ExcludedProperties.Add('*.Enabled');
  ExcludedProperties.Add('*.StyledSettings');
  ExcludedProperties.Add('*.HitTest');
  ExcludedProperties.Add('*.Locked');
  ExcludedProperties.Add('*.FocusControl');
  ExcludedProperties.Add('*.HelpContext');
  ExcludedProperties.Add('*.HelpKeyword');
  ExcludedProperties.Add('*.HelpType');
  ExcludedProperties.Add('*.PopupMenu');
  ExcludedProperties.Add('*.StyleLookup');
  ExcludedProperties.Add('*.ParentShowHint');
  ExcludedProperties.Add('*.TabOrder');
  ExcludedProperties.Add('*.TabStop');
  ExcludedProperties.Add('*.Touch');
  ExcludedProperties.Add('*.Tag');
  ExcludedProperties.Add('*.StyleName');
  ExcludedProperties.Add('*.MultiResBitmap');
  ExcludedProperties.Add('*.TouchTargetExpansion');
  ExcludedProperties.Add('TJSONFieldLink.Name');
  ExcludedProperties.Add('TJSONLabel.BitmapMargins');
  ExcludedProperties.Add('TJSONLabel.DisableInterpolation');
  ExcludedProperties.Add('TJSONLabel.MarginWrapMode');
  ExcludedProperties.Add('TJSONLabel.WrapMode');
end;

procedure TJSONCard.BackgroundImageChange (Sender: TObject);
begin
  Changed := True;
end;

function TJSONCard.IsExcludedProperty (ClassName, PropertyName: string): boolean;
begin
  result := (ExcludedProperties.IndexOf('*.'            + PropertyName) > -1) or
            (ExcludedProperties.IndexOf(ClassName + '.' + PropertyName) > -1);
end;

function StrJSON (s: string): string;
begin
  s := StringReplace(s, '\', '\\', [rfReplaceAll]);  //Backslash
  s := StringReplace(s, '"', '\"', [rfReplaceAll]);  //Quote
  s := StringReplace(s, #10, '\n', [rfReplaceAll]);  //New line
  s := StringReplace(s, #13, '\r', [rfReplaceAll]);  //Carriage return
  s := StringReplace(s,  #8, '\b', [rfReplaceAll]);  //Backspace
  s := StringReplace(s,  #9, '\t', [rfReplaceAll]);  //Tab
  s := StringReplace(s, #12, '\f', [rfReplaceAll]);  //Form feed
  result := '"' +  s  + '"';
end;

function TJSONCard.CtrlToJSON (Ctrl: TControl): string;
var
  Ctx: TRttiContext;

  procedure ReadSubclass (Level: integer; Prop: TRttiProperty; Obj: TObject); forward;

  procedure ListObjectProperties (Level: integer; Properties: TArray<TRttiProperty>; Obj: TObject);
  var
    i:     integer;
    Prop:  TRttiProperty;
    First: boolean;
  begin
    First := True;
    for i := 0 to Length (Properties) - 1 do
    begin
      Prop := Properties [i];
      if (Prop.Visibility = mvPublished) and
         //Not events
         not (Prop.PropertyType.TypeKind in [tkMethod]) and
         not IsExcludedProperty(Obj.ClassName, Prop.Name) then
      begin
        if not First then
          result := result + ',' + sLineBreak;
        result := result + StringOfChar (' ', Level * 4) + StrJSON (Prop.Name) + ': ';
        if Prop.PropertyType.TypeKind in [tkClass, tkClassRef] then
        begin
          result := result + '{' + sLineBreak;
          ReadSubclass (Level + 1, Prop, Obj);
          result := result + sLineBreak + StringOfChar (' ', Level * 4) + '}';
        end
        else
          result := result + StrJSON (GetPropText (Obj, Prop, False));
        First := False;
      end;
    end;
  end;

  procedure ReadSubclass (Level: integer; Prop: TRttiProperty; Obj: TObject);
  var
    V:          TValue;
    ObjType:    TRttiType;
    Properties: TArray<TRttiProperty>;
  begin
    if Prop.PropertyType.TypeKind = tkClass then
    begin
      V := Prop.GetValue(Obj);
      if V.IsObject and (V.AsObject <> nil) then
      begin
        objType    := Ctx.GetType(V.AsObject.ClassInfo);
        Properties := objType.GetProperties;
        ListObjectProperties (Level, Properties, V.AsObject);
      end;
    end;
  end;

var
  ObjType:    TRttiType;
  Properties: TArray<TRttiProperty>;
begin
  result := '';
  if not Assigned (Ctrl) then exit;

  Ctx := TRttiContext.Create;
  try
    objType    := Ctx.GetType(Ctrl.ClassInfo);
    Properties := objType.GetProperties;
    result     := '{' + sLineBreak + StringOfChar (' ', 4) + StrJSON('ParentCtrl') + ': ';
    if not Assigned (Ctrl.Parent) or (Ctrl.Parent = FBackground) then
      result := result + '""'
    else
      result := result + StrJSON(Ctrl.Parent.Name);

    if Length (Properties) > 0 then
      result := result + ',';

    result := result + sLineBreak;
    ListObjectProperties (1, Properties, Ctrl);
  finally
    Ctx.Free;
    result := result + sLineBreak + '}';
  end;
end;

procedure TJSONCard.ReadCtrlFromJSON (Ctrl: TControl; JSON: string);
var
  Ctx:       TRttiContext;
  JSONValue: TJSONValue;

  procedure ReadSubclass (ObjPath: string; Prop: TRttiProperty; Obj: TObject); forward;

  procedure SetObjectProperties (ObjPath: string; Properties: TArray<TRttiProperty>; Obj: TObject);
  var
    i:        integer;
    Prop:     TRttiProperty;
    s:        string;
    PropName: string;
  begin
    for i := 0 to Length (Properties) - 1 do
    begin
      Prop := Properties [i];
      if (Prop.Visibility = mvPublished) and
         //Not events
         not (Prop.PropertyType.TypeKind in [tkMethod]) and
         not IsExcludedProperty(Obj.ClassName, Prop.Name) then
      begin
        if ObjPath.IsEmpty then
          PropName := Prop.Name
        else
          PropName := ObjPath + '.' + Prop.Name;

        if Prop.PropertyType.TypeKind in [tkClass, tkClassRef] then
          ReadSubclass (PropName, Prop, Obj)
        else if PropName <> 'Name' then
        begin
          s := '';
          if JSONValue.FindValue (PropName) <> nil then
            s := JSONValue.GetValue<string>(PropName);

          SetPropText(Obj, Prop, s);
        end;
      end;
    end;
  end;

  procedure ReadSubclass (ObjPath: string; Prop: TRttiProperty; Obj: TObject);
  var
    V:          TValue;
    ObjType:    TRttiType;
    Properties: TArray<TRttiProperty>;
  begin
    if Prop.PropertyType.TypeKind = tkClass then
    begin
      V := Prop.GetValue(Obj);
      if V.IsObject and (V.AsObject <> nil) then
      begin
        objType    := Ctx.GetType(V.AsObject.ClassInfo);
        Properties := objType.GetProperties;
        SetObjectProperties (ObjPath, Properties, V.AsObject);
      end;
    end;
  end;

var
  ObjType:    TRttiType;
  Properties: TArray<TRttiProperty>;
begin
  Ctx       := TRttiContext.Create;
  JSONValue := TJSONObject.ParseJSONValue(JSON);
  try
    Ctrl.BeginUpdate;
    objType    := Ctx.GetType(Ctrl.ClassInfo);
    Properties := objType.GetProperties;
    SetObjectProperties ('', Properties, Ctrl);
  finally
    Ctrl.EndUpdate;
    Ctx.Free;
    JSONValue.Free;
  end;
end;
{$endregion}

{$region 'Android Control Routines'}
{$IFDEF ANDROID}
procedure TJSONCard.ClearControlNames;
var
  i: integer;
begin
  for i := 0 to LstControls.Count - 1 do
    TControl (LstControls [i]).Name := '';
end;

procedure TJSONCard.AssignControlNames;
var
  i: integer;
begin
  for i := 0 to LstControls.Count - 1 do
    TControl (LstControls [i]).Name := CtrlNames [i];
end;
{$ENDIF}
{$endregion}

{$region 'Garbage routines'}
procedure TJSONCard.PurgeGarbageCtrls;
var
  Ctrl: TControl;
begin
  while FGarbageCtrls.Count > 0 do
  begin
    Ctrl := TControl (FGarbageCtrls [0]);
    FGarbageCtrls.Delete(0);
    Ctrl.Free;
  end;
end;

procedure TJSONCard.AddGarbageCtrl (Ctrl: TControl);
begin
  TimerPurgeGarbage.Enabled := False;
  Ctrl.Visible              := False;
  Ctrl.Name                 := '';
  FGarbageCtrls.Add(Ctrl);
  TimerPurgeGarbage.Enabled := True;
end;

procedure TJSONCard.TimerPurgeGarbageTimer(Sender: TObject);
begin
  TimerPurgeGarbage.Enabled := False;
  PurgeGarbageCtrls;
end;
{$endregion}

{$region 'Tree structure routines'}
procedure TJSONCard.BuildTreeStructure;
var
  i:      integer;
  Item:   TTreeViewStructureItem;
  Ctrl:   TControl;
  Tool:   TPaletteTool;
  ImgIdx: integer;

  procedure DisplayChilds (Item: TTreeViewStructureItem);
  var
    Ctrl:   TControl;
    i, Idx: integer;
    Child:  TTreeViewStructureItem;
  begin
    if not Assigned (Item) or not Assigned (Item.Ctrl) then exit;
    Ctrl := Item.Ctrl;

    for i := 0 to Ctrl.ControlsCount - 1 do
    begin
      Idx := LstControls.IndexOf(Ctrl.Controls [i]);
      if Idx <> -1 then
      begin
        Tool := GetControlType(Ctrl.Controls [i]);
        GetStructureImageIndex(Tool, ImgIdx);
        Child            := TTreeViewStructureItem.Create(FTreeStructure);
        Child.Parent     := Item;
        Child.Text       := Ctrl.Controls [i].Name;
        Child.Ctrl       := Ctrl.Controls [i];
        Child.Ctrl.Tag   := ImgIdx;
        Child.ImageIndex := Child.Ctrl.Tag;
        DisplayChilds(Child);
      end;
    end;
  end;
begin
  if not Assigned (FTreeStructure) then exit;

  FTreeStructure.BeginUpdate;
  try
    FTreeStructure.Clear;
    if not FDesignMode then exit;

    for i := 0 to FLstControls.Count - 1 do
    begin
      Ctrl := TControl (FLstControls [i]);
      Tool := GetControlType(Ctrl);
      GetStructureImageIndex(Tool, ImgIdx);
      if Ctrl.ParentControl = FBackground then
      begin
        Item            := TTreeViewStructureItem.Create(FTreeStructure);
        Item.Parent     := FTreeStructure;
        Item.Text       := Ctrl.Name;
        Item.Ctrl       := Ctrl;
        Ctrl.Tag        := ImgIdx;
        Item.ImageIndex := Ctrl.Tag;
        DisplayChilds (Item);
      end;
    end;
  finally
    FTreeStructure.ExpandAll;
    FTreeStructure.EndUpdate;
  end;
end;

function TJSONCard.FindItemTreeStructure (Ctrl: TControl): TTreeViewStructureItem;
var
  Current: TTreeViewStructureItem;
  i:       integer;

  procedure FindInChilds (Item: TTreeViewStructureItem);
  var
    i:     integer;
    Child: TTreeViewStructureItem;
  begin
    for i := 0 to Item.Count - 1 do
    begin
      Child := TTreeViewStructureItem (Item.Items [i]);
      if Child.Ctrl = Ctrl then
      begin
        result := Child;
        exit;
      end;

      FindInChilds(Child);
      if Assigned (result) then
        exit;
    end;
  end;
begin
  result := nil;

  if not Assigned (FTreeStructure) then exit;

  for i := 0 to FTreeStructure.Count - 1 do
  begin
    Current := TTreeViewStructureItem (FTreeStructure.Items [i]);
    if Current.Ctrl = Ctrl then
    begin
      result := Current;
      exit;

      if Assigned (result) then exit;
    end;
    FindInChilds (Current);
  end;
end;

procedure TJSONCard.UpdateNameTreeStructure (Ctrl: TControl);
var
  Item: TTreeViewStructureItem;
begin
  if not FDesignMode or not Assigned (FTreeStructure) then exit;

  Item := FindItemTreeStructure(Ctrl);
  if Assigned (Item) then
    Item.Text := Item.Ctrl.Name;
end;

procedure TJSONCard.AddCtrlToTreeStructure (Ctrl: TControl);
var
  Item, Child: TTreeViewStructureItem;
begin
  if not FDesignMode or not Assigned (FTreeStructure) then exit;

  Item             := FindItemTreeStructure(Ctrl.ParentControl);
  Child            := TTreeViewStructureItem.Create(FTreeStructure);
  Child.Text       := Ctrl.Name;
  Child.Ctrl       := Ctrl;
  Child.ImageIndex := Ctrl.Tag;
  if Assigned (Item) then
  begin
    Child.Parent := Item;
    Item.Expand;
  end
  else
    Child.Parent := FTreeStructure;

  Child.IsSelected := IsSelectedCtrl(Ctrl);
end;

procedure TJSONCard.UpdateSelectionTreeStructure;
var
  i:    integer;
  Item: TTreeViewStructureItem;

  procedure SelectChilds (Item: TTreeViewStructureItem);
  var
    i:     integer;
    Child: TTreeViewStructureItem;
  begin
    for i := 0 to Item.Count - 1 do
    begin
      Child            := TTreeViewStructureItem (Item.Items [i]);
      Child.IsSelected := IsSelectedCtrl(Child.Ctrl);
      SelectChilds(Child);
    end;
  end;
begin
  if not FDesignMode or not Assigned (FTreeStructure) then exit;

  for i := 0 to FTreeStructure.Count - 1 do
  begin
    Item            := TTreeViewStructureItem (FTreeStructure.Items [i]);
    Item.IsSelected := IsSelectedCtrl(Item.Ctrl);
    SelectChilds (Item);
  end;
end;

procedure TJSONCard.UserChangeSelectionTreeStructure;
var
  Tmp:  TList;
  i:    integer;
  Item: TTreeViewStructureItem;
  Ctrl: TControl;

  procedure VerifyChilds (Item: TTreeViewStructureItem);
  var
    i:     integer;
    Child: TTreeViewStructureItem;
  begin
    for i := 0 to Item.Count - 1 do
    begin
      Child := TTreeViewStructureItem (Item.Items [i]);
      if Child.IsSelected then
        Tmp.Add(Child.Ctrl);

      VerifyChilds(Child);
    end;
  end;
begin
  if not FDesignMode or not Assigned (FTreeStructure) then exit;

  Tmp := TList.Create;
  try
    for i := 0 to FTreeStructure.Count - 1 do
    begin
      Item := TTreeViewStructureItem (FTreeStructure.Items [i]);
      if Item.IsSelected then
        Tmp.Add(Item.Ctrl);

      VerifyChilds (Item);
    end;

    ClearSelection;
    for i := 0 to Tmp.Count - 1 do
    begin
      Ctrl := TControl (Tmp [i]);
      AddSelection(Ctrl);
    end;
  finally
    Tmp.Clear;
    Tmp.Free;
  end;
end;

procedure TJSONCard.TreeStructureMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  if not FDesignMode or not Assigned (FTreeStructure) or (Sender <> FTreeStructure) then exit;

  UserChangeSelectionTreeStructure;
end;
{$endregion}

{$region 'Event handlers'}
procedure TJSONCard.ChangeCtrlSelection;
begin
  UpdateSelectionTreeStructure;

  if Assigned (FOnChangeCtrlSelection) then
    FOnChangeCtrlSelection (Self);
end;

procedure TJSONCard.ChangeCurrentTool;
begin
  if Assigned (FOnChangeCurrentTool) then
    FOnChangeCurrentTool (Self);
end;

procedure TJSONCard.ControlNotification (Ctrl: TControl; AOperation: TOperation);
begin
  if Assigned (FOnControlNotitication) then
    FOnControlNotitication (Self, Ctrl, AOperation);
end;

procedure TJSONCard.Change;
begin
  if Assigned (FOnChange) then
    FOnChange (Self);
end;

procedure TJSONCard.ChangeControlName (Ctrl: TControl; OldName, NewName: string);
begin
  if Assigned (FOnControlChangeName) then
    FOnControlChangeName (Self, Ctrl, OldName, NewName);

  UpdateNameTreeStructure(Ctrl);
end;

procedure TJSONCard.GetStructureImageIndex (Tool: TPaletteTool; var ImageIndex: integer);
begin
  ImageIndex := -1;
  if Assigned (FOnGetStructureImageIndex) then
    FOnGetStructureImageIndex (Self, Tool, ImageIndex);
end;

{$endregion}

{$region 'Mouse routines'}
function TJSONCard.PnlMouseAction (Button: TMouseButton; Shift: TShiftState): TMouseAction;
begin
  result := maNone;

  if Button = TMouseButton.mbLeft then
  begin
    if ssCtrl in Shift then
      result := maMultiSelect
    else
    begin
      if CurrentTool = ptDeselect then
        result := maSelect
      else
        result := maCreateObject;
    end
  end;
end;

procedure TJSONCard.BackgroundMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  CtrlParent,
  Ctrl:       TControl;
  MouseAct:   TMouseAction;
  IsSelected: boolean;
  Sel:        TSelectionCtrl;
  Idx:        integer;
  CtrlBounds: TBounds;
begin
  if not DesignMode then exit;
  CtrlParent := ControlAtPos (X, Y);

  MouseAct := PnlMouseAction (Button, Shift);
  if not ((MouseAct = maMultiSelect) or ((MouseAct = maSelect) and Assigned (CtrlParent) and IsSelectedCtrl (CtrlParent))) then
    ClearSelection;

  if not Assigned (CtrlParent) then
    CtrlParent := FBackground;

  case CurrentTool of
    ptDeselect: begin
      Ctrl       := ControlAtPos (X, Y);
      CtrlBounds := GetControlRealPosition(Ctrl);
      IsSelected := IsSelectedCtrl (Ctrl);

      if Assigned (Ctrl) and ((SelectedCtrls.Count = 0) or (Selected = Ctrl)) and
         Math.InRange(CtrlBounds.Right  - X, 0, 15) and
         Math.InRange(CtrlBounds.Bottom - Y, 0, 15) then
      begin
        Sel := nil;
        Idx := IndexsSelectedCtrl(Ctrl);
        if Idx <> -1 then
          Sel := SelectedCtrls [Idx];

        LastPosition.X     := X;
        LastPosition.Y     := Y;
        Resizing           := True;
        ResizingCtrl       := Ctrl;
        Ctrl.Cursor        := crSizeNWSE;
        FBackground.Cursor := crSizeNWSE;

        if Assigned (Sel) then
          Sel.Cursor := crSizeNWSE;
      end
      else if (MouseAct = maMultiSelect) and IsSelected then
        DeleteSelection(Ctrl)
      else if (MouseAct = maSelect) and IsSelected then
      begin
        DragEnabled        := True;
        MouseDownPos.X     := X;
        MouseDownPos.Y     := Y;
        Cursor             := crSizeAll;
        FBackground.Cursor := crSizeAll;
      end
      else if Ctrl <> FBackground then
        AddSelection (Ctrl);

      CtrlBounds.Free;
    end
    else
    begin
      if CtrlParent = FBackground then
        CreateCtrl (CurrentTool, CtrlParent, X, Y)
      else
      begin
        CtrlBounds := GetControlRealPosition(CtrlParent);
        CreateCtrl (CurrentTool, CtrlParent, X - CtrlBounds.Left, Y - CtrlBounds.Top);
        CtrlBounds.Free;
      end;
    end;
  end;
end;

procedure TJSONCard.BackgroundMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
var
  i, Idx:     integer;
  Sel:        TSelectionCtrl;
  DeltaX,
  DeltaY:     Single;
  Ctrl:       TControl;
  CtrlBounds: TBounds;
begin
  if DragEnabled then
  begin
    DeltaX         := X - MouseDownPos.X;
    DeltaY         := Y - MouseDownPos.Y;
    MouseDownPos.X := X;
    MouseDownPos.Y := Y;
    for i := 0 to SelectedCtrls.Count - 1 do
    begin
      Sel := TSelectionCtrl (SelectedCtrls [i]);
      if (Sel.Control.Align = Align.alNone) and not IsSelectedAncestorCtrl (Sel.Control) then
      begin
        Sel.Control.Position.X := Sel.Control.Position.X + DeltaX;
        Sel.Control.Position.Y := Sel.Control.Position.Y + DeltaY;
        Sel.Position.X         := Sel.Control.Position.X;
        Sel.Position.Y         := Sel.Control.Position.Y;
      end;
      Application.ProcessMessages;
    end;
  end
  else if Resizing then
  begin
    Sel  := nil;
    Idx  := IndexsSelectedCtrl(ResizingCtrl);
    if Idx <> -1 then
      Sel := SelectedCtrls [Idx];

    ResizingCtrl.Width  := ResizingCtrl.Width  + (X - LastPosition.X);
    ResizingCtrl.Height := ResizingCtrl.Height + (Y - LastPosition.Y);
    LastPosition.X      := X;
    LastPosition.Y      := Y;

    if Assigned (Sel) then
    begin
      Sel.Width  := ResizingCtrl.Width;
      Sel.Height := ResizingCtrl.Height;
    end;
  end
  else
  begin
    Sel        := nil;
    Ctrl       := ControlAtPos(X, Y);
    Idx        := IndexsSelectedCtrl(Ctrl);
    if Idx <> -1 then
      Sel := SelectedCtrls [Idx];

    if Assigned (Ctrl) then
    begin
      CtrlBounds := GetControlRealPosition(Ctrl);
      if Math.InRange (CtrlBounds.Right  - X, 0, 15) and
         Math.InRange (CtrlBounds.Bottom - Y, 0, 15) then
      begin
        FBackground.Cursor := crSizeNWSE;
        Ctrl.Cursor        := crSizeNWSE;
        if Assigned (Sel) then
          Sel.Cursor := crSizeNWSE;
      end
      else
      begin
        FBackground.Cursor := crArrow;
        Ctrl.Cursor        := crArrow;
        if Assigned (Sel) then
          Sel.Cursor := crArrow;
      end;
      CtrlBounds.Free;
    end;
  end;
end;

procedure TJSONCard.BackgroundMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  Sel:      TSelectionCtrl;
  Idx:      integer;
  _changed: boolean;
begin
  _changed    := DragEnabled or Resizing;
  DragEnabled := False;
  Resizing    := False;
  if Assigned (ResizingCtrl) then
  begin
    ResizingCtrl.Cursor := crArrow;
    Idx                 := IndexsSelectedCtrl(ResizingCtrl);
    if Idx <> -1 then
    begin
      Sel        := SelectedCtrls [Idx];
      Sel.Cursor := crArrow;
    end;
  end;
  ResizingCtrl        := nil;
  FBackground.Cursor  := crArrow;

  if _changed then
    Changed := True;
end;
{$endregion}

{$region 'Selection routines'}
procedure TJSONCard.ClearSelection;
var
  i:       integer;
  SelCtrl: TSelectionCtrl;
begin
  for i := 0 to FSelectedCtrls.Count - 1 do
  begin
    SelCtrl            := TSelectionCtrl (FSelectedCtrls [i]);
    FSelectedCtrls [i] := nil;
    SelCtrl.Free;
  end;
  FSelectedCtrls.Clear;

  if not FDestroying then
    ChangeCtrlSelection;
end;

function TJSONCard.IndexsSelectedCtrl (Ctrl: TControl): integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to FSelectedCtrls.Count - 1 do
    if TSelectionCtrl (FSelectedCtrls [i]).Control = Ctrl then
    begin
      result := i;
      exit;
    end;
end;

function TJSONCard.IsSelectedCtrl (Ctrl: TControl): boolean;
begin
  result := IndexsSelectedCtrl(Ctrl) <> -1;
end;

function TJSONCard.IsSelectedAncestorCtrl (Ctrl: TControl): boolean;
var
  ParentCtrl: TControl;
begin
  result     := False;
  ParentCtrl := Ctrl.ParentControl;
  while Assigned (ParentCtrl) and (ParentCtrl <> FBackground) do
  begin
    if IsSelectedCtrl (ParentCtrl) then
    begin
      result := True;
      exit;
    end;

    ParentCtrl := ParentCtrl.ParentControl;
  end;
end;

function TJSONCard.GetSelected: TControl;
begin
  result := nil;

  if FSelectedCtrls.Count <> 1 then exit;

  result := TSelectionCtrl (FSelectedCtrls [0]).Control;
end;

procedure TJSONCard.AddSelection (Ctrl: TControl);
var
  SelCtrl: TSelectionCtrl;
begin
  if not Assigned (Ctrl) or IsSelectedCtrl (Ctrl) then exit;

  FUpdatingSelection := True;
  try
    SelCtrl             := TSelectionCtrl.Create(Self);
    SelCtrl.Control     := Ctrl;
    SelCtrl.HitTest     := False;
    FSelectedCtrls.Add(SelCtrl);
    UpdateSelections;

    ChangeCtrlSelection;
  finally
    FUpdatingSelection := False;
  end;
end;

procedure TJSONCard.DeleteSelection (Ctrl: TControl);
var
  Idx: integer;
  SelCtrl: TSelectionCtrl;
begin
  Idx := IndexsSelectedCtrl(Ctrl);
  if Idx = -1 then exit;

  SelCtrl := TSelectionCtrl (FSelectedCtrls [Idx]);
  FSelectedCtrls.Delete(Idx);
  AddGarbageCtrl (SelCtrl);
  UpdateSelections;

  ChangeCtrlSelection;
end;

procedure TJSONCard.UpdateSelections;
var
  i: integer;
begin
  for i := 0 to FSelectedCtrls.Count - 1 do
    TSelectionCtrl (FSelectedCtrls [i]).UpdatePosition;
end;
{$endregion}

{$region 'Control routines'}
function TJSONCard.ControlAtPos (X, Y: Single): TControl;
var
  i, ZOrder, Depth, CtrlZOrder, CtrlDepth: integer;
  Ctrl: TControl;
  CtrlBounds: TBounds;
begin
  result := nil;
  ZOrder := -1;
  Depth  := -1;

  for i := 0 to FLstControls.Count - 1 do
  begin
    Ctrl       := TControl (FLstControls [i]);
    CtrlBounds := GetControlRealPosition(Ctrl);
    CtrlZOrder := zzGetControlZOrder(Ctrl);
    CtrlDepth  := GetControlDepth(Ctrl);
    if Math.InRange (X, CtrlBounds.Left, CtrlBounds.Right)  and
       Math.InRange (Y, CtrlBounds.Top,  CtrlBounds.Bottom) and
       (((CtrlDepth = Depth) and (CtrlZOrder > ZOrder)) or (CtrlDepth > Depth)) then
    begin
      result := Ctrl;
      ZOrder := CtrlZOrder;
      Depth  := CtrlDepth;
    end;
    CtrlBounds.Free;
  end;
end;

function TJSONCard.GetControlByName (CtrlName: string): TControl;
var
  i: integer;
  Ctrl: TControl;
begin
  result   := nil;
  CtrlName := UpperCase (CtrlName);
  for i := 0 to FLstControls.Count - 1 do
  begin
    Ctrl := TControl (FLstControls [i]);
    if UpperCase (Ctrl.Name) = CtrlName then
    begin
      result := Ctrl;
      exit;
    end;
  end;
end;

function TJSONCard.GenerateCtrlName (Ctrl: TControl): string;
var
  i: integer;
begin
  result := Ctrl.ClassName;
  i      := 0;
  repeat
    Inc (i);
  until GetControlByName (result + i.ToString) = nil;
  result := result + i.ToString;
end;

procedure TJSONCard.ClearLstControls;
var
  Ctrl: TControl;
  Tmp: TList;
begin
  Tmp := TList.Create;
  try
    CtrlsPostOrder (Tmp);

    while Tmp.Count > 0 do
    begin
      Ctrl := TControl (Tmp [Tmp.Count - 1]);
      Tmp.Delete(Tmp.Count - 1);
      Ctrl.Free;
    end;
  finally
    FLstControls.Clear;
    Tmp.Clear;
    Tmp.Free;
  end;
end;

procedure TJSONCard.AddControl (Ctrl: TControl);
begin
  FLstControls.Add(Ctrl);
  ControlNotification(Ctrl, opInsert);
end;

procedure TJSONCard.DeleteControl (Ctrl: TControl);
var
  Idx: integer;
begin
  Idx := FLstControls.IndexOf(Ctrl);

  if Idx <> -1 then
    FLstControls.Delete(Idx);

  ControlNotification(Ctrl, opRemove);
  Ctrl.Free;
end;

procedure TJSONCard.DeleteSelectedControls;
var
  i:   integer;
  Lst: TList;
  Sel: TSelectionCtrl;
begin
  Lst := TList.Create;
  try
    for i := 0 to SelectedCtrls.Count - 1 do
    begin
      Sel := TSelectionCtrl (SelectedCtrls [i]);
      Lst.Add(Sel.Control);
    end;

    ClearSelection;
    while Lst.Count > 0 do
    begin
      DeleteControl(TControl (Lst [0]));
      Lst [0] := nil;
      Lst.Delete(0);
    end;
  finally
    Lst.Free;
  end;
  BuildTreeStructure;
  Changed := True;
end;

function TJSONCard.GetControlRealPosition (Ctrl: TControl): TBounds;
var
  CtrlParent: TControl;
begin
  if not Assigned (Ctrl) or (Ctrl = FBackground) then
  begin
    result := TBounds.Create(TRect.Create(0, 0, 0, 0));
    exit;
  end;

  result     := TBounds.Create(Ctrl.BoundsRect);
  CtrlParent := Ctrl.ParentControl;
  while CtrlParent <> FBackground do
  begin
    result.Left := result.Left + CtrlParent.Position.X;
    result.Top  := result.Top  + CtrlParent.Position.Y;
    CtrlParent  := CtrlParent.ParentControl;
  end;
  result.Right  := result.Left + Ctrl.Width;
  result.Bottom := result.Top  + Ctrl.Height;
end;

function TJSONCard.GetControlDepth (Ctrl: TControl): integer;
var
  CtrlParent: TControl;
begin
  result := 0;
  if not Assigned (Ctrl) or (Ctrl = FBackground) then exit;

  CtrlParent := Ctrl.ParentControl;
  while CtrlParent <> FBackground do
  begin
    Inc (result);
    CtrlParent  := CtrlParent.ParentControl;
  end;
end;

function TJSONCard.GetControlType (Control: TControl): TPaletteTool;
begin
    result := ptDeselect;
    if      Control is TJSONLabel     then
      result := ptLabel
    else if Control is TJSONImage     then
      result := ptImage
    else if Control is TCardLine      then
      result := ptLine
    else if Control is TCardRectangle then
      result := ptRectangle
    else if Control is TCardRoundRect then
      result := ptRoundedRect
    else if Control is TCardCircle    then
      result := ptCircle
    else if Control is TCardEllipse   then
      result := ptEllipse
    else if Control is TCardArc       then
      result := ptArc;
end;

procedure TJSONCard.ForceCtrlIntoViewport (Ctrl: TControl);
var
  CtrlParent: TControl;
begin
  CtrlParent := TControl (Ctrl.Parent);
  if Math.InRange (Ctrl.Position.X,               0, CtrlParent.Width)  and
     Math.InRange (Ctrl.Position.X + Ctrl.Width,  0, CtrlParent.Width)  and
     Math.InRange (Ctrl.Position.Y,               0, CtrlParent.Height) and
     Math.InRange (Ctrl.Position.Y + Ctrl.Height, 0, CtrlParent.Height)
   then exit;

  if not Math.InRange (Ctrl.Position.X + Ctrl.Width, 0, CtrlParent.Width) then
    Ctrl.Position.X := CtrlParent.Width - Ctrl.Width;

  if not Math.InRange (Ctrl.Position.Y + Ctrl.Height, 0, CtrlParent.Height) then
    Ctrl.Position.Y := CtrlParent.Height - Ctrl.Height;
end;

procedure TJSONCard.RepaintControls;
var
  i: integer;
begin
  for i := 0 to LstControls.Count - 1 do
    TControl (LstControls [i]).Repaint;

  FBackground.Repaint;
  Repaint;
  Application.ProcessMessages;
end;

function TJSONCard.ChangeControlParent (Ctrl, NewParent: TControl): boolean;
begin
  Ctrl.Parent := NewParent;
  Changed     := True;
  ForceCtrlIntoViewport (Ctrl);
  BuildTreeStructure;
  UpdateSelectionTreeStructure;
end;

function TJSONCard.IsChildOf (Tested, Ctrl: TControl): boolean;
  procedure FindInChilds (Ctrl: TControl);
  var
    i: integer;
  begin
    if result then exit;

    for i := 0 to Ctrl.ControlsCount - 1 do
    begin
      if Ctrl.Controls [i] = Tested then
      begin
        result := True;
        exit;
      end;

      FindInChilds (Ctrl.Controls [i]);
    end;
  end;

begin
  result := False;
  FindInChilds (Ctrl);
end;

function TJSONCard.CreateCtrl (Tool: TPaletteTool; CtrlParent: TControl; X, Y: Single; JSON: string = ''): TControl;
var
  Lab:    TJSONLabel;
  Img:    TJSONImage;
  Bmp:    TBitmap;
  Line:   TCardLine;
  Rect:   TCardRectangle;
  Rnd:    TCardRoundRect;
  Ell:    TCardEllipse;
  Circ:   TCardCircle;
  Arc:    TCardArc;
  ImgIdx: integer;
begin
  result := nil;
  GetStructureImageIndex (Tool, ImgIdx);
  case Tool of
    ptLabel: begin
      Lab                      := TJSONLabel.Create(CtrlParent);
      Lab.BeginUpdate;
      Lab.Parent               := CtrlParent;
      Lab.Position.X           := X;
      Lab.Position.Y           := Y;
      Lab.Name                 := GenerateCtrlName (Lab);
      Lab.Text                 := Lab.Name;
      {$IFNDEF RTML}
      Lab.StyledSettings       := [];
      {$ENDIF}
      Lab.ClipChildren         := True;
      Lab.JSONLink.JSONDataSet := JSONDataSet;
      result                   := Lab;
    end;
    ptImage: begin
      Img                      := TJSONImage.Create(CtrlParent);
      Img.BeginUpdate;
      Img.Parent               := CtrlParent;
      Img.Name                 := GenerateCtrlName (Img);
      Img.Position.X           := X;
      Img.Position.Y           := Y;
      Img.JSONLink.JSONDataSet := JSONDataSet;
      Img.ClipChildren         := True;
      result                   := Img;
      Img.Bitmap.SetSize(Round(Img.Width), Round(Img.Height));
      Bmp := TBitmap.Create;
      Bmp.SetSize(Round(Img.Width), Round(Img.Height));

      if DesignMode then
        Bmp.Clear(TAlphaColors.White);

      Img.Bitmap.Assign(Bmp);
      Bmp.free;
    end;
    ptLine: begin
      Line              := TCardLine.Create(CtrlParent);
      Line.BeginUpdate;
      Line.Parent       := CtrlParent;
      Line.Name         := GenerateCtrlName (Line);
      Line.Position.X   := X;
      Line.Position.Y   := Y;
      Line.ClipChildren := True;
      result            := Line;
    end;
    ptRectangle: begin
      Rect              := TCardRectangle.Create(CtrlParent);
      Rect.BeginUpdate;
      Rect.Parent       := CtrlParent;
      Rect.Name         := GenerateCtrlName (Rect);
      Rect.Position.X   := X;
      Rect.Position.Y   := Y;
      Rect.ClipChildren := True;
      result            := Rect;
    end;
    ptRoundedRect: begin
      Rnd              := TCardRoundRect.Create(CtrlParent);
      Rnd.BeginUpdate;
      Rnd.Parent       := CtrlParent;
      Rnd.Name         := GenerateCtrlName (Rnd);
      Rnd.Position.X   := X;
      Rnd.Position.Y   := Y;
      Rnd.ClipChildren := True;
      result           := Rnd;
    end;
    ptEllipse: begin
      Ell              := TCardEllipse.Create(CtrlParent);
      Ell.BeginUpdate;
      Ell.Parent       := CtrlParent;
      Ell.Name         := GenerateCtrlName (Ell);
      Ell.Position.X   := X;
      Ell.Position.Y   := Y;
      Ell.ClipChildren := True;
      result           := Ell;
    end;
    ptCircle: begin
      Circ              := TCardCircle.Create(CtrlParent);
      Circ.BeginUpdate;
      Circ.Parent       := CtrlParent;
      Circ.Name         := GenerateCtrlName (Circ);
      Circ.Position.X   := X;
      Circ.Position.Y   := Y;
      Circ.ClipChildren := True;
      result            := Circ;
    end;
    ptArc: begin
      Arc              := TCardArc.Create(CtrlParent);
      Arc.BeginUpdate;
      Arc.Parent       := CtrlParent;
      Arc.Name         := GenerateCtrlName (Arc);
      Arc.Position.X   := X;
      Arc.Position.Y   := Y;
      Arc.ClipChildren := True;
      result           := Arc;
    end;
  end;

  if Assigned (result) then
  begin
    result.HitTest := False;
    result.Tag     := ImgIdx;
    CurrentTool    := ptDeselect;

    AddControl (result);
    if not FLoading then
      AddSelection (result);

    if not JSON.IsEmpty then
      ReadCtrlFromJSON(result, JSON);

    AddCtrlToTreeStructure (result);

    if not FLoading then
      Changed := True;

    result.EndUpdate;
  end;
end;

procedure TJSONCard.CtrlsPostOrder (Ctrls: TList);
var
  i:    integer;
  Ctrl: TControl;

  procedure PostOrder (Ctrl: TControl);
  var
    i:     integer;
    Child: TControl;
  begin
    if not Assigned (Ctrl) then exit;

    Ctrls.Add(Ctrl);
    for i := 0 to Ctrl.ControlsCount - 1 do
    begin
      Child := Ctrl.Controls [i];
      if LstControls.IndexOf (Child) <> -1 then
        PostOrder (Child);
    end;
  end;
begin
  Ctrls.Clear;

  for i := 0 to LstControls.Count - 1 do
  begin
    Ctrl := TControl (LstControls [i]);
    if Ctrl.Parent = FBackground then
      PostOrder (Ctrl);
  end;
end;

{$endregion}

{$region 'Align routines'}
procedure TJSONCard.AlignVertTop;
var
  i:            integer;
  MinTop:       Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MinTop := Height * 2;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MinTop     := Math.Min(CtrlBounds.Top, MinTop);
    CtrlBounds.Free;
  end;

  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;
    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.Y := MinTop
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.Y := MinTop - ParentBounds.Top;
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;

procedure TJSONCard.AlignVertMiddle;
var
  i:            integer;
  MinTop:       Single;
  MaxBottom:    Single;
  Center:       Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MinTop    := Height * 2;
  MaxBottom := -1;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MinTop     := Math.Min(CtrlBounds.Top, MinTop);
    MaxBottom  := Math.Max(CtrlBounds.Bottom, MaxBottom);
  end;

  Center := (MinTop + MaxBottom) / 2;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;

    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.Y := Center - (Ctrl.Height / 2)
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.Y := Center - ParentBounds.Top - (Ctrl.Height / 2);
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;

procedure TJSONCard.AlignVertBottom;
var
  i:            integer;
  MaxBottom:    Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MaxBottom := -1;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MaxBottom  := Math.Max(CtrlBounds.Bottom, MaxBottom);
  end;

  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;
    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.Y := MaxBottom - Ctrl.Height
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.Y := MaxBottom - ParentBounds.Top - Ctrl.Height;
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;

procedure TJSONCard.AlignHorizLeft;
var
  i:            integer;
  MinLeft:      Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MinLeft := Width * 2;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MinLeft    := Math.Min(CtrlBounds.Left, MinLeft);
    CtrlBounds.Free;
  end;

  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;
    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.X := MinLeft
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.X := MinLeft - ParentBounds.Left;
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;

procedure TJSONCard.AlignHorizCenter;
var
  i:            integer;
  MinLeft:      Single;
  MaxRight:     Single;
  Center:       Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MinLeft  := Width * 2;
  MaxRight := -1;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MinLeft    := Math.Min(CtrlBounds.Left,  MinLeft);
    MaxRight   := Math.Max(CtrlBounds.Right, MaxRight);
  end;

  Center := (MinLeft + MaxRight) / 2;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;

    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.X := Center - (Ctrl.Width / 2)
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.X := Center - ParentBounds.Left - (Ctrl.Width / 2);
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;

procedure TJSONCard.AlignHorizRight;
var
  i:            integer;
  MaxRight:     Single;
  Ctrl:         TControl;
  CtrlBounds:   TBounds;
  ParentBounds: TBounds;
begin
  MaxRight := -1;
  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl       := TSelectionCtrl (SelectedCtrls [i]).Control;
    CtrlBounds := GetControlRealPosition(Ctrl);
    MaxRight   := Math.Max(CtrlBounds.Right, MaxRight);
  end;

  for i := 0 to SelectedCtrls.Count - 1 do
  begin
    Ctrl := TSelectionCtrl (SelectedCtrls [i]).Control;
    if Ctrl.ParentControl = FBackground then
      Ctrl.Position.X := MaxRight - Ctrl.Width
    else
    begin
      ParentBounds    := GetControlRealPosition(Ctrl.ParentControl);
      Ctrl.Position.X := MaxRight - ParentBounds.Left - Ctrl.Width;
      ParentBounds.Free;
    end;
  end;

  UpdateSelections;
  Changed := True;
end;
{$endregion}

{$region 'Scripter routines'}
procedure TJSONCard.ScripterCreate;
begin
  if Assigned (FScripter) then exit;

  ScripterRelease;

  FScripter := TatScripter.Create(Self);
  FScripter.DefineClassByRTTI(TJSONCard,      roInclude);
  FScripter.DefineClassByRTTI(TJSONLabel,     roInclude);
  FScripter.DefineClassByRTTI(TJSONImage,     roInclude);
  FScripter.DefineClassByRTTI(TCardLine,      roInclude);
  FScripter.DefineClassByRTTI(TCardRectangle, roInclude);
  FScripter.DefineClassByRTTI(TCardRoundRect, roInclude);
  FScripter.DefineClassByRTTI(TCardEllipse,   roInclude);
  FScripter.DefineClassByRTTI(TCardCircle,    roInclude);
  FScripter.DefineClassByRTTI(TCardArc,       roInclude);
  FScripter.DefineRecord(TypeInfo (TAlphaColors), 'TAlphaColors');

  ScripterRegisterComponents;
end;

procedure TJSONCard.ScripterRelease;
begin
  if Assigned (FScripter) then
    FreeAndNil (FScripter);
end;

procedure TJSONCard.ScripterRegisterComponents;
var
  i:    integer;
  Ctrl: TControl;
begin
  Assert (Assigned (FScripter), 'Scripter not created');

  for i := 0 to LstControls.Count - 1 do
  begin
    Ctrl := TControl (LstControls [i]);
    Scripter.AddComponent(Ctrl);
  end;
end;

procedure TJSONCard.ClearJSONFields;
var
  Field: PJSONFieldValue;
  i: integer;
begin
  for i := 0 to FJSONFields.Count - 1 do
  begin
    Field           := PJSONFieldValue (FJSONFields [i]);
    FJSONFields [i] := nil;
    Dispose (Field);
  end;
  FJSONFields.Clear;
end;

function TJSONCard.GetJSONFieldCount: integer;
begin
  result := FJSONFields.Count;
end;

function TJSONCard.IndexOfJSONField (FieldName: string): integer;
var
  i:     integer;
  Field: PJSONFieldValue;
begin
  result    := -1;
  FieldName := FieldName.ToUpper;

  for i := 0 to FJSONFields.Count -1  do
  begin
    Field := PJSONFieldValue (FJSONFields [i]);
    if Field^.FieldName.ToUpper = FieldName then
    begin
      result := i;
      exit;
    end;
  end;
end;

function TJSONCard.GetJSONFieldName (Index: integer): string;
var
  Field: PJSONFieldValue;
begin
  result := '';
  if not Math.InRange (Index, 0, FJSONFields.Count - 1) then exit;

  Field := PJSONFieldValue (FJSONFields [Index]);
  result := Field^.FieldName;
end;

function TJSONCard.GetJSONFieldValueByIndex (Index: integer): string;
var
  Field: PJSONFieldValue;
begin
  result := '';
  if not Math.InRange (Index, 0, FJSONFields.Count - 1) then exit;

  Field  := PJSONFieldValue (FJSONFields [Index]);
  result := Field^.Value;
end;

function TJSONCard.GetJSONFieldValue (FieldName: string): string;
var
  Index: integer;
  Field: PJSONFieldValue;
begin
  result := '';
  Index  := IndexOfJSONField(FieldName);
  if Index = -1 then exit;

  Field  := PJSONFieldValue (FJSONFields [Index]);
  result := Field^.Value;
end;

procedure TJSONCard.AddJSONFieldValue (FieldName, Value: string);
var
  Field: PJSONFieldValue;
begin
  New (Field);
  Field^.FieldName := FieldName;
  Field^.Value     := Value;
  FJSONFields.Add(Field);
end;

{$endregion}

{$endregion}

{$region 'TRESTThread'}
procedure TRESTThread.Initialize;
begin
  FBaseURL        := '';
  FServiceRequest := '';
  FJSONResult     := '';
  FFinished       := False;
end;

destructor TRESTThread.Destroy;
begin
  FBaseURL        := '';
  FServiceRequest := '';
  FJSONResult     := '';

  inherited Destroy;
end;

procedure TRESTThread.Execute;
var
  REST: TRESTJSONServiceClient;

  procedure SafeRESTExecute;
  begin
    //Trick to avoid crash in Android devices
    FJSONResult := REST.Execute;
  end;
begin
  FFinished   := False;
  FJSONResult := '';
  FErrorMsg   := '';
  try
    REST                := TRESTJSONServiceClient.Create (nil);
    REST.BaseURL        := BaseURL;
    REST.ServiceRequest := ServiceRequest;
    try
      {$IFDEF ANDROID}
      SafeRESTExecute;
      {$ELSE}
      FJSONResult := REST.Execute;
      {$ENDIf}
      FErrorMsg := REST.ErrorMsg;
    except
      on E: Exception do
        FErrorMsg := E.Message;
    end;
    FFinished := True;
  finally
    REST.Free;
  end;
end;
{$endregion}

{$region 'TGridCard'}
constructor TGridCard.Create (AOwner: TComponent);
var
  rectMargin: TRectF;
begin
  FCanResize := False;
  inherited Create (AOwner);

  ClipChildren                    := True;
  rectMargin                      := TRectF.Create(5, 5, 5, 5);
  FJSONDataSet                    := TJSONDataSet.Create (Self);
  FRowCount                       := 2;
  FColCount                       := 2;
  FMaxGridCards                   := 0;
  FBaseCard                       := TJSONCard.Create (Self);
  FGridMargin                     := TBounds.Create (rectMargin);
  FPageCount                      := 0;
  FCurrentPage                    := -1;
  FHorizInnerMargin               := 10;
  FVertInnerMargin                := 10;
  FCards                          := TList.Create;
  FOnRecalculeColRows             := nil;
  FOnChangePage                   := nil;
  FOnAnalyzeJSON                  := nil;
  FOnGridError                    := nil;
  FAutoAdjust                     := False;
  FRESTServerAddress              := '';
  FRESTRequest                    := '';
  FRESTInterval                   := 60 * 5; // 5 minutes
  FTransitionType                 := ttNone;
  FTransitionInterval             := 15;     //15 seconds
  FTransitionSpeed                := 500;    //0.5 seconds
  FTransitionSteps                := 25;
  FJSONFileName                   := '';
  FJSONSource                     := '';
  FJSONSourceType                 := jtManual;
  Timer                           := TTimer.Create (Self);
  Timer.OnTimer                   := TimerTimer;
  Timer.Enabled                   := False;
  Timer.Interval                  := 250;
  RESTThread                      := nil;
  FPlaying                        := False;
  LastTransition                  := 0;
  LastRESTRequest                 := EncodeDate(1800, 1, 1);
  FCanResize                      := True;
  FScript                         := '';
  LstPages                        := TList.Create;
  TransitionActive                := False;
  TransitionTimer                 := TTimer.Create(Self);
  TransitionTimer.OnTimer         := TimerTransitionTimer;
  FErrorMsg                       := '';
  {$IFDEF ANDROID}
  FBaseStream                     := nil;
  {$ENDIF}
end;

destructor TGridCard.Destroy;
begin
  TransitionTimer.Enabled := False;
  TransitionTimer.Free;

  Timer.Enabled := False;
  Timer.Free;

  if Assigned (RESTThread) then
  begin
    RESTThread.Terminate;
    Sleep (200);
    RESTThread.WaitFor;
    RESTThread.Free;
  end;

  {$IFDEF ANDROID}
  if Assigned (FBaseStream) then
    FBaseStream.Free;
  {$ENDIF}

  FScript             := '';
  FRESTServerAddress  := '';
  FRESTRequest        := '';
  FJSONFileName       := '';
  FJSONSource         := '';
  FErrorMsg           := '';

  FBaseCard.Free;
  FGridMargin.Free;
  try
    ClearCards;
  except

  end;
  ClearCardPages;
  LstPages.Free;
  FCards.Free;
  inherited Destroy;
end;

{$region 'Scripter routines'}
procedure TGridCard.RegisterControlsAndJSONFields (Card: TJSONCard; UserCodes: TatScript);
var
  i: integer;
  Ctrl: TControl;
begin
  for i := 0 to Card.LstControls.Count - 1 do
  begin
    Ctrl := TControl (Card.LstControls [i]);
    UserCodes.AddObject(Ctrl.Name, Ctrl);
  end;
  UserCodes.AddObject(Card.Name, Card);
  UserCodes.SelfRegisterAsLibrary('UserCodes');
end;

procedure TGridCard.BeforeDisplay (Card: TJSONCard);
var
  UserCodes: TatScript;

  procedure SafeCompileAndExecute;
  begin
    //Trick to avoid crash in Android devices
    Card.Scripter.Compile;
    Card.Scripter.Execute;
  end;
begin
  if not FScript.IsEmpty then
  begin
    UserCodes := nil;
    {$IFDEF ANDROID}
    Card.AssignControlNames;
    {$ENDIF}
    try
      try
        Card.Name                 := 'Card';
        UserCodes                 := Card.Scripter.Scripts.Add;
        UserCodes.SourceCode.Text := FScript;
        RegisterControlsAndJSONFields (Card, UserCodes);
        Card.Scripter.SourceCode.Text := 'Begin BeforeDisplayCard; End;';
        SafeCompileAndExecute;
      except
      end;
    finally
      if Assigned (UserCodes) then
        UserCodes.Free;

      Card.Name := '';
      {$IFDEF ANDROID}
      Card.ClearControlNames;
      {$ENDIF}
    end;
  end;
end;

procedure TGridCard.BeforeObjectDisplay (Card: TJSONCard; Sender: TControl; EventHandler: string);
var
  UserCodes: TatScript;

  procedure SafeCompileAndExecute;
  begin
    //Trick to avoid crash in Android devices
    Card.Scripter.Compile;
    Card.Scripter.Execute;
  end;
begin
  if not FScript.IsEmpty and not EventHandler.IsEmpty then
  begin
    UserCodes := nil;
    {$IFDEF ANDROID}
    Card.AssignControlNames;
    {$ENDIF}
    try
      try
        Card.Name                 := 'Card';
        UserCodes                 := Card.Scripter.Scripts.Add;
        UserCodes.SourceCode.Text := FScript;
        RegisterControlsAndJSONFields(Card, UserCodes);

        Card.Scripter.AddObject('Card', Card);
        Card.Scripter.SourceCode.Text := Format ('Var Ctrl: TControl; Begin Ctrl := Card.GetControlByName(''%s'');  %s(Ctrl); End;', [Sender.Name, EventHandler]);
        SafeCompileAndExecute;
      except
      end;
    finally
      if Assigned (UserCodes) then
        UserCodes.Free;

      Card.Name := '';
      {$IFDEF ANDROID}
      Card.ClearControlNames;
      {$ENDIF}
    end;
  end;
end;

procedure TGridCard.DisplayJSONLabel (Card: TJSONCard; JSONLabelName, JSONField: string; var Text: string);
var
  varText: Variant;
  UserCodes: TatScript;
  Lab: TJSONLabel;
  S: TLabel;

  procedure SafeCompileAndExecute;
  begin
    //Trick to avoid crash in Android devices
    Card.Scripter.Compile;
    Card.Scripter.Execute;
  end;
begin
  if not FScript.IsEmpty then
  begin
    {$IFDEF ANDROID}
    Card.AssignControlNames;
    {$ENDIF}
    varText   := Text;
    Lab       := TJSONLabel (Card.GetControlByName(JSONLabelName));
    UserCodes := nil;
    S         := TLabel.Create (Self);
    S.Text    := Text;
    try
      if not Lab.OnDisplayJSONLabel.IsEmpty then
      try
        Card.Name                 := 'Card';
        UserCodes                 := Card.Scripter.Scripts.Add;
        UserCodes.SourceCode.Text := FScript;
        RegisterControlsAndJSONFields(Card, UserCodes);

        Card.Scripter.SourceCode.Text := Format ('Var MyText: string; Begin MyText := StringObject.Text; %s (Sender, ''%s'', ''%s'', MyText); StringObject.Text := MyText; End;', [Lab.OnDisplayJSONLabel, JSONLabelName, JSONField]);
        Card.Scripter.AddObject('Sender',       Lab);
        Card.Scripter.AddObject('StringObject', S);
        SafeCompileAndExecute;
        Text := S.Text;
      except
      end;
    finally
      if Assigned (UserCodes) then
        UserCodes.Free;

      S.Free;
      Card.Name := '';
      {$IFDEF ANDROID}
      Card.ClearControlNames;
      {$ENDIF}
    end;
  end;
end;
{$endregion}

{$region 'Event handlers'}
procedure TGridCard.RecalculateColRows;
begin
  if Assigned (FOnRecalculeColRows) then
    FOnRecalculeColRows (Self);
end;

procedure TGridCard.Resize;
begin
  inherited;

  if not FCanResize then exit;

  Update;
end;
{$endregion}

{$region 'Properties setters and getters'}
procedure TGridCard.ClearCards;
var
  i: integer;
  c: TJSONCard;
begin
  for i := 0 to FCards.Count - 1 do
  begin
    c          := TJSONCard (FCards [i]);
    FCards [i] := nil;
    c.Free;
  end;
  FCards.Clear;
end;

procedure TGridCard.SetRowCount (Value: integer);
begin
  if FRowCount <> Value then
  begin
    if Value < 0 then
      Exception.Create('Row count must be greater than 0');

    FRowCount := Value;
    Update (False);
  end;
end;

procedure TGridCard.SetColCount (Value: integer);
begin
  if FColCount <> Value then
  begin
    if Value < 0 then
      Exception.Create('Col count must be greater than 0');

    FColCount := Value;
    Update (False);
  end;
end;

procedure TGridCard.SetMaxGridCards (Value: integer);
begin
  if FMaxGridCards <> Value then
  begin
    FMaxGridCards := Value;
    Update (False);
  end;
end;


procedure TGridCard.SetCurrentPage (Value: integer);
begin
  if FCurrentPage <> Value then
  begin
    if not Math.InRange (Value, 0, FPageCount - 1) then
      Exception.Create('Invalid page number');

    MoveToPage(Value);
    FCurrentPage := Value;
  end;
end;

procedure TGridCard.SetHorizInnerMargin (Value: Single);
begin
   if FHorizInnerMargin <> Value then
   begin
     FHorizInnerMargin := Value;
     Update (False);
   end;
end;

procedure TGridCard.SetVertInnerMargin (Value: Single);
begin
  if FVertInnerMargin <> Value then
  begin
     FVertInnerMargin := Value;
     Update (False);
  end;
end;

procedure TGridCard.SetCardVisible (Show: boolean);
var
  i: integer;
begin
  for i := 0 to FCards.Count - 1 do
    TJSONCard (FCards [i]).Visible := Show;
end;

procedure TGridCard.SetAutoAdjust (Value: boolean);
begin
  if FAutoAdjust <> Value then
  begin
    FAutoAdjust := Value;
    Update (False);
  end;
end;

function TGridCard.GetTopMargin: Single;
begin
  result := FGridMargin.Top;
end;

procedure TGridCard.SetTopMargin (Value: Single);
begin
  if FGridMargin.Top <> Value then
  begin
    FGridMargin.Top := Value;
    Update (False);
  end;
end;

function TGridCard.GetLeftMargin: Single;
begin
  result := FGridMargin.Left;
end;

procedure TGridCard.SetLeftMargin (Value: Single);
begin
  if FGridMargin.Left <> Value then
  begin
    FGridMargin.Left := Value;
    Update (False);
  end;
end;

function TGridCard.GetRightMargin: Single;
begin
  result := FGridMargin.Right;
end;

procedure TGridCard.SetRightMargin (Value: Single);
begin
  if FGridMargin.Right <> Value then
  begin
    FGridMargin.Right := Value;
    Update (False);
  end;
end;

function TGridCard.GetBottomMargin: Single;
begin
  result := FGridMargin.Bottom;
end;

procedure TGridCard.SetBottomMargin (Value: Single);
begin
  if FGridMargin.Bottom <> Value then
  begin
    FGridMargin.Bottom := Value;
    Update (False);
  end;
end;

procedure TGridCard.SetTransitionType (Value: TTransitionType);
begin
  if FTransitionType <> Value then
  begin
    FTransitionType := Value;
  end;
end;

procedure TGridCard.SetTransitionInterval (Value: integer);
begin
  if FTransitionInterval <> Value then
  begin
    FTransitionInterval := Value;
  end;
end;

procedure TGridCard.SetTransitionSpeed (Value: integer);
begin
  if FTransitionSpeed <> Value then
  begin
    FTransitionSpeed := Value;
  end;
end;


procedure TGridCard.SetTransitionSteps (Value: integer);
begin
  if FTransitionSteps <> Value then
  begin
    FTransitionSteps := Value;
  end;
end;


procedure TGridCard.SetRESTServerAddress (Value: string);
begin
  if FRESTServerAddress <> Value then
  begin
    FRESTServerAddress := Value;
  end;
end;

procedure TGridCard.SetRESTRequest (Value: string);
begin
  if FRESTRequest <> Value then
  begin
    FRESTRequest := Value;
  end;
end;

procedure TGridCard.SetRESTInterval (Value: integer);
begin
  if FRESTInterval <> Value then
  begin
    FRESTInterval := Value;
  end;
end;

procedure TGridCard.SetJSONFile (Value: string);
begin
  if FJSONFileName <> Value then
  begin
    FJSONFileName := Value;
  end;
end;

procedure TGridCard.SetJSONSourceType (Value: TJSONSourceType);
begin
  if FJSONSourceType <> Value then
  begin
    FJSONSourceType := Value;
    UpdateJSON;
  end;
end;

procedure TGridCard.SetJSONSource (Value: string);
begin
  if FJSONSource <> Value then
  begin
    FJSONSource := Value;
  end;
end;

function TGridCard.GetInternalJSONSource: string;
begin
  result := FJSONDataSet.FJSONSource;
end;

procedure TGridCard.SetInternalJSONSource (Value: string);
var
  RootItem: string;
  DataDepth:   integer;
begin
  if FJSONDataSet.FJSONSource <> Value then
  begin
    if Value.IsEmpty and (FJSONSourceType = jtManual) then
      ErrorMsg := 'Empty JSON source'
    else
    begin
      ErrorMsg := '';
      try
        AnalyzeJSON(Value, RootItem, DataDepth);
        FJSONDataSet.BeginUpdate;
        FJSONDataSet.JSONSource := Value;
        FJSONDataSet.RootPath   := RootItem;
        FJSONDataSet.ScanDepth  := DataDepth;
        FJSONDataSet.EndUpdate;
      except
      end;

      Update(True, True);
      CalculatePages;
    end;
    if Assigned (FOnChangePage) then
      FOnChangePage (Self);
  end;
end;

procedure TGridCard.UpdateJSON;
begin
  case FJSONSourceType of
    jtManual: begin
      InternalJSONSource := FJSONSource;
      if FJSONSource.IsEmpty then
         ErrorMsg := 'JSON source is empty';
    end;
    jtRESTService: begin
      if not Assigned (RESTThread) and not FRESTServerAddress.IsEmpty and not FRESTRequest.IsEmpty and (FRESTInterval > 0) then
        CreateThread;
    end;
    jtFile: begin
      if not FJSONFileName.IsEmpty and FileExists (FJSONFileName) then
        InternalJSONSource := TFile.ReadAllText(FJSONFileName)
      else
      begin
        InternalJSONSource := '';

        if FJSONFileName.IsEmpty then
          ErrorMsg := 'JSON file name is empty'
        else
          ErrorMsg := Format ('JSON file name [%s] not exists', [FJSONFileName]);
      end;
    end;
  end;
end;

procedure TGridCard.AnalyzeJSON (JSON: string; var RootItem: string; var DataDepth: integer);
begin
  if Assigned (FOnAnalyzeJSON) then
    FOnAnalyzeJSON (Self, JSON, RootItem, DataDepth)
  else
  begin
    RootItem  := '';
    DataDepth := 1;
  end;
end;

{function TGridCard.GetErrorMsg: string;
begin
  result := lblError.Text;
end;

procedure TGridCard.SetErrorMsg (Value: string);
begin
  if lblError.Text <> Value then
  begin
    lblError.Visible := False;
    lblError.Text    := Value;

    if Value.IsEmpty then
    begin
      lblError.Opacity := 0;
    end
    else
    begin
      SetCardVisible(False);
      lblError.Opacity := 1;
    end;
  end;
  lblError.Visible := not Value.IsEmpty;
end;}

procedure TGridCard.SetErrorMsg (Value: string);
begin
  if FErrorMsg <> Value then
  begin
    FErrorMsg := Value;

    if not FErrorMsg.IsEmpty then
      SetCardVisible(False);

    if Assigned (FOnGridError) then
      FOnGridError (Self, FErrorMsg);
  end;
end;
{$endregion}

{$region 'File and stream routines'}
procedure TGridCard.LoadTemplate (FileName: string);
var
  Stream: TFileStream;
begin
  FBaseCard.Clear;

  if not FileExists (FileName) then
  begin
    ErrorMsg := Format ('Card file [%s] not found', [FileName]);
    exit;
  end;

  FBaseCard.LoadFromFile(FileName);
  {$IFDEF ANDROID}
  if Assigned (FBaseStream) then
    FBaseStream.Free;

  FBaseStream := TMemoryStream.Create;
  Stream      := TFileStream.Create(FileName, fmOpenRead);
  try
    Stream.Position := 0;
    FBaseStream.CopyFrom(Stream, Stream.Size);
    FBaseStream.Position := 0;
  finally
    Stream.Free;
  end;
  {$ELSE}
  Update;
  {$ENDIF}
end;

procedure TGridCard.LoadTemplate (Stream: TStream);
begin
  FBaseCard.LoadFromStream(Stream);
  Update;
end;
{$endregion}

{$region 'Display routines'}
procedure TGridCard.Update (CardChanges: boolean = True; JSONOrPasChanges: boolean = True);
var
  i, j:      integer;
  c:         TJSONCard;
  mem:       TMemoryStream;
  Ctrl:      TControl;
  Lab:       TJSONLabel;
  Img:       TJSONImage;
  s,
  strRow:    string;
  Obj,
  ObjRow:    TJSONObject;
  JSONRows,
  ImgArr:    TJSONArray;
  Stream:    TStream;
  ValidJSON: boolean;
  Ctx:       TRttiContext;
  ObjType:   TRttiType;
  Prop:      TRttiProperty;
begin
  if FUpdating > 0 then exit;

  BeginUpdate;
  try
    SetCardVisible(False);

    if CardChanges or JSONOrPasChanges then
      ClearCards;

    if not FJSONDataSet.MemTable.Active then exit;

    if CardChanges or (FJSONDataSet.RecordCount <> FCards.Count) then
    begin
      //Adjust the card list to the JSON records
      if not FJSONDataSet.MemTable.Active or (FJSONDataSet.RecordCount = 0) then
      begin
        ClearCards;
        FPageCount   := 0;
        FCurrentPage := -1;
        exit;
      end
      else if FJSONDataSet.RecordCount > FCards.Count then
      begin
        for i := FCards.Count to FJSONDataSet.RecordCount - 1 do
        begin
          c         := TJSONCard.Create(Self);
          c.Visible := False;
          FCards.Add(c);
        end;
      end
      else if FJSONDataSet.RecordCount < FCards.Count then
      begin
        while FJSONDataSet.RecordCount < FCards.Count do
        begin
          c                         := TJSONCard (FCards [FCards.Count - 1]);
          FCards [FCards.Count - 1] := nil;
          FCards.Delete(FCards.Count - 1);
          c.Free;
        end;
      end;

      //Assign card template to all cards
      mem := TMemoryStream.Create;
      try
        {$IFDEF ANDROID}
        if Assigned (FBaseStream) then
        begin
          FBaseStream.Position := 0;
          mem.CopyFrom(FBaseStream, FBaseStream.Size);
        end;
        {$ELSE}
        FBaseCard.SaveToStream(mem);
        {$ENDIF}
        for i := 0 to FCards.Count - 1 do
        begin
          c            := TJSONCard (FCards [i]);
          c.Parent     := Self;
          mem.Position := 0;
          if mem.Size > 0 then
            c.LoadFromStream(mem);
          c.ScripterCreate;
        end;
      finally
        mem.Free;
      end;
    end;

    if CardChanges or JSONOrPasChanges then
    begin
      //Get JSON fields
      Obj       := nil;
      ValidJSON := False;
      JSONRows  := nil;

      if FJSONDataSet.RootPath.IsEmpty then
      begin
        try
          Obj       := TJSONObject.ParseJSONValue(JSONDataSet.JSONSource) as TJSONObject;
          JSONRows  := TJSONArray.Create;
          JSONRows.AddElement(Obj);
          ValidJSON := True;
        except
          ValidJSON := False;
        end;
      end
      else
      begin
        try
          Obj       := TJSONObject.ParseJSONValue(JSONDataSet.JSONSource) as TJSONObject;
          JSONRows  := Obj.Get(FJSONDataSet.RootPath).JsonValue as TJSONArray;
          ValidJSON := True;
        except
          ValidJSON := False;
        end;
      end;

      try
        FJSONDataSet.MemTable.First;
        while not FJSONDataSet.MemTable.Eof do
        begin
          strRow := TJSONObject (JSONRows.Get(FJSONDataSet.MemTable.RecNo - 1)).ToJSON;
          c      := TJSONCard (FCards [FJSONDataSet.MemTable.RecNo - 1]);
          c.ClearJSONFields;

          for i := 0 to FJSONDataSet.MemTable.FieldCount - 1 do
            c.AddJSONFieldValue(FJSONDataSet.MemTable.Fields [i].FieldName, FJSONDataSet.MemTable.Fields [i].AsString);

          for j := 0 to c.LstControls.Count - 1 do
          begin
            Ctrl := TControl (c.LstControls [j]);
            if Ctrl is TJSONLabel then
            begin
              Lab := TJSONLabel (Ctrl);
              if not Lab.JSONLink.JSONField.IsEmpty then
              begin
                s := '';
                if FJSONDataSet.MemTable.FindField (Lab.JSONLink.JSONField) <> nil then
                  s := FJSONDataSet.MemTable.FieldByName(Lab.JSONLink.JSONField).AsString;

                {$IFDEF RTML}
                if not Lab.ParseToRTML then
                  s := Lab.LabelFormat.ApplyFormat(s);
                {$ENDIF}

                if not Lab.OnDisplayJSONLabel.IsEmpty then
                begin
                  {$IFDEF ANDROID}
                  DisplayJSONLabel(c, c.CtrlNames [j], Lab.JSONLink.JSONField, s);
                  {$ELSE}
                  DisplayJSONLabel(c, Lab.Name, Lab.JSONLink.JSONField, s);
                  {$ENDIF}
                end;
                Lab.Text := s;
              end
              else {$IFDEF RTML} if not Lab.ParseToRTML then {$ENDIF}
                Lab.Text := Lab.LabelFormat.ApplyFormat(Lab.Text);

              if {$IFDEF RTML}not Lab.ParseToRTML and{$ENDIF} (lbHideIfEmpty in Lab.LabelFormat.LabelBehavior) and (Lab.Text = '') then
                Lab.Visible := False;
            end
            else if Ctrl is TJSONImage then
            begin
              Img := TJSONImage (Ctrl);
              if not Img.JSONLink.JSONField.IsEmpty then
              begin
                s := '';
                if FJSONDataSet.MemTable.FindField (Img.JSONLink.JSONField) <> nil then
                  s := FJSONDataSet.MemTable.FieldByName(Img.JSONLink.JSONField).AsString;

                if s.IsEmpty then
                  Img.Visible := False
                else
                begin
                  //JSON field contains a binary image
                  if ValidJSON and (Pos ('[', s) <> 0) then
                  begin
                    //In the case of an array, it is necessary to obtain it directly from JSON, because the dataset
                    //loses most of the content of the array
                    ObjRow := TJSONObject.ParseJSONValue(strRow) as TJSONObject;
                    ObjRow.TryGetValue<TJSONArray>(Img.JSONLink.JSONField, ImgArr);
                    if ImgArr.Count > 0 then
                    begin
                      Stream := TDBXJSONTools.JSONToStream(ImgArr);
                      try
                        Stream.Position := 0;
                        Img.Bitmap.LoadFromStream(Stream);
                      finally
                        Stream.Free;
                      end;
                    end;
                  end
                  else if FileExists (s) then
                  //JSON field contains an image file
                    Img.Bitmap.LoadFromFile(s);
                end;
              end;
            end;
          end;

          FJSONDataSet.MemTable.Next;
        end;
      finally
        if Assigned (Obj) then
          Obj.Free;
      end;
    end;

    if FAutoAdjust then
    begin
      CalculateGridDimensions (FRowCount, FColCount);

  {    FGridMargin.Left   := (Width  - ((FColCount * FBaseCard.Width)  + ((FColCount - 1) * FHorizInnerMargin))) / 2;
      FGridMargin.Top    := (Height - ((FRowCount * FBaseCard.Height) + ((FRowCount - 1) * FVertInnerMargin)))  / 2;
      FGridMargin.Right  := FGridMargin.Left;
      FGridMargin.Bottom := FGridMargin.Top;}
    end
    else
    begin
      //Calculate page number and grid dimension
//      Width      := (FBaseCard.Width  * FColCount) + (FHorizInnerMargin * (FColCount - 1)) + FGridMargin.Left + FGridMargin.Right;
//      Height     := (FBaseCard.Height * FRowCount) + (FVertInnerMargin  * (FRowCount - 1)) + FGridMargin.Top  + FGridMargin.Bottom;
    end;
    FPageCount   := Math.Max (1, Ceil (FJSONDataSet.RecordCount / (FRowCount * FColCount)));
    FCurrentPage := 0;
    CalculatePages;

    Ctx := TRttiContext.Create;
    try
      for i := 0 to FCards.Count - 1 do
      begin
        c := TJSONCard (FCards [i]);
        BeforeDisplay(c);

        for j := 0 to c.LstControls.Count - 1 do
        begin
          Ctrl    := TControl (c.LstControls [j]);
          objType := Ctx.GetType(Ctrl.ClassInfo);
          Prop    := objType.GetProperty('OnBeforeObjectDisplay');

          BeforeObjectDisplay(c, Ctrl, c.GetPropText(Ctrl, Prop));
        end;
      end;
    finally
      Ctx.Free;
    end;

    case TransitionType of
      ttNone:          PlacePageCards (FCurrentPage);
      ttZoomInOut:     ZoomInOut (FCurrentPage, True);
      ttScrollFullRow: RowByRow  (FCurrentPage, True);
      ttFadeInOut:     FadeInOut (FCurrentPage, True);
    end;

  finally
    Repaint;
    EndUpdate;
    ParentControl.Repaint;

    {$IFDEF RTML}
    for i := 0 to FCards.Count - 1 do
    begin
      c := TJSONCard (FCards [i]);
      c.Background.Repaint;
      for j := 0 to c.LstControls.Count - 1 do
      begin
        Ctrl := TControl (c.LstControls [j]);
        if Ctrl is TJSONLabel then
          TJSONLabel (Ctrl).RepaintLabel;
      end;
    end;
    {$ENDIF}

    RecalculateColRows;
  end;
end;

procedure TGridCard.ClearCardPages;
var
  i, j:     integer;
  CardInfo: PCardPageInfo;
  PageInfo: PPageInfo;
  RowInfo:  PRowInfo;
begin
  for i := 0 to LstPages.Count - 1 do
  begin
    PageInfo     := PPageInfo (LstPages [i]);
    LstPages [i] := nil;

    for j := 0 to PageInfo^.Cards.Count - 1 do
    begin
      CardInfo            := PCardPageInfo (PageInfo^.Cards [j]);
      PageInfo^.Cards [j] := nil;
      Dispose (CardInfo);
    end;
    PageInfo^.Cards.Clear;
    PageInfo^.Cards.Free;

    for j := 0 to PageInfo^.Rows.Count - 1 do
    begin
      RowInfo            := PRowInfo (PageInfo^.Rows [j]);
      PageInfo^.Rows [j] := nil;
      Dispose (RowInfo);
    end;
    PageInfo^.Rows.Clear;
    PageInfo^.Rows.Free;

    Dispose (PageInfo);
  end;
  LstPages.Clear;
end;

procedure TGridCard.CalculateGridDimensions (var ARowCount, AColCount: integer);
var
  PhysicMaxCols, PhysicMaxRows, PhysicMaxCards, MaxUserCards: integer;
  CurrCol, CurrRow, ValidCols, ValidRows: integer;
  ColCount, RowCount: integer;
  Change: boolean;
begin
  PhysicMaxCols  := Math.Max (1, Trunc ((Width  - FGridMargin.Left) / FBaseCard.Width));
  PhysicMaxRows  := Math.Max (1, Trunc ((Height - FGridMargin.Top)  / FBaseCard.Height));
  PhysicMaxCards := PhysicMaxCols * PhysicMaxRows;

  if FMaxGridCards > 0 then
    MaxUserCards := Math.Min (FMaxGridCards, FCards.Count)
  else
    MaxUserCards := FCards.Count;

  if ((FMaxGridCards > 0) and ((PhysicMaxCards > FMaxGridCards) or (FMaxGridCards > FCards.Count))) or (PhysicMaxCards > FCards.Count) then
  begin
    CurrCol   := 1;
    CurrRow   := 1;
    ValidCols := CurrCol;
    ValidRows := CurrRow;

    repeat
      Change := False;
      if (CurrCol > CurrRow) and (CurrRow < PhysicMaxRows) then
      begin
        Inc (CurrRow);
        Change := True;
      end
      else if CurrCol < PhysicMaxCols then
      begin
        Inc (CurrCol);
        Change := True;
      end
      else if CurrRow < PhysicMaxRows then
      begin
        Inc (CurrRow);
        Change := True;
      end;

      if Change and ((CurrCol * CurrRow) <= MaxUserCards) then
      begin
        ValidCols := CurrCol;
        ValidRows := CurrRow;
      end;

    until not Change or ((CurrCol * CurrRow) >= MaxUserCards);
    AColCount := ValidCols;
    ARowCount := ValidRows;
  end
  else
  begin
    AColCount := PhysicMaxCols;
    ARowCount := PhysicMaxRows;
  end;
end;

procedure TGridCard.CalculatePages;
var
  i, NPage, NCard, Col, Row: integer;
  X, Y: Single;
  CardInfo: PCardPageInfo;
  PageInfo: PPageInfo;
  RowInfo:  PRowInfo;

  InnerX, InnerY: Single;
begin
  ClearCardPages;

  InnerX  := ((Width  - FGridMargin.Left) - (FColCount * FBaseCard.Width))  / (FColCount + 1);
  InnerY  := ((Height - FGridMargin.Top)  - (FRowCount * FBaseCard.Height)) / (FRowCount + 1);
  NPage   := -1;
  NCard   := 0;
  Row     := 0;
  Col     := 0;
  X       := FGridMargin.Left;
  Y       := FGridMargin.Top;
  RowInfo := nil;
  for i := 0 to FCards.Count - 1 do
  begin
    if (NPage = -1) or (NCard >= ((FRowCount * FColCount) - 1)) then
    begin
      Inc (NPage);
      New (PageInfo);
      LstPages.Add(PageInfo);
      NCard               := 0;
      Row                 := 0;
      Col                 := 0;
      X                   := FGridMargin.Left + InnerX;
      Y                   := FGridMargin.Top  + InnerY;
      PageInfo^.Cards     := TList.Create;
      PageInfo^.PageIndex := NPage;
      PageInfo^.First     := i;
      PageInfo^.Rows      := TList.Create;
    end
    else
      Inc (NCard);

    if not Assigned (RowInfo) then
    begin
      New (RowInfo);
      PageInfo^.Rows.Add(RowInfo);
      RowInfo^.Row   := Row;
      RowInfo^.First := i;
    end;

    RowInfo^.Last  := i;
    PageInfo^.Last := i;
    New (CardInfo);
    PageInfo^.Cards.Add(CardInfo);
    CardInfo^.CardIndex := i;
    CardInfo^.Left      := X;
    CardInfo^.Top       := Y;
    CardInfo^.Row       := Row;
    CardInfo^.Col       := Col;
    X                   := X + FBaseCard.Width + InnerX; //FHorizInnerMargin;

    Inc (Col);
    if Col >= FColCount then
    begin
      Inc (Row);
      Col     := 0;
      X       := FGridMargin.Left + InnerX;
      Y       := Y + FBaseCard.Height + InnerY; // FVertInnerMargin;
      RowInfo := nil;
    end;
  end;
end;

procedure TGridCard.PlacePageCards (PageNumber: integer);
var
  i:        integer;
  Card:     TJSONCard;
  CardInfo: PCardPageInfo;
  PageInfo: PPageInfo;
begin
  if not Math.InRange (PageNumber, 0, FPageCount - 1) then exit;

  SetCardVisible(False);

  FCurrentPage := PageNumber;
  PageInfo     := PPageInfo (LstPages [PageNumber]);
  for i := 0 to PageInfo.Cards.Count - 1 do
  begin
    CardInfo        := PCardPageInfo (PageInfo.Cards [i]);
    Card            := TJSONCard (FCards [CardInfo^.CardIndex]);
    Card.Position.X := CardInfo.Left;
    Card.Position.Y := CardInfo.Top;
    Card.Visible    := True;
    Card.Opacity    := FBaseCard.Opacity;
    Card.Scale.X    := 1;
    Card.Scale.Y    := 1;
    Card.RepaintControls;
  end;

  if Assigned (FOnChangePage) then
    FOnChangePage (Self);
end;

procedure TGridCard.MoveToPage (NewPage: integer);
begin
  if NewPage <> CurrentPage then
    case TransitionType of
      ttNone:          PlacePageCards (NewPage);
      ttZoomInOut:     ZoomInOut (NewPage);
      ttScrollFullRow: RowByRow  (NewPage);
      ttFadeInOut:     FadeInOut (NewPage);
    end;
end;

procedure TGridCard.RestoreAllPageProperties (APosition, AScale, AOpacity: boolean);
var
  p, c:     integer;
  Page:     PPageInfo;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
begin
  for p := 0 to LstPages.Count - 1 do
  begin
    Page := PPageInfo (LstPages [p]);
    for c := 0 to Page^.Cards.Count - 1 do
    begin
      CardInfo := PCardPageInfo (Page^.Cards [c]);
      Card     := TJSONCard (FCards [CardInfo^.CardIndex]);

      if AScale then
      begin
        Card.Scale.X := 1;
        Card.Scale.Y := 1;
      end;

      if APosition then
      begin
        Card.Position.X := CardInfo^.Left;
        Card.Position.Y := CardInfo^.Top;
      end;

      if AOpacity then
        Card.Opacity := FBaseCard.Opacity;;
    end;
  end;
end;

procedure TGridCard.SetVisiblePage (PageNumber: integer);
var
  p, c:     integer;
  Page:     PPageInfo;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
begin
  for p := 0 to LstPages.Count - 1 do
  begin
    Page := PPageInfo (LstPages [p]);
    for c := 0 to Page^.Cards.Count - 1 do
    begin
      CardInfo     := PCardPageInfo (Page^.Cards [c]);
      Card         := TJSONCard (FCards [CardInfo^.CardIndex]);
      Card.Visible := PageNumber = p;
    end;
  end;
end;

procedure TGridCard.FadeInOut (PageNumber: integer; FirstTime: boolean = False);
var
  MaxSteps: Integer;
begin
  MaxSteps := FTransitionSteps ;
  TransitionTimer.Enabled := False;
  if not ErrorMsg.IsEmpty then exit;

  TransitionCurrPage       := -1;
  TransitionActive         := True;
  TransitionStageSteps     := MaxSteps;
  TransitionNextPage       := PageNumber;
  TransitionStep           := 0;
  TransitionStepMSecs      := Math.Max (10, Round (FTransitionSpeed /  (MaxSteps * 2) ));
  TransitionTimer.Interval := TransitionStepMSecs;
  RestoreAllPageProperties (True, True, False);


  if FirstTime then
  begin
        TransitionStage := 2   ;
        //ShowMessage( TransitionStageSteps.ToString)  ;
  end

  else
  begin
    SetVisiblePage (CurrentPage);
    TransitionCurrPage := CurrentPage;
    TransitionStage    := 1;
  end;

  LastTransition          := Now;
  TransitionTimer.Enabled := True;
end;

procedure TGridCard.FadeInOutStep;
var
  Page:     PPageInfo;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
  i:        integer;
begin
  if LstPages.Count = 0 then exit;

  if TransitionStage = 1 then
  begin
    Page := PPageInfo (LstPages [TransitionCurrPage]);
    for i := 0 to Page.Cards.Count - 1 do
    begin
      CardInfo     := PCardPageInfo (Page.Cards [i]);
      Card         := TJSONCard (FCards [CardInfo^.CardIndex]);
      Card.Visible := True;
      Card.Opacity := ((TransitionStageSteps - TransitionStep) / TransitionStageSteps) * FBaseCard.Opacity;
      Card.BringToFront;
      Card.RepaintControls;
    end;
    SetVisiblePage (TransitionCurrPage);
    Inc (TransitionStep);
    if TransitionStep > TransitionStageSteps then
    begin
      Sleep (100);
      TransitionStage := 2;
      TransitionStep  := 0;
    end;
  end
  else
  begin
    Page := PPageInfo (LstPages [TransitionNextPage]);
    for i := 0 to Page.Cards.Count - 1 do
    begin
      CardInfo     := PCardPageInfo (Page.Cards [i]);
      Card         := TJSONCard (FCards [CardInfo^.CardIndex]);
      Card.Visible := True;
      Card.Opacity := (TransitionStep / TransitionStageSteps) * FBaseCard.Opacity;;
      Card.BringToFront;
      Card.RepaintControls;
    end;
    SetVisiblePage (TransitionNextPage);
    Inc (TransitionStep);
    TransitionActive := TransitionStep <= TransitionStageSteps;
    if not TransitionActive then
    begin
      FCurrentPage := TransitionNextPage;
      if Assigned (FOnChangePage) then
        FOnChangePage (Self);
    end;
  end;
  Repaint;
  Application.ProcessMessages;
end;

procedure TGridCard.ZoomInOut (PageNumber: integer; FirstTime: boolean = False);
 var
  MaxSteps: Integer;
begin
  MaxSteps := FTransitionSteps ;

  TransitionTimer.Enabled := False;
  if not ErrorMsg.IsEmpty then exit;

  TransitionCurrPage       := -1;
  TransitionActive         := True;
  TransitionStageSteps     := MaxSteps;
  TransitionNextPage       := PageNumber;
  TransitionStep           := 0;
  TransitionStepMSecs      := Math.Max (10, Round (FTransitionSpeed / (MaxSteps * 2)));
  TransitionTimer.Interval := TransitionStepMSecs;
  RestoreAllPageProperties (True, True, True);

  if FirstTime then
    TransitionStage := 2
  else
  begin
    SetVisiblePage (CurrentPage);
    TransitionCurrPage := CurrentPage;
    TransitionStage    := 1;
  end;

  LastTransition          := Now;
  TransitionTimer.Enabled := True;
end;

procedure TGridCard.ZoomInOutStep;
var
  Page:     PPageInfo;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
  i:        integer;
begin
  if LstPages.Count = 0 then exit;

  if TransitionStage = 1 then
  begin
    Page := PPageInfo (LstPages [TransitionCurrPage]);
    for i := 0 to Page.Cards.Count - 1 do
    begin
      CardInfo        := PCardPageInfo (Page.Cards [i]);
      Card            := TJSONCard (FCards [CardInfo^.CardIndex]);
      Card.Visible    := True;
      Card.Scale.X    := (TransitionStageSteps - TransitionStep) / TransitionStageSteps;
      Card.Scale.Y    := (TransitionStageSteps - TransitionStep) / TransitionStageSteps;
      Card.Position.X := CardInfo^.Left + ((FBaseCard.Width  - Card.Width  * Card.Scale.X) / 2);
      Card.Position.Y := CardInfo^.Top  + ((FBaseCard.Height - Card.Height * Card.Scale.Y) / 2);
      Card.BringToFront;
      Card.RepaintControls;
    end;
    SetVisiblePage (TransitionCurrPage);
    Inc (TransitionStep);
    if TransitionStep > TransitionStageSteps then
    begin
      Sleep (100);
      TransitionStage := 2;
      TransitionStep  := 0;
    end;
  end
  else
  begin
    Page := PPageInfo (LstPages [TransitionNextPage]);
    for i := 0 to Page.Cards.Count - 1 do
    begin
      CardInfo        := PCardPageInfo (Page.Cards [i]);
      Card            := TJSONCard (FCards [CardInfo^.CardIndex]);
      Card.Visible    := True;
      Card.Scale.X    := TransitionStep / TransitionStageSteps;
      Card.Scale.Y    := TransitionStep / TransitionStageSteps;
      Card.Position.X := CardInfo^.Left + ((FBaseCard.Width  - Card.Width  * Card.Scale.X) / 2);
      Card.Position.Y := CardInfo^.Top  + ((FBaseCard.Height - Card.Height * Card.Scale.Y) / 2);
      Card.BringToFront;
      Card.RepaintControls;
  end;
    SetVisiblePage (TransitionNextPage);
    Inc (TransitionStep);
    TransitionActive := TransitionStep <= TransitionStageSteps;
    if not TransitionActive then
    begin
      FCurrentPage := TransitionNextPage;
      if Assigned (FOnChangePage) then
        FOnChangePage (Self);
    end;
  end;
  Repaint;
  Application.ProcessMessages;
end;

procedure TGridCard.RowByRow (PageNumber: integer; FirstTime: boolean = False);
const
  MaxSteps = 100;
var
  p, c, r, i: integer;
  Page:       PPageInfo;
  CardInfo:   PCardPageInfo;
  Card:       TJSONCard;
begin
  if not ErrorMsg.IsEmpty then exit;

  RestoreAllPageProperties (True, True, True);
  for p := 0 to LstPages.Count - 1 do
  begin
    Page := PPageInfo (LstPages [p]);
    for c := 0 to Page^.Cards.Count - 1 do
    begin
      CardInfo        := PCardPageInfo (Page^.Cards [c]);
      Card            := FCards [CardInfo^.CardIndex];
      Card.Position.Y := CardInfo^.Top;
      if not (p in [PageNumber, CurrentPage]) then
        Card.Visible := False;
    end;
  end;

  if not FirstTime then
  begin
    Page := PPageInfo (LstPages [CurrentPage]);

    for r := 0 to Page^.Rows.Count - 1 do
    begin
      for i := 0 to Page^.Cards.Count - 1 do
      begin
        CardInfo := PCardPageInfo (Page^.Cards [i]);
        if CardInfo^.Row = r then
        begin
          Card            := FCards [CardInfo^.CardIndex];
          CardInfo^.Left2 := Width + CardInfo^.Left;
          Card.Visible    := True;
        end;
      end;
    end;
  end;

  BeginUpdate;
  try
    Page := PPageInfo (LstPages [PageNumber]);
    for r := 0 to Page^.Rows.Count - 1 do
    begin
      for i := 0 to Page^.Cards.Count - 1 do
      begin
        CardInfo := PCardPageInfo (Page^.Cards [i]);
        if CardInfo^.Row = r then
        begin
          Card            := FCards [CardInfo^.CardIndex];
          CardInfo^.Left2 := CardInfo^.Left - Width;
          Card.Position.X := CardInfo^.Left2;
          Card.Visible    := True;
          Card.BringToFront;
        end;
      end;
    end;
  finally
    EndUpdate;
  end;

  TransitionTimer.Enabled  := False;
  TransitionCurrPage       := -1;
  TransitionActive         := True;
  TransitionStageSteps     := MaxSteps;
  TransitionNextPage       := PageNumber;
  TransitionStep           := 0;
  TransitionStepMSecs      := Math.Max (10, Round (FTransitionSpeed / (MaxSteps * 2)));
  TransitionTimer.Interval := TransitionStepMSecs;
  TransitionRow            := 0;
  TransitionStage          := 1;

  if not FirstTime then
    TransitionCurrPage := CurrentPage;

  Application.ProcessMessages;
  LastTransition          := Now;
  TransitionTimer.Enabled := True;
end;

procedure TGridCard.RowByRowStep;
const
  LenStep  = 30;
var
  p, c:     integer;
  Page:     PPageInfo;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
begin
  if LstPages.Count = 0 then exit;

  TransitionRowChange := False;

  if TransitionCurrPage <> -1 then
  begin
    Page := PPageInfo (LstPages [TransitionCurrPage]);
    for p := 0 to Page^.Cards.Count - 1 do
    begin
      CardInfo := PCardPageInfo (Page^.Cards [p]);
      Card     := FCards [CardInfo^.CardIndex];
      if (CardInfo^.Row = TransitionRow) and (Card.Position.X < CardInfo^.Left2) then
      begin
        Card.Position.X     := Math.Min(Card.Position.X + LenStep, CardInfo^.Left2);
        TransitionRowChange := True;
        Card.RepaintControls;
      end;
    end;
  end;

  Page := PPageInfo (LstPages [TransitionNextPage]);
  for c := 0 to Page^.Cards.Count - 1 do
  begin
    CardInfo := PCardPageInfo (Page^.Cards [c]);
    Card     := FCards [CardInfo^.CardIndex];
    if (CardInfo^.Row = TransitionRow) and (Card.Position.X < CardInfo^.Left) then
    begin
      Card.Position.X     := Math.Min(Card.Position.X + LenStep, CardInfo^.Left);
      TransitionRowChange := True;
      Card.RepaintControls;
    end;
  end;
  Repaint;
  Application.ProcessMessages;

  if not TransitionRowChange then
  begin
    Inc (TransitionRow);

    if TransitionRow >= FRowCount then
    begin
      TransitionActive := False;
      FCurrentPage     := TransitionNextPage;
      if Assigned (FOnChangePage) then
        FOnChangePage (Self);
    end;
  end;
end;

procedure TGridCard.TimerTransitionTimer (Sender: TObject);
begin
  TransitionTimer.Enabled := False;
  try
    case TransitionType of
      ttZoomInOut:     ZoomInOutStep;
      ttScrollFullRow: RowByRowStep;
      ttFadeInOut:     FadeInOutStep;
    end;

  finally
    TransitionTimer.Enabled := TransitionActive;
  end;
end;

procedure TGridCard.Start;
var
  s:         string;
  RootItem:  string;
  DataDepth: integer;
begin
  while FUpdating > 0 do
    EndUpdate;

  if FPlaying then exit;

  if FJSONSourceType = jtFile then
  begin
    if not FJSONFileName.IsEmpty and FileExists (FJSONFileName) then
      InternalJSONSource := TFile.ReadAllText(FJSONFileName)
    else
      if FJSONFileName.IsEmpty then
        ErrorMsg := 'JSON file name is required, it is empty'
      else
        ErrorMsg := Format ('File [%s] not found', [JSONFileName]);
  end;

  FPlaying       := True;
  LastTransition := 0;
  Timer.Enabled  := True;
end;

procedure TGridCard.Stop;
begin
  if not FPlaying then exit;

  FPlaying                := False;
  Timer.Enabled           := False;
  TransitionTimer.Enabled := False;
end;

procedure TGridCard.Step (Up: boolean);
var
  i:        integer;
  CardInfo: PCardPageInfo;
  Card:     TJSONCard;
  Page:     PPageInfo;
  Sign:     integer;
begin
  if Up then
    Sign := 1
  else
    Sign := -1;

  Page := PPageInfo (LstPages [CurrentPage]);
  for i := 0 to Page^.Cards.Count - 1 do
  begin
    CardInfo := PCardPageInfo (Page^.Cards [i]);
    Card     := TJSONCard (FCards [CardInfo^.CardIndex]);

    case FTransitionType of
      ttZoomInOut: begin
        if Up then
        begin
          Card.Scale.X := Math.Min (Card.Scale.X + 0.1, 1);
          Card.Scale.Y := Math.Min (Card.Scale.Y + 0.1, 1);
        end
        else
        begin
          Card.Scale.X := Math.Max (Card.Scale.X - 0.1, 0);
          Card.Scale.Y := Math.Max (Card.Scale.Y - 0.1, 0);
        end;
      end;
      ttScrollFullRow: begin
        if Up then
          Card.Position.X := Math.Min(Card.Position.X + 20, Width + Card.Width)
        else
          Card.Position.X := Math.Max(Card.Position.X - 20, -Card.Width)
      end;
      ttFadeInOut: begin
        if Up then
          Card.Opacity := Math.Min(Card.Opacity + 0.1, 1)
        else
          Card.Opacity := Math.Max(Card.Opacity - 0.1, 0)
      end;
    end;
  end;
end;

procedure TGridCard.CreateThread;
begin
  LastRESTRequest           := Now;
  RESTThread                := TRESTThread.Create (True);
  RESTThread.BaseURL        := RESTServerAddress;
  RESTThread.ServiceRequest := RESTRequest;
  RESTThread.Resume;
end;

procedure TGridCard.TimerTimer (Sender: TObject);
var
  CurrPage, NextPage, DataDepth: integer;
  RootItem: string;
begin
  Timer.Enabled := False;

  try
    if (FJSONSourceType <> jtRESTService) and Assigned (RESTThread) and RESTThread.Finished then
      FreeAndNil (RESTThread);

    if Assigned (RESTThread) and RESTThread.Finished then
    begin
      if RESTThread.ErrorMsg.IsEmpty and not RESTThread.JSONResult.IsEmpty then
      begin
        ErrorMsg           := '';
        InternalJSONSource := RESTThread.JSONResult;
      end
      else
      begin
        InternalJSONSource := '';
        if not RESTThread.ErrorMsg.IsEmpty then
          ErrorMsg := RESTThread.ErrorMsg
        else
          ErrorMsg := 'JSON source empty';
      end;
      FreeAndNil(RESTThread);
    end;

    if (FJSONSourceType = jtRESTService) and not RESTServerAddress.IsEmpty and not RESTRequest.IsEmpty and not Assigned (RESTThread) and (SecondsBetween(Now, LastRESTRequest) >= FRESTInterval) then
      CreateThread;

    if (FJSONSourceType = jtManual) and (FJSONSource <> FJSONDataSet.JSONSource) then
      InternalJSONSource := FJSONSource;

    if (PageCount = 0) or TransitionActive or FJSONDataSet.JSONSource.IsEmpty {or not ErrorMsg.IsEmpty} then exit;

    if LastTransition = 0 then
      LastTransition := Now;

    if SecondsBetween (Now, LastTransition) >= TransitionInterval then
    begin
      LastTransition := Now;
      CurrPage       := CurrentPage;
      NextPage       := (CurrPage + 1) mod PageCount;

      case TransitionType of
        ttNone:          MoveToPage (NextPage);
        ttZoomInOut:     ZoomInOut (NextPage);
        ttScrollFullRow: RowByRow  (NextPage);
        ttFadeInOut:     FadeInOut (NextPage);
      end;

    end;
  finally
    Timer.Enabled := True;
  end;
end;

procedure TGridCard.BaseCardNotFound (Sender: TObject; FileName: string);
begin
  ErrorMsg := Format ('Card file [%s] not found', [FileName]);
end;


{$endregion}

{$endregion}


{$region 'TProyectSettings'}
constructor TProyectSettings.Create (AOwner: TComponent);
var
  rectMargin: TRectF;
begin
  inherited Create (AOwner);
  Initialize;
  FOnChange           := nil;
  FOnChangeFileName   := nil;
  FOnBackgroundChange := nil;
  FSpread             := True;
end;

destructor TProyectSettings.Destroy;
begin
  FFileName           := '';
  FCardFileName       := '';
  FScriptFileName     := '';
  FBackgroundFileName := '';
  FDeviceID           := '';
  FJSONFileName       := '';
  FJSONSource         := '';
  FGridMargin.free;
  FGrid               := nil;

  inherited Destroy;
end;

procedure TProyectSettings.Notification (AComponent: TComponent; Operation: TOperation);
begin
  if (AComponent = FGrid) and (Operation = opRemove) then
    FGrid := nil;
end;

procedure TProyectSettings.SetFileName (Value: string);
begin
  if FFileName <> Value then
  begin
    FFileName := Value;

    if Assigned (FOnChangeFileName) then
      FOnChangeFileName (Self);
  end;
end;

procedure TProyectSettings.SetCardFileName (Value: string);
begin
  if FCardFileName <> Value then
  begin
    FCardFileName := Value;
    Changed       := True;
  end;
end;

procedure TProyectSettings.SetScriptFileName (Value: string);
begin
  if FScriptFileName <> Value then
  begin
    FScriptFileName := Value;
    Changed         := True;
  end;
end;

procedure TProyectSettings.SetGrid (Value: TGridCard);
begin
  if FGrid <> Value then
  begin
    FGrid := Value;

    if FSpread and Assigned (FGrid) then
      ApplyGridSettings;
  end;
end;

procedure TProyectSettings.SetChanged (Value: boolean);
begin
  if FChanged <> Value then
    FChanged := Value;

  if Value then
    DoChange;
end;

procedure TProyectSettings.SetRowCount (Value: integer);
begin
  if FRowCount <> Value then
  begin
    FRowCount := Value;
    Changed   := True;

    if FSpread and Assigned (FGrid) then
      FGrid.RowCount := FRowCount;
  end;
end;

procedure TProyectSettings.SetColCount (Value: integer);
begin
  if FColCount <> Value then
  begin
    FColCount := Value;
    Changed   := True;

    if FSpread and Assigned (FGrid) then
      FGrid.ColCount := FColCount;
  end;
end;

procedure TProyectSettings.SetMaxGridCards (Value: integer);
begin
  if FMaxGridCards <> Value then
  begin
    FMaxGridCards := Value;
    Changed       := True;

    if FSpread and Assigned (FGrid) then
      FGrid.MaxGridCards := FMaxGridCards;
  end;
end;

procedure TProyectSettings.SetPlayerMonitorNum(const Value: integer);
begin
  if FPlayerMonitorNum <> Value then
  begin
    FPlayerMonitorNum := Value;
    Changed     := True;
  end;
end;

procedure TProyectSettings.SetAutoAdjust (Value: boolean);
begin
  if FAutoAdjust <> Value then
  begin
    FAutoAdjust := Value;
    Changed     := True;

    if FSpread and Assigned (FGrid) then
      FGrid.AutoAdjust := FAutoAdjust;
  end;
end;

procedure TProyectSettings.SetAutoAdjustCardSize(const Value: boolean);
begin
  if FAutoAdjustCardSize <> Value then
  begin
    FAutoAdjustCardSize := Value;
    Changed     := True;
  end;
end;

function TProyectSettings.GetTopMargin: Single;
begin
  result := FGridMargin.Top
end;

procedure TProyectSettings.SetTopMargin (Value: Single);
begin
  if FGridMargin.Top <> Value then
  begin
    FGridMargin.Top := Value;
    Changed         := True;

    if FSpread and Assigned (FGrid) then
      FGrid.GridTopMargin := Value;
  end;
end;

function TProyectSettings.GetLeftMargin: Single;
begin
  result := FGridMargin.Left;
end;

procedure TProyectSettings.SetLeftMargin (Value: Single);
begin
  if FGridMargin.Left <> Value then
  begin
    FGridMargin.Left := Value;
    Changed          := True;

    if FSpread and Assigned (FGrid) then
      FGrid.GridLeftMargin := Value;
  end;
end;

function TProyectSettings.GetRightMargin: Single;
begin
  result := FGridMargin.Right;
end;

procedure TProyectSettings.SetRightMargin (Value: Single);
begin
  if FGridMargin.Right <> Value then
  begin
    FGridMargin.Right := Value;
    Changed           := True;

    if FSpread and Assigned (FGrid) then
      FGrid.GridRightMargin := Value;
  end;
end;

function TProyectSettings.GetBottomMargin: Single;
begin
  result := FGridMargin.Bottom;
end;

procedure TProyectSettings.SetBottomMargin (Value: Single);
begin
  if FGridMargin.Bottom <> Value then
  begin
    FGridMargin.Bottom := Value;
    Changed            := True;

    if FSpread and Assigned (FGrid) then
      FGrid.GridBottomMargin := Value;
  end;
end;

procedure TProyectSettings.SetHorizInnerMargin (Value: Single);
begin
  if FHorizInnerMargin <> Value then
  begin
    FHorizInnerMargin := Value;
    Changed           := True;

    if FSpread and Assigned (FGrid) then
      FGrid.HorizInnerMargin := Value;
  end;
end;

procedure TProyectSettings.SetVertInnerMargin (Value: Single);
begin
  if FVertInnerMargin <> Value then
  begin
    FVertInnerMargin := Value;
    Changed          := True;

    if FSpread and Assigned (FGrid) then
      FGrid.VertInnerMargin := Value;
  end;
end;

procedure TProyectSettings.SetRESTServerAddress (Value: string);
begin
  if FRESTServerAddress <> Value then
  begin
    FRESTServerAddress := Value;
    Changed            := True;

    if Assigned (FGrid) then
      FGrid.RESTServerAddress := Value;
  end;
end;

procedure TProyectSettings.SetRESTRequest (Value: string);
begin
  if FRESTRequest <> Value then
  begin
    FRESTRequest := Value;
    Changed      := True;

    if FSpread and Assigned (FGrid) then
      FGrid.RESTRequest := Value;
  end;
end;

procedure TProyectSettings.SetRESTInterval (Value: integer);
begin
  if FRESTInterval <> Value then
  begin
    FRESTInterval := Value;
    Changed       := True;

    if FSpread and Assigned (FGrid) then
      FGrid.RESTInterval := Value;
  end;
end;

procedure TProyectSettings.SetJSONFile (Value: string);
begin
  if FJSONFileName <> Value then
  begin
    FJSONFileName := Value;
    Changed       := True;

    if FSpread and Assigned (FGrid) then
      FGrid.JSONFileName := FJSONFileName;
  end;
end;

procedure TProyectSettings.SetJSONSourceType (Value: TJSONSourceType);
begin
  if FJSONSourceType <> Value then
  begin
    FJSONSourceType := Value;
    Changed         := True;

    if FSpread and Assigned (FGrid) then
      FGrid.JSONSourceType := FJSONSourceType;
  end;
end;

procedure TProyectSettings.SetJSONSource (Value: string);
begin
  if FJSONSource <> Value then
  begin
    FJSONSource := Value;
    //In this case, the OnChange event is not fired because it is not a property that is stored in the .ini

    if FSpread and Assigned (FGrid) then
      FGrid.JSONSource := FJSONSource;
  end;
end;

procedure TProyectSettings.SetTransitionType (Value: TTransitionType);
begin
  if FTransitionType <> Value then
  begin
    FTransitionType := Value;
    Changed         := True;

    if FSpread and Assigned (FGrid) then
      FGrid.TransitionType := Value;
  end;
end;

procedure TProyectSettings.SetTransitionInterval (Value: integer);
begin
  if FTransitionInterval <> Value then
  begin
    FTransitionInterval := Value;
    Changed             := True;

    if FSpread and Assigned (FGrid) then
      FGrid.TransitionInterval := Value;
  end;
end;

procedure TProyectSettings.SetTransitionSpeed (Value: integer);
begin
  if FTransitionSpeed <> Value then
  begin
    FTransitionSpeed := Value;
    Changed          := True;

    if FSpread and Assigned (FGrid) then
      FGrid.TransitionSpeed := Value;
  end;
end;


procedure TProyectSettings.SetTransitionSteps (Value: integer);
begin
  if FTransitionSteps <> Value then
  begin
    FTransitionSteps := Value;
    Changed          := True;

    if FSpread and Assigned (FGrid) then
      FGrid.TransitionSteps := Value;
  end;
end;


procedure TProyectSettings.SetDeviceID (Value: string);
begin
  if FDeviceID <> Value then
  begin
    FDeviceID := Value;
    Changed   := True;
  end;
end;

procedure TProyectSettings.SetBackgroundType (Value: TBackgroundType);
begin
  if FBackgroundType <> Value then
  begin
    FBackgroundType := Value;
    Changed         := True;

    DoBackgroundChange;
  end;
end;

procedure TProyectSettings.SetBackgroundColor (Value: TAlphaColor);
begin
  if FBackgroundColor <> Value then
  begin
    FBackgroundColor := Value;
    Changed          := True;
    DoBackgroundChange;
  end;
end;

procedure TProyectSettings.SetBackgroundFileName (Value: string);
begin
  if FBackgroundFileName <> Value then
  begin
    FBackgroundFileName := Value;
    Changed             := True;
    DoBackgroundChange;
  end;
end;

procedure TProyectSettings.SetSpread (Value: boolean);
begin
  if FSpread <> Value then
  begin
    FSpread := Value;

    if FSpread then
      ApplyGridSettings;
  end;
end;

procedure TProyectSettings.Clear;
begin
  Initialize;
end;

procedure TProyectSettings.ApplyGridSettings;
begin
  if not FSpread or not Assigned (FGrid) then exit;

  FGrid.BeginUpdate;
  try
    FGrid.RowCount           := FRowCount;
    FGrid.ColCount           := FColCount;
    FGrid.AutoAdjust         := FAutoAdjust;
    FGrid.MaxGridCards       := FMaxGridCards;
    FGrid.HorizInnerMargin   := FHorizInnerMargin;
    FGrid.VertInnerMargin    := FVertInnerMargin;
    FGrid.GridMargin.Top     := FGridMargin.Top;
    FGrid.GridMargin.Left    := FGridMargin.Left;
    FGrid.GridMargin.Right   := FGridMargin.Right;
    FGrid.GridMargin.Bottom  := FGridMargin.Bottom;
    FGrid.RESTServerAddress  := FRESTServerAddress;
    FGrid.RESTRequest        := FRESTRequest;
    FGrid.RESTInterval       := FRESTInterval;
    FGrid.JSONFileName       := FJSONFileName;
    FGrid.JSONSourceType     := FJSONSourceType;
    FGrid.JSONSource         := FJSONSource;
    FGrid.TransitionType     := FTransitionType;
    FGrid.TransitionInterval := FTransitionInterval;
    FGrid.TransitionSpeed    := FTransitionSpeed;
    FGrid.TransitionSteps    := FTransitionSteps;
  finally
    FGrid.EndUpdate;
    FGrid.Update;
  end;
end;

procedure TProyectSettings.Initialize;
var
  rectMargin: TRectF;
begin
  rectMargin          := TRectF.Create(5, 5, 5, 5);
  FFileName           := '';
  FCardFileName       := '';
  FScriptFileName     := '';
  FChanged            := False;
  FGrid               := nil;
  FRowCount           := 2;
  FColCount           := 3;
  FMaxGridCards       := 0;
  FAutoAdjust         := True;
  FAutoAdjustCardSize := False;
  FGridMargin         := TBounds.Create(rectMargin);
  FHorizInnerMargin   := 10;
  FVertInnerMargin    := 10;
  FRESTServerAddress  := '';
  FRESTRequest        := '';
  FRESTInterval       := 60 * 5; // 5 minutes
  FTransitionType     := ttNone;
  FTransitionInterval := 15;     //15 seconds
  FTransitionSpeed    := 500;    //0.5 seconds
  FTransitionSteps    := 25 ;
  FJSONFileName       := '';
  FJSONSourceType     := jtManual;
  FJSONSource         := '';
  FDeviceID           := '';
  FBackgroundType     := btColor;
  FBackgroundColor    := TAlphaColors.Null;
  FBackgroundFileName := '';
  FPlayerMonitorNum   := 1;
end;

procedure TProyectSettings.DoChange;
begin
  if Assigned (FOnChange) then
    FOnChange (Self);
end;

procedure TProyectSettings.DoBackgroundChange;
begin
  if Assigned (FOnBackgroundChange) then
    FOnBackgroundChange (Self);
end;

procedure TProyectSettings.LoadFromIniFile (AFileName: string);
var
  Ini: TIniFile;
  s:   string;
begin
  if not FileExists (AFileName) then
    raise Exception.Create(Format ('File %s not found', [AFileName]));

  Ini := TIniFile.Create(AFileName);
  try
    FileName := AFileName;

    //General settings
    FCardFileName   := Ini.ReadString('General', 'CardFileName',   '');
    FScriptFileName := Ini.ReadString('General', 'ScriptFileName', '');
    FDeviceID       := Ini.ReadString('General', 'DeviceID',       '');

    //Grid settings
    FRowCount             := Ini.ReadInteger('Grid', 'RowCount',      2);
    FColCount             := Ini.ReadInteger('Grid', 'ColCount',      3);
    FMaxGridCards         := Ini.ReadInteger('Grid', 'MaxGridCards',  0);
    FAutoAdjust           := Ini.ReadBool   ('Grid', 'AutoAdjust', True);
    FAutoAdjustCardSize   := Ini.ReadBool   ('Grid', 'AutoAdjustCardSize', False);
    FPlayerMonitorNum     := Ini.ReadInteger('Grid', 'PlayerMonitorNum', 1);
    FGridMargin.Top       := Ini.ReadInteger('Grid', 'TopMargin',     5);
    FGridMargin.Left      := Ini.ReadInteger('Grid', 'LeftMargin',    5);
    FHorizInnerMargin     := Ini.ReadInteger('Grid', 'InnerHoriz',   10);
    FVertInnerMargin      := Ini.ReadInteger('Grid', 'InnerVert',    10);

    //Background settings
    FBackgroundType     := TBackgroundType (GetEnumValue (TypeInfo (TBackgroundType), Ini.ReadString ('Background', 'Type', 'btColor')));
    FBackgroundColor    := Ini.ReadInteger('Background', 'Color',         TAlphaColors.Null);
    FBackgroundFileName := Ini.ReadString ('Background', 'ImageFileName', '');

    //Rest settings
    FRESTServerAddress := Ini.ReadString ('REST', 'ServerAddress', '');
    FRESTRequest       := Ini.ReadString ('REST', 'Request',       '');
    FRESTInterval      := Ini.ReadInteger('REST', 'Interval',      60 * 5);
    FJSONFileName      := Ini.ReadString ('REST', 'JSONFileName',  '');
    FJSONSourceType    := TJSONSourceType (Ini.ReadInteger('REST', 'JSONSourceType', 0));

    //Transition settings
    FTransitionType     := TTransitionType (GetEnumValue (TypeInfo (TTransitionType), Ini.ReadString ('Transition', 'Type', 'ttNone')));
    FTransitionInterval := Ini.ReadInteger('Transition', 'Interval', 10);
    FTransitionSpeed    := Ini.ReadInteger('Transition', 'Speed',    500);
    FTransitionSteps    := Ini.ReadInteger('Transition', 'Steps',    25);

  finally
    Ini.Free;
    Changed := False;
  end;
end;

procedure TProyectSettings.SaveToIniFile (AFileName: string = '');
var
  Ini: TIniFile;
begin
  if FFileName.IsEmpty and AFileName.IsEmpty then
    raise Exception.Create('File name required');

  if AFileName.IsEmpty then
    AFileName := FFileName;

  Ini := TIniFile.Create(AFileName);
  try
    FileName := AFileName;

    //General settings
    Ini.WriteString('General', 'CardFileName',   FCardFileName);
    Ini.WriteString('General', 'ScriptFileName', FScriptFileName);
    Ini.WriteString('General', 'DeviceID',       FDeviceID);

    //Grid settings
    Ini.WriteInteger('Grid', 'RowCount',     FRowCount);
    Ini.WriteInteger('Grid', 'ColCount',     FColCount);
    Ini.WriteInteger('Grid', 'MaxGridCards', FMaxGridCards);
    Ini.WriteBool   ('Grid', 'AutoAdjust',   FAutoAdjust);
    Ini.WriteBool   ('Grid', 'AutoAdjustCardSize',   FAutoAdjustCardSize);
    Ini.WriteInteger('Grid', 'PlayerMonitorNum',   FPlayerMonitorNum);
    Ini.WriteInteger('Grid', 'TopMargin',    Trunc (FGridMargin.Top));
    Ini.WriteInteger('Grid', 'LeftMargin',   Trunc (FGridMargin.Left));
    Ini.WriteInteger('Grid', 'InnerHoriz',   Trunc (FHorizInnerMargin));
    Ini.WriteInteger('Grid', 'InnerVert',    Trunc (FVertInnerMargin));

    //Background settings
    Ini.WriteString ('Background', 'Type',          GetEnumName (TypeInfo (TBackgroundType), Ord (FBackgroundType)));
    Ini.WriteInteger('Background', 'Color',         FBackgroundColor);
    Ini.WriteString ('Background', 'ImageFileName', FBackgroundFileName);

    //Rest settings
    Ini.WriteString ('REST', 'ServerAddress',  FRESTServerAddress);
    Ini.WriteString ('REST', 'Request',        FRESTRequest);
    Ini.WriteInteger('REST', 'Interval',       FRESTInterval);
    Ini.WriteString ('REST', 'JSONFileName',   FJSONFileName);
    Ini.WriteInteger('REST', 'JSONSourceType', Ord (FJSONSourceType));

    //Transition settings
    Ini.WriteString ('Transition', 'Type',     GetEnumName (TypeInfo (TTransitionType), Ord (FTransitionType)));
    Ini.WriteInteger('Transition', 'Interval', FTransitionInterval);
    Ini.WriteInteger('Transition', 'Speed',    FTransitionSpeed);
    Ini.WriteInteger('Transition', 'Steps',    FTransitionSteps);
  finally
    Ini.Free;
    Changed := False;
  end;
end;
{$endregion}

procedure Register;
begin
  System.Classes.RegisterComponents('Compudime', [TJSONCard]);
  System.Classes.RegisterComponents('Compudime', [TGridCard]);
end;

end.
