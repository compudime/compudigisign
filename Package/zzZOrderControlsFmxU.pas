unit zzZOrderControlsFmxU;

{
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PURPOSE     Get and Set the Z order of FireMonkey controls

AUTHOR      Scott Hollows
            Web     www.ScottHollows.com
            Email   scott.hollow@gmail.com

LEGAL
            Placed in the public domain
            Use at your own risk
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}

interface

function zzGetControlZOrder (
                  aControl   : TObject     // only Controls are processed, but you can throw anything you like at this
                  ) : integer;
  // get the Z order of the control within its parent
  // if not found or not applicable return -1

function zzSetControlZOrder (
                  aControl   : TObject;    // only Controls are processed, but you can throw anything you like at this
                  aNewZorder : integer     // ignore if this is -1
                  ) : boolean;
  // set the Z order of the control within its parent
  // if a control already exists at this Z Order, the control will be placed underneath it (pushing the other control up)

implementation

uses
       FMX.Controls
      ,System.Classes
//    ,MainForm          // for testing
      ,FMX.Types
      ,FMX.Forms
      ,FMX.StdCtrls;

// =============================================================================

function zzGetControlZOrder (
                  aControl   : TObject        // only Controls are processed, but throw anything you like at this
                  ) : integer;
  // return the Z order of the control within its parent
  // if not found or not applicable, return -1
var
  i        : integer;
  vControl : TControl;
  vList    : TList;
  vParent  : TFmxObject;
begin
  result := -1;                                    // flag for not found or not relevent

  try

      if    (aControl  is TControl)                     // some of these might not be needed
        and ((aControl as TControl).Parent is TComponent)
        and ((aControl as TControl).Parent <> nil)
        and ((aControl as TControl).Parent is TFmxObject)
      then
          begin
          vControl := aControl as TControl;
          vParent  := vControl.Parent as TFmxObject;

          vList := TList.Create;
                                                         // determine current position in z-order
          for i := 0 to vParent.ChildrenCount - 1 do     // loop through all children
              if  vParent.Children[i] = aControl then    // found the control
                  begin
                  result := i;
                  Break;
                  end;

          vList.Free;
          end;
  except          // ignore all errors
  end;
end;

// =============================================================================

function zzSetControlZOrder (
                  aControl   : TObject;    // only Controls are processed, but you can throw anything you like at this
                  aNewZorder : integer     // ignore if this is -1
                  ) : boolean;
  // set the Z order of the control
  // if a control already exists at this Z Order, the control will be placed underneath it (pushing the other control up)
  //
  // ignore all errors
  // return TRUE if change was applied
  // return FALSE if not changes, probably because Z Order can be set
var
  i           : Integer;
  vControl    : TControl;
  vZorderList : TList;        // list of controls to be raised
  vParent     : TFmxObject;   // form, panel, button etc
  vCurrentZ   : integer;
  vFirstZ     : integer;
  vStart      : integer;
begin
  result := FALSE;

  vCurrentZ := zzGetControlZOrder(aControl);

  if    (aControl is TControl)
    and (vCurrentZ  >= 0)
    and (aNewZorder >= 0)
    and (vCurrentZ <> aNewZorder)
    and ((aControl as TControl).Parent <> nil)
  then
        begin
        vZorderList := TList.Create;
        try
              vControl := aControl as TControl;
              vParent  := vControl.Parent as TFmxObject;

              if     aNewZOrder > vCurrentZ         // if moving higher in Z order
                then vFirstZ := aNewZOrder + 1
                else vFirstZ := aNewZOrder;

                  // TForm and TPanel includes a child element #0 that is a TRectangle.
                  // the control should not be moved underneath the TRectangle otherwise
                  // it will be covered up by the TRectangle and wont be visible
              if   (1=2)
                or (vParent is TForm)
                or (vParent is TPanel)
              then vStart := 1
              else vStart := 0;

              if  vStart > vFirstZ then
                  vFirstZ := vStart;

                                                                // populate list of controls that need to be raised to the top
              vZorderList.Add (aControl);                       // source control
              for i := vFirstZ to vParent.ChildrenCount - 1 do
                  if  vParent.Children[i] <> aControl then      // skip source control as already added
                      vZorderList.Add (vParent.Children[i]);    // other controls

                                                    // loop through controls and bring each one to the front
              for i := 0 to vZorderList.count - 1 do
                  TControl (vZorderList[i]).BringToFront;

              if   vParent is TControl then
                  (vParent as TControl).Repaint;

              result := TRUE;
        finally
             vZorderList.Free;
        end;
        end;
end;

// =============================================================================

end.
