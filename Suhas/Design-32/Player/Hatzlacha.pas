function Currency(text: string): string ;  
var iText: string; 
Begin
iText := strtofloat(text) ;

if (pos('.',itext)> 1) and (length(itext)=3)then
  text:= text + '0'; 

  if Pos('.', itext)=0 then
    text:= text + '.00'
  if itext >0 then
   Begin
    if itext> 1.00 then
    text:= '$' + text else
    if itext< 1.00 then
    text:= text + ''+chr(0162);
    if pos('0',itext)=1 then
    text:= copy(Text,3,9) 
   End;
 Else
     text := '' ;
  
      
  result := text ;
end; 

procedure BeforeObjectDisplay_MyDesc (Sender: TControl);
Var vPrice, vSale, vPkgPrc, vUpp, vSPkgPrc, vSUpp, vPkSaleMode,vCasePrice,vCaseSale,vCsSaleMode,vCode: string ; 
begin
  vPrice       := card.GetJSONFieldValue ('Price')       ; 
vSale        := card.GetJSONFieldValue ('Sale')          ;
vPkgPrc      := card.GetJSONFieldValue ('PkgPrc')        ;
vUpp         := card.GetJSONFieldValue ('Upp')           ;
vSPkgPrc     := card.GetJSONFieldValue ('SPkgPrc')       ;
vSUpp        := card.GetJSONFieldValue ('SUpp')          ;
vPkSaleMode  := card.GetJSONFieldValue ('PkSaleMode')    ;
vCasePrice   := card.GetJSONFieldValue ('CasePrice')     ;
vCaseSale    := card.GetJSONFieldValue ('CaseSale')      ;
vCsSaleMode  := card.GetJSONFieldValue ('CsSaleMode')    ;
vCode        := card.GetJSONFieldValue ('Code')          ;
//vPrice := CurFrmater(vPrice) ;
//CurFrmater(card.GetJSONFieldValue ('Sale')) ; 

                //Sets Desc Value based on Desc,SmlDesc

   MyDesc.text := card.GetJSONFieldValue ('Desc') ; 
   If MyDesc.text = '' Then
      MyDesc.text := card.GetJSONFieldValue ('SmlDesc')


               


               //Sets Price value based on Price,Sale$,Sale%

   If StrToFloat(vSale)=0.00 Then
    Begin
       if card.GetJSONFieldValue('Scale')= False Then
        MyPrice.Text := '<align:left>'  + '<leftmargin:"35">' + '<Font:"ar essence":"55">' + '<b>' + Currency(vPrice) + '</b>' + '</Font>' + '<baseline:"-35">' + '<font:"rockwell":"20">'  + '  Ea'  + '</font>'
       Else
        MyPrice.Text := '<align:left>'  + '<leftmargin:"35">' + '<Font:"ar essence":"55">' + '<b>' + Currency(vPrice) + '</b>' + '</Font>' + '<baseline:"-35">' + '<font:"rockwell":"20">'  + '  Lb'  + '</font>'
    End;
   Else
     If card.GetJSONFieldValue('SaleMode')='$'  Then 
          MyPrice.Text := '<align:left>' + '<leftmargin:"35">' + '<Font:"ar essence":"55">' + '<b>' + Currency(vSale) + '</b>' + '</font>'  + '<baseline:"-35">' + '<font:rockwell:"20">'  + '  ' + '<strike>' + 'Reg '  + Currency(vPrice) + '</font>' + '</ul>' 
     Else
          MyPrice.text := '<align:left>' + '<leftmargin:"35">' + '<font:"ar essence":"55">' + '<b>' + vSale + '% Off' + '</font>' + '</b>' + '<font:"rockwell":"20">' + '<baseline:"-35">' + '  ' + '<strike>' + 'Reg ' + Currency(vPrice) + '</font>'




             //Sets PkgPrice value based on price,Sale$,Sale%

  If (StrToFloat(vPkgPrc) >0.00) and (vUpp)>1 Then
    Begin
       If (StrToFloat(vSpkgPrc) >0.00) and (vSUpp)>1 Then
         Begin
           If vPkSaleMode ='$' Then
             MyPkg.Text :=  '<align:left>' + '<leftmargin:"35">' + '<font:rockwell:"30">' + '<b>' + '<baseline:"-15">' + vSUpp + ' for ' + '</font>' + '</b>' + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + Currency(vSpkgPrc) + '</b>' + '<font:"rockwell":"20">' + '<baseline:"-35">' + '  ' + '<strike>' + 'Reg ' + vUpp + ' for ' + Currency(VPkgprc) + '</font>'  
           Else
            begin
             if  card.GetJSONFieldValue('Scale')= False Then 
              MyPkg.Text := '<align:left>' + '<leftmargin:"35">'  + '<font:"ar essence":"55">' + '<font:"rockwell":"30">' + '<b>' + '<baseline:"-15">'  + vSUpp + ' for' + '</font>' + '</b>'  + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + vSPkgPrc + '% off' + '</font>' + '</b>' +'<font:"rockwell":"20">' + '<baseline:"-35">' + '  ' + '<strike>' + 'Reg ' + currency(vPrice) + ' Ea';
             Else
              MyPkg.Text := '<align:left>' + '<leftmargin:"35">'  + '<font:"ar essence":"55">' + '<font:"rockwell":"30">' + '<b>' + '<baseline:"-15">'  + vSUpp + ' for' + '</font>' + '</b>'  + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + vSPkgPrc + '% off' + '</font>' + '</b>' +'<font:"rockwell":"20">' + '<baseline:"-35">' + '  ' + '<strike>' + 'Reg ' + currency(vPrice) + ' Lb';
            end; 
          End;
   Else
       MyPkg.Text := '<align:left>' + '<leftmargin:"35">' + '<font:rockwell:"30">'  + '<b>' + '<baseline:"-15">' + vUpp + ' for  ' + '</font>'  +  '</b>' + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + Currency(vPkgPrc) + '</font>' + '</b>'
    End; 
 Else
  If (StrToFloat(vSpkgPrc) >0.00) and (vSUpp)>1 Then
    begin  
      If vPkSaleMode ='$' Then
             MyPkg.Text := '<align:left>' + '<leftmargin:"35">' + '<font:"rockwell":"30">' + '<b>' + '<baseline:"-15">' + vSUpp + ' for ' + '</font>' + '</b>' + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + Currency(vSpkgPrc) 
           Else 
             MyPkg.Text := '<align:left>' + '<leftmargin:"35">' + '<font:"rockwell":"30">' + '<b>' + '<baseline:"-15">' + vSUpp + ' for' + '</font>' + '</b>' + '<font:"ar essence":"55">' + '<b>' + chr(13) + ' ' + vSPkgPrc + '% off' + '</font>' + '</b>' + '<font:"rockwell":"20">' + '<baseline:"-35">' + '  ' + '<strike>' + 'Reg ' + currency(vPrice) + ' Ea'
    end;     
        
 



            //Sets Case price based on price,Sale$,Sale%  

  If StrToFloat(vCasePrice)>0.00 Then
   Begin
     If StrToFloat(vCaseSale)>0.00 Then
       Begin
         If vCsSaleMode ='$' Then
           MyCase.Text := '<Align:left>' + '<leftmargin:"35">' + '<font:"rockwell":"20">' + 'Case Sale ' + Currency(vCaseSale) + ' /Reg ' + Currency(vCasePrice)
         Else
           MyCase.Text :=  'Case Sale ' +  vCaseSale + ' % off /Reg ' + Currency(vCasePrice)
       End;
     Else
    MyCase.Text := '<align:left>' + '<leftmargin:"35">' + '<font:rockwell:"20">' + 'Case Price ' + Currency(vCasePrice)
   End;   
 Else
  If StrToFloat(vCaseSale)>0.00 Then
         If vCsSaleMode ='$' Then
           MyCase.Text := '<align:left>' + '<leftmargin:"35">' + '<Font:"rockwell":"20">' + 'Case Sale ' + Currency(vCaseSale)
         Else
           MyCase.Text := ' Case Sale ' + vCaseSale+ ' % off'
    




          //Sets visibility based on conditions

                          //When Sale Turn on MySaleSymbol

   If (StrToFloat(vSale)>0.00) or (StrToFloat(vSPkgprc)>0.00) or (StrToFloat(vCaseSale)>0.00)
     Then
      MySaleSymbol.visible :=True
   Else
      MySaleSymbol.visible :=False

 


  If (StrToFloat(vPkgPrc)>0.00) or (StrToFloat(vSPkgPrc)>0.00)  Then
   Begin
    If (StrToFloat(vCasePrice)>0.00) or (StrToFloat(vCaseSale) >0.00) Then
     Begin
      MyPrice.Visible := False;
      MyPkg.Visible   := True;
      MyCase.Visible  := True;
      MyPkg.Height    := 115;
      MyPkg.Align     := 12;
     End;
    Else    
     Begin
      MyPrice.visible := False;
      MyPkg.Visible   := True;
      MyCase.visible  := False;
      MyPkg.height    := 140;
      MyPkg.align     := 12;
     End;
   End;
  Else
   Begin
    If (StrToFloat(vCasePrice)>0.00) or (StrToFloat(vCaseSale) >0.00) Then
     Begin
      MyPrice.Visible := True;
      MyPkg.Visible   := False;
      MyCase.Visible  := True;
      MyPrice.align   := 12; 
     End;
    Else
     Begin
      MyPrice.Visible := True;
      MyPkg.Visible   := False;
      MyCase.Visible  := False;
      MyPrice.align   := 12;
     End;
   End;
end;

