unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, uCards, System.IOUtils,
  Androidapi.Helpers, Androidapi.JNI.JavaTypes, Androidapi.JNI.GraphicsContentViewText,
  System.Permissions, Androidapi.JNI.App, Androidapi.JNI.OS, FMX.Layouts,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.EditBox, FMX.SpinBox;

type
  TFMain = class(TForm)
    ScrollBox1: TScrollBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    SpinBox1: TSpinBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure SpinBox1Change(Sender: TObject);
  private
    { Private declarations }
    Card:            TJSONCard;
    GrantedReadPriv: boolean;
    procedure CheckIfReadExternalStorageIsGranted;
  public
    { Public declarations }
  end;

var
  FMain: TFMain;

implementation

{$R *.fmx}

function RandString(const stringsize: integer): string;
const
  ss: string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var
  i: integer;
  s: string;
begin
  result :='';
  for i := 1 to stringsize do
    result := result + ss [random (length(ss)) + 1];
end;

procedure TFMain.FormCreate(Sender: TObject);
begin
  Card            := TJSONCard.Create(Self);
  Card.Parent     := Self;
  Card.Position.X := 0;
  Card.Position.Y := 0;
  GrantedReadPriv := False;
end;

procedure TFMain.FormDestroy(Sender: TObject);
begin
  Card.Free;
end;

procedure TFMain.FormShow(Sender: TObject);
begin
  CheckIfReadExternalStorageIsGranted;
  if not GrantedReadPriv then Close;

  Card.LoadFromFile(System.IOUtils.TPath.GetSharedDocumentsPath + System.SysUtils.PathDelim + 'Small2.card');
  ScrollBox1.Position.Y := Card.Height + 10;
  ScrollBox1.Align      := TAlignLayout.Bottom;
  SpinBox1.Value        := TJSONLabel (Card.LstControls [4]).RotationAngle;
end;

procedure TFMain.SpinBox1Change(Sender: TObject);
begin
  TJSONLabel (Card.LstControls [4]).RotationAngle := SpinBox1.Value;
  Label1.RotationAngle := SpinBox1.Value;
end;

procedure TFMain.Button1Click(Sender: TObject);
var
  Ctrl: TControl;
begin
  Ctrl := TControl (Card.LstControls [1]);
  if Assigned (Ctrl) and (Ctrl is TJSONLabel) then
    TJSONLabel (Ctrl).Text := RandString(Random(5) + 5);

  Ctrl := TControl (Card.LstControls [3]);
  if Assigned (Ctrl) and (Ctrl is TJSONLabel) then
    TJSONLabel (Ctrl).Text := RandString(Random(5) + 5);

  Card.Background.Repaint;
//  Card.RepaintControls;
//  Card.Repaint;
end;

procedure TFMain.Button2Click(Sender: TObject);
var
  i:    integer;
  Ctrl: TControl;
begin
  for i := 0 to Card.LstControls.Count - 1 do
  begin
    Ctrl         := TControl (Card.LstControls [i]);
    Ctrl.Visible := not Ctrl.Visible;
  end;
end;

procedure TFMain.Button3Click(Sender: TObject);
begin
  TControl (Card.LstControls [3]).Visible := not TControl (Card.LstControls [3]).Visible;
end;

procedure TFMain.CheckIfReadExternalStorageIsGranted;
begin
  PermissionsService.RequestPermissions([JStringToString(TJManifest_permission.JavaClass.READ_EXTERNAL_STORAGE)],
    procedure(const APermissions: TArray<string>; const AGrantResults: TArray<TPermissionStatus>)
    begin
      GrantedReadPriv := (Length(AGrantResults) = 1) and (AGrantResults[0] = TPermissionStatus.Granted);
    end);
end;

end.
