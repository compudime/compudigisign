unit uMainPlayer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, uCards,
  FMX.StdCtrls, FMX.Controls.Presentation, System.IOUtils, FMX.ListBox;

type
  TForm2 = class(TForm)
    StatusBar1: TStatusBar;
    labVersion: TLabel;
    labPages: TLabel;
    labSystem: TLabel;
    Panel1: TPanel;
    cbTransitions: TComboBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbTransitionsChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    Background:        TBackgroundImage;
    Grid:              TGridCard;
    Settings:          TProyectSettings;
    SettingsFileName:  string;
    CardFileName:      string;
    ScriptFileName:    string;
    GrantedReadPriv:   boolean;
    procedure UpdatePageNumber;
    procedure RecalculateRowCols (Sender: TObject);
    procedure ChangePage (Sender: TObject);
    procedure LoadFiles;
    procedure CheckIfReadExternalStorageIsGranted;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation
uses Androidapi.Helpers, Androidapi.JNI.JavaTypes, Androidapi.JNI.GraphicsContentViewText,
  System.Permissions, Androidapi.JNI.App, Androidapi.JNI.OS;
{$R *.fmx}

{$region 'Version routines'}
function GetAppVersionStr: string;
var
{$IFDEF ANDROID}
   PackageManager: JPackageManager;
   PackageInfo: JPackageInfo;
{$ENDIF}
{$IFDEF MACOS}
  CFStr: CFStringRef;
  Range: CFRange;
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
{$ENDIF}
begin
{$IFDEF MACOS}
  CFStr := CFBundleGetValueForInfoDictionaryKey(
    CFBundleGetMainBundle, kCFBundleVersionKey);
  Range.location := 0;
  Range.length := CFStringGetLength(CFStr);
  SetLength(Result, Range.length);
  CFStringGetCharacters(CFStr, Range, PChar(Result));
{$ENDIF}
{$IFDEF MSWINDOWS}
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
{$ENDIF}
{$IFDEF ANDROID}
   PackageManager := SharedActivityContext.getPackageManager;
   PackageInfo := PackageManager.getPackageInfo
     (SharedActivityContext.getPackageName, 0);
   result := JStringToString(PackageInfo.versionName);
{$ENDIF}
end;
{$endregion}

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Grid.Stop;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  labVersion.Text            := GetAppVersionStr;
  GrantedReadPriv            := False;
  SettingsFileName           := '';
  CardFileName               := '';
  ScriptFileName             := '';
  Background                 := TBackgroundImage.Create(Self);
  Background.Parent          := Self;
  Background.Align           := TAlignLayout.Client;
  Background.BackgroundColor := TAlphaColors.Skyblue;
  Grid                       := TGridCard.Create (Background);
  Grid.Parent                := Background;
  Grid.Align                 := TAlignLayout.Client;
  Grid.OnRecalculeColRows    := RecalculateRowCols;
  Grid.OnChangePage          := ChangePage;
  Grid.BeginUpdate;
  Grid.AutoAdjust         := True;
  Grid.TransitionType     := TTransitionType (cbTransitions.ItemIndex);
  Grid.TransitionInterval := 5;
  Grid.TransitionSpeed    := 500;
  Grid.EndUpdate;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  SettingsFileName := '';
  CardFileName     := '';
  ScriptFileName   := '';

  Settings.Free;
  Grid.Free;
  Background.Free;
end;

procedure TForm2.cbTransitionsChange(Sender: TObject);
begin
  Grid.TransitionType := TTransitionType (cbTransitions.ItemIndex);
end;

procedure TForm2.CheckIfReadExternalStorageIsGranted;
begin
  PermissionsService.RequestPermissions([JStringToString(TJManifest_permission.JavaClass.READ_EXTERNAL_STORAGE)],
    procedure(const APermissions: TArray<string>; const AGrantResults: TArray<TPermissionStatus>)
    begin
      GrantedReadPriv := (Length(AGrantResults) = 1) and (AGrantResults[0] = TPermissionStatus.Granted);
    end);
end;


procedure TForm2.FormShow(Sender: TObject);
begin
  CheckIfReadExternalStorageIsGranted;
  LoadFiles;
  Grid.Update;
  Grid.Start;
end;

procedure TForm2.LoadFiles;
var
  DemoJSON:  string;
  RootItem:  string;
  DataDepth: integer;
  JSON:      string;
  Script:    string;
begin
  if not GrantedReadPriv then exit;

  DemoJSON       := System.IOUtils.TPath.GetSharedDocumentsPath + System.SysUtils.PathDelim + 'JSON Prd List with Images.txt';
  CardFileName   := System.IOUtils.TPath.GetSharedDocumentsPath + System.SysUtils.PathDelim + 'Small2.card';
  ScriptFileName := System.IOUtils.TPath.GetSharedDocumentsPath + System.SysUtils.PathDelim + 'Small2.pas';
  if FileExists (DemoJSON) then
  begin
    JSON := TFile.ReadAllText(DemoJSON);
    AnalyzeJSON(JSON, RootItem, DataDepth);
    Grid.JSONDataSet.JSONSource := JSON;
    Grid.JSONDataSet.RootPath   := RootItem;
    Grid.JSONDataSet.ScanDepth  := DataDepth;
  end;

  if FileExists (CardFileName) then
    Grid.LoadTemplate(CardFileName);

  if FileExists (ScriptFileName) then
  begin
    Script := TFile.ReadAllText(ScriptFileName);
    Grid.Script := Script;
  end;
end;

procedure TForm2.UpdatePageNumber;
begin
  labPages.Text := Format ('%d/%d', [Grid.CurrentPage + 1, Grid.PageCount]);
end;

procedure TForm2.RecalculateRowCols (Sender: TObject);
begin
  UpdatePageNumber;
end;

procedure TForm2.ChangePage (Sender: TObject);
begin
  UpdatePageNumber;
end;

procedure TForm2.SpeedButton1Click(Sender: TObject);
begin
  Grid.Step(False);
end;

procedure TForm2.SpeedButton2Click(Sender: TObject);
begin
  Grid.Step(True);
end;

end.
